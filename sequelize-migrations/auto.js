var SequelizeAuto = require('sequelize-auto-v2')

const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/config/config.json')[env];

options = Object.assign({
  directory: './src/models/generated',
  //tables: ['users'],
  skipTables: ["_migrations", "_seeders"],
  additional: {}
}, config);

var auto = new SequelizeAuto(config.database, config.username, config.password, options);

auto.run(function (err) {
  if (err) throw err;

  //console.log(auto.tables); // table list
  //console.log(auto.foreignKeys); // foreign key list

});
