module.exports = require("../doScript.js")({
  sqlUp: `
  alter table chat add column uploadId int;

  delimiter $$

  drop procedure if exists findChat
  $$

  create procedure findChat(pConversationId int)
  begin

  select chat.*, uploads.nombreAsignado, uploads.nombreOriginal, origin.userName as fromName, target.userName as toNombre from chat
  join users origin on origin.id = chat.fromId
  join users target on target.id = chat.toId
  left join uploads on uploads.id = chat.uploadId
  where chat.conversationId = pConversationId;

  end
  $$

  `,
  sqlDown: `
  alter table chat drop column uploadId;
  `
});
