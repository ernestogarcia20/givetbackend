'use strict';

module.exports = require('../doScript.js')({
  sqlUp: `

CREATE TABLE messages(
  id int(11) NOT NULL AUTO_INCREMENT, 
  hUserTo int(11) DEFAULT NULL,
  message varchar(250),
  readOn datetime default null,
  createdAt datetime DEFAULT CURRENT_TIMESTAMP,
  updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
  
  PRIMARY KEY(id)
) ;
$$
  ALTER TABLE messages
  ADD INDEX fk_messages_to_idx(hUserTo ASC);
  $$
  ALTER TABLE messages
  ADD CONSTRAINT fk_messages_to
  FOREIGN KEY(hUserTo)
  REFERENCES users(id)
  ON DELETE RESTRICT;
  $$
`,

  sqlDown: `
drop table messages;
`
});
