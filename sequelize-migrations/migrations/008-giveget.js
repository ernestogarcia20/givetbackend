module.exports = require("../doScript.js")({
  sqlUp: `
  ALTER TABLE give_get
  ADD COLUMN giveRating smallint NULL AFTER updatedAt,
  ADD COLUMN getRating smallint NULL AFTER giveRating;
  `,
  sqlDown: `
  alter table give_get drop column giveRating, getRating;
  `
});
