'use strict';

module.exports = require('../doScript.js')({
  sqlUp: `

  drop procedure if exists findGiveGet;
  DELIMITER $$
  $$

CREATE PROCEDURE findGiveGet(
  phUser int
)
BEGIN
select gg.*,
		giveS.hUser as hGiveUser,getS.hUser as hGetuser, 
		giveUser.fullName as giveUserName, getUser.fullName as getUserName,
    giveUser.rating as giveUserRating, getUser.rating as getUserRating,
    'country' as giveUserCountry, 'country' as getUserCountry
from give_get gg
join give_support giveS on giveS.id = gg.hGive
join get_support getS on getS.id = gg.hGet
join users giveUser on giveUser.id = giveS.hUser
join users getUser on getUser.id = getS.hUser
where giveUser.id = phUser or getUser.id = phUser;
END 
$$
`,

  sqlDown: `

drop procedure if exists findGiveGet;
`
});
