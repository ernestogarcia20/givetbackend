module.exports = require("../doScript.js")({
  sqlUp: `  
  ALTER TABLE sell_tickets
  ADD INDEX fk_sell_user_idx(hUser ASC);
$$
  ALTER TABLE sell_tickets
  ADD CONSTRAINT fk_sell_user
  FOREIGN KEY(hUser)
  REFERENCES users(id)
  ON DELETE RESTRICT;
$$
  ALTER TABLE buy_tickets
  ADD INDEX fk_buy_user_idx(hUser ASC);
$$
  ALTER TABLE buy_tickets
  ADD CONSTRAINT fk_buy_user
  FOREIGN KEY(hUser)
  REFERENCES users(id)
  ON DELETE RESTRICT;
$$
  ALTER TABLE buy_tickets
  ADD INDEX fk_packs_idx(hPack ASC);
$$
  ALTER TABLE buy_tickets
  ADD CONSTRAINT fk_packs
  FOREIGN KEY(hPack)
  REFERENCES packs(id)
  ON DELETE RESTRICT;
$$
  ALTER TABLE orders
  ADD INDEX fk_orders_sell_idx(hSell ASC);
$$
  ALTER TABLE orders
  ADD INDEX fk_orders_buy_idx(hBuy ASC);
$$
  ALTER TABLE orders
  ADD CONSTRAINT fk_orders_sell
  FOREIGN KEY(hSell)
  REFERENCES sell_tickets(id)
  ON DELETE RESTRICT;
$$
  ALTER TABLE orders
  ADD CONSTRAINT fk_orders_buy
  FOREIGN KEY(hBuy)
  REFERENCES buy_tickets(id)
  ON DELETE RESTRICT;
$$
  ALTER TABLE business
  ADD INDEX fk_business_cats_idx(hCategory ASC);
$$
  ALTER TABLE business
  ADD INDEX fk_business_users_idx(hUser ASC);
$$
  ALTER TABLE business
  ADD CONSTRAINT fk_business_cats
  FOREIGN KEY(hCategory)
  REFERENCES business_category(id)
  ON DELETE RESTRICT;
$$
  ALTER TABLE business
  ADD CONSTRAINT fk_business_users
  FOREIGN KEY(hUser)
  REFERENCES users(id)
  ON DELETE RESTRICT;
$$
  ALTER TABLE chat
  ADD INDEX fk_chat_from_idx(fromId ASC);
$$
  ALTER TABLE chat
  ADD INDEX fk_chat_to_idx(toId ASC);
$$
  ALTER TABLE chat
  ADD CONSTRAINT fk_chat_from
  FOREIGN KEY(fromId)
  REFERENCES users(id)
  ON DELETE RESTRICT;
$$
  ALTER TABLE chat
  ADD CONSTRAINT fk_chat_to
  FOREIGN KEY(toId)
  REFERENCES users(id)
  ON DELETE RESTRICT;
$$
  `,
  sqlDown: `
   ALTER TABLE sell_tickets
   DROP FOREIGN KEY fk_sell_user;
$$
   ALTER TABLE buy_tickets
   DROP FOREIGN KEY fk_buy_user;
$$
   ALTER TABLE buy_tickets
   DROP FOREIGN KEY fk_packs;
$$
    ALTER TABLE orders
   DROP FOREIGN KEY fk_orders_sell;
$$
   ALTER TABLE orders
   DROP FOREIGN KEY fk_orders_buy;
$$
   ALTER TABLE business
   DROP FOREIGN KEY fk_business_cats;
$$
   ALTER TABLE business
   DROP FOREIGN KEY fk_business_users;
$$
   ALTER TABLE chat
   DROP FOREIGN KEY fk_chat_from;
$$
   ALTER TABLE chat
   DROP FOREIGN KEY fk_chat_to;
$$
  ALTER TABLE sell_tickets
  DROP INDEX fk_sell_user_idx;
$$
  ALTER TABLE buy_tickets
  DROP INDEX fk_buy_user_idx;
$$
  ALTER TABLE buy_tickets
  DROP INDEX fk_packs_idx;
$$
  ALTER TABLE orders
  DROP INDEX fk_orders_sell_idx;
$$
  ALTER TABLE orders
  DROP INDEX fk_orders_buy_idx;
$$
  ALTER TABLE business
  DROP INDEX fk_business_cats_idx;
$$
  ALTER TABLE business
  DROP INDEX fk_business_users_idx;
$$
   ALTER TABLE chat
   DROP INDEX fk_chat_from_idx;
$$
   ALTER TABLE chat
   DROP INDEX fk_chat_to_idx;
$$
  `
});
