'use strict';

module.exports = require('../doScript.js')({
  sqlUp: `
drop table if exists uploads;
CREATE TABLE uploads(
	id int(11) NOT NULL AUTO_INCREMENT,
	hUser int not NULL,
	originalName varchar(100),
	assignedName varchar(100),
	fSize int,
  type varchar(20),
  hType int NULL,
	createdAt datetime default CURRENT_TIMESTAMP,
	updatedAt datetime default CURRENT_TIMESTAMP,
	updatedBy int NULL,
	PRIMARY key (id)
);
`,

  sqlDown: `
drop table if exists uploads;
`
});
