module.exports = require('../doScript.js')({
  sqlUp: `
	alter table users_tree
	add column
	createdAt datetime DEFAULT CURRENT_TIMESTAMP,
	add column
	updatedAt datetime DEFAULT CURRENT_TIMESTAMP;
		
  `,
  sqlDown: `
	alter table users_tree drop column createdAt, drop column updatedAt;

  `
});
