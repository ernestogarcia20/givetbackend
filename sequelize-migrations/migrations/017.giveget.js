module.exports = require("../doScript.js")({
  sqlUp: `
  
  alter table give_get 
    add column mustConfirmAt datetime;
  `,
  sqlDown: `
  alter table give_get
    drop column mustConfirmAt;
  `
});
