module.exports = require("../doScript.js")({
  sqlUp: `
    
  alter table wallet add column available decimal(17, 8) default 0;
  alter table wallet CHANGE COLUMN status status varchar(15);

  `,
  sqlDown: `
  alter table wallet drop column available;
  `
});
