module.exports = require("../doScript.js")({
  sqlUp: `
  drop table if exists config;
    
  CREATE TABLE config(
    id INT NOT NULL,
    gainPercent int(5) NULL,
    timeToConfirmOrder int(5) NULL,
    expirationDays int(5) NULL,
    paymantToUnlock decimal(17, 8) NULL,
    minGet decimal(17, 8) NULL,
    unlockPaymentAddress varchar(60),
    createdAt datetime DEFAULT CURRENT_TIMESTAMP,
    updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
  );

  insert into config(id, gainPercent, timeToConfirmOrder, expirationDays, paymantToUnlock, minGet)
    values(1,50,48,10,0.0096,0.0048);

  `,
  sqlDown: `
  
    drop table if exists config;
  `
});
