module.exports = require("../doScript.js")({
  sqlUp: `
  ALTER TABLE give_support
  ADD COLUMN finishAt datetime;

   ALTER TABLE get_support
   ADD COLUMN finishAt datetime;
  `,
  sqlDown: `
  alter table give_support drop column finishAt;
  alter table get_support drop column finishAt;
  `
});
