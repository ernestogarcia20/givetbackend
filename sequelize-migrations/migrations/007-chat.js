module.exports = require("../doScript.js")({
  sqlUp: `
  drop table if exists chat;
  
  CREATE TABLE chat(
    id INT(11) NOT NULL AUTO_INCREMENT,
    fromId int(11),
    toId int(11),
	conversationId int(11),
	message varchar(250),
	createdAt datetime DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(id));
  
  ALTER TABLE give_get ADD COLUMN hChat INT NULL AFTER mustCloseAt;

  delimiter $$

  drop procedure if exists findChat
  $$

  create procedure findChat(pConversationId int)
  begin

   select chat.*, origin.userName as fromName, target.userName as toNombre from chat
   join users origin on origin.id = chat.fromId
   join users target on target.id = chat.toId
   where chat.conversationId = pConversationId;

  end
  $$

  `,
  sqlDown: `
  drop table if exists chat;
  alter table give_get drop column hChat;
  drop procedure if exists findChat;
  `
});
