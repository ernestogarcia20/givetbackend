module.exports = require("../doScript.js")({
  sqlUp: `
  alter table give_support add column hPack int;
  `,
  sqlDown: `
  alter table give_support drop column hPack;
  `
});
