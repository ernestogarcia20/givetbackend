module.exports = require("../doScript.js")({
  sqlUp: `
  drop table if exists settings;

  create table settings(
    id INT NOT NULL,
    name varchar(20),
    type varchar(20),
    value varchar(100),
    description varchar(250),
    createdAt datetime DEFAULT CURRENT_TIMESTAMP,
    updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
  );

  insert into settings (id, name, type, value)
  values  (1,'gainPercent','number',50),
          (2,'timeToConfirmOrder','number',48),
          (3,'expirationDays','number',10),
          (4,'payToUnlock','number',0.0096),
          (5,'minSell','number',0.0048),
          (6,'payToUnlockAddress','string','btcbtcbtcbtc');
          
  `,
  sqlDown: `
  
    drop table if exists settings;
  `
});
