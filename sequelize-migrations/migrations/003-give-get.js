module.exports = require('../doScript.js')({
  sqlUp: `
CREATE TABLE give_support(
	id int(11) NOT NULL AUTO_INCREMENT,
	hUser int(11) DEFAULT NULL,
	amount decimal(17, 8) DEFAULT NULL,
	balance decimal(17, 8) DEFAULT NULL,
	status varchar(20) DEFAULT NULL,
	revocations smallint(6) DEFAULT '0',
	transactions smallint(6) DEFAULT '0',
	approvals smallint(6) DEFAULT '0',
	gain smallint(1) DEFAULT '0',
	createdAt datetime DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(id)
	);

CREATE TABLE get_support(
	id int(11) NOT NULL AUTO_INCREMENT,
	hUser int(11) DEFAULT NULL,
	amount decimal(17, 8) DEFAULT NULL,
	balance decimal(17, 8) DEFAULT NULL,
	status varchar(20) DEFAULT NULL,
	receiveEfective smallint(6) DEFAULT 0,
	receiveToken smallint(6) DEFAULT 0,
	revocations smallint(6) DEFAULT '0',
	transactions smallint(6) DEFAULT '0',
	approvals smallint(6) DEFAULT '0',
	createdAt datetime DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(id)
	);

CREATE TABLE give_get(
	id int(11) NOT NULL AUTO_INCREMENT,
	hGive int(11) DEFAULT NULL,
	hGet int(11) DEFAULT NULL,
	amount decimal(17, 8) DEFAULT NULL,
	status varchar(20) DEFAULT NULL,
	wayToPay varchar(100) DEFAULT NULL,
	voucher_image varchar(50) DEFAULT NULL,
	voucher_sentAt datetime,
	voucher_receivedAt datetime,
	mustCloseAt datetime DEFAULT CURRENT_TIMESTAMP,
	createdAt datetime DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(id)
);
		
  `,
  sqlDown: `
drop table give_support;
drop table get_support;
drop table give_get;
  `
});
