'use strict';

module.exports = require('../doScript.js')({
  sqlUp: `
CREATE TABLE roles(
  id int(11) NOT NULL AUTO_INCREMENT,
  roleName varchar(45) DEFAULT NULL,
  permissions varchar(254) DEFAULT NULL,
  createdAt datetime DEFAULT CURRENT_TIMESTAMP,
  updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id),
  UNIQUE KEY roleName_UNIQUE(roleName)
) ENGINE = InnoDB DEFAULT CHARSET = latin1;


CREATE TABLE user_roles(
  id int(11) NOT NULL AUTO_INCREMENT,
  roleId int(11) DEFAULT NULL,
  userId int(11) DEFAULT NULL,
  createdAt datetime DEFAULT CURRENT_TIMESTAMP,
  updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id)
) ENGINE = InnoDB DEFAULT CHARSET = latin1;


CREATE TABLE users(
  id int(11) NOT NULL AUTO_INCREMENT,
  email varchar(45) NOT NULL,
  userName varchar(45) DEFAULT NULL,
  fullName varchar(45) NOT NULL,
  hCountry int(11),
  phone varchar(45) DEFAULT NULL,
  password varchar(100) NOT NULL,
  password2 varchar(100) NOT NULL,
  referralUserName varchar(45) DEFAULT NULL,
  btc varchar(100) DEFAULT NULL,
  phoneOk smallint(6) DEFAULT '0',
  emailOk smallint(6) DEFAULT '0',
  verifyToken varchar(45) DEFAULT NULL,
  accessPin varchar(45) DEFAULT NULL,
  avatar varchar(45) DEFAULT NULL,
  rating decimal(3, 1) DEFAULT NULL,
  auth0Id varchar(45) DEFAULT NULL,
  googleId varchar(45) DEFAULT NULL,
  createdAt datetime DEFAULT CURRENT_TIMESTAMP,
  updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id),
  UNIQUE KEY eMail_UNIQUE(eMail),
  UNIQUE KEY userName_UNIQUE(userName)
) ENGINE = InnoDB DEFAULT CHARSET = latin1;


ALTER TABLE user_roles
  ADD INDEX fk_user_roles_users_idx(userId ASC),
  ADD INDEX fk_user_roles_roles_idx(roleId ASC);

ALTER TABLE user_roles
  ADD CONSTRAINT fk_user_roles_users
    FOREIGN KEY(userId)
    REFERENCES users(id)
    ON DELETE cascade,
  ADD CONSTRAINT fk_user_roles_roles
    FOREIGN KEY(roleId)
    REFERENCES roles(id)
    ON DELETE cascade;
`,

  sqlDown: `
drop table user_roles;
drop table roles;
drop table users;
`
});
