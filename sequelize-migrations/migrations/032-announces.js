'use strict';

module.exports = require('../doScript.js')({
  sqlUp: `

CREATE TABLE announces(
  id int(11) NOT NULL AUTO_INCREMENT,
  hUser int(11) DEFAULT NULL,
  url varchar(250),
  status varchar(20),
  hash varchar(20),
  createdAt datetime DEFAULT CURRENT_TIMESTAMP,
  updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id)
) ;

`,

  sqlDown: `
drop table announces;
`
});
