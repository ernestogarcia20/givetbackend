module.exports = require("../doScript.js")({
  sqlUp: `
  drop table if exists wallet;
  
  CREATE TABLE  wallet  (
   id  int(11) NOT NULL AUTO_INCREMENT,
   hUser  int(11) DEFAULT NULL,
   hRef  int(11) DEFAULT NULL,
   op  char(1) DEFAULT NULL,
   reason  varchar(45) DEFAULT NULL,
   description  varchar(45) DEFAULT NULL,
   amount  decimal(17,8) DEFAULT NULL,
   localBalance  decimal(17,8) DEFAULT NULL,
   balance  decimal(17,8) DEFAULT NULL,
   hParent  int(11) DEFAULT NULL,
   createdAt  datetime DEFAULT CURRENT_TIMESTAMP,
   updatedAt  datetime DEFAULT CURRENT_TIMESTAMP,
   freedAt  datetime DEFAULT NULL,
   ticketAmount  decimal(17,8) DEFAULT NULL,
  PRIMARY KEY ( id )
);
  
  `,
  sqlDown: `
  drop table if exists wallet;
  `
});
