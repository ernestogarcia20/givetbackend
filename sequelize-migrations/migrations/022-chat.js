module.exports = require("../doScript.js")({
  sqlUp: `
  alter table chat add column fileName varchar(100);

  `,
  sqlDown: `
  alter table chat drop column fileName;
  `
});
