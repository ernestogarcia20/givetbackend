module.exports = require("../doScript.js")({
  sqlUp: `
    
  alter table wallet add column hTicket int(11);

  `,
  sqlDown: `
  alter table wallet drop column hTicket;
  `
});
