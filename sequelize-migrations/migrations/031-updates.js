'use strict';

module.exports = require('../doScript.js')({
  sqlUp: `
  alter table business 
drop column gvtToComsumtion, 
drop column comsumtionToGvt,
add column minConsumtionToReward decimal(6,2), 
add column gvtToUser decimal(6,2), 
add column gvtToSponsor decimal(6,2);
$$

alter table business add column countryCode varchar(5);
alter table users add column countryCode varchar(5);
$$

`,

  sqlDown: `
  
`
});
