'use strict';
const view_name = 'v_users';
const query = `select t.hParent, t.hLeft, t.hRight, u.* 
from users u join users_tree t on t.id = u.id`;
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(`CREATE OR REPLACE VIEW ${view_name} AS ${query}`);

  },

  down: async (queryInterface, Sequelize) => {

    return queryInterface.sequelize.query(`DROP VIEW ${view_name}`);

  }
};
