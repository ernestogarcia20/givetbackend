module.exports = require("../doScript.js")({
  sqlUp: `
  
  alter table config 
    add column gainPercentAfter int(5),
    add column minDirect10 int(5),
    add column minDirect20 int(5),
    add column minDirect30 int(5);

  update config set gainPercentAfter=30, minDirect10=1, minDirect20=2, minDirect30=3 where id=1;

  DELIMITER $$
  drop procedure countChildrenWithPackage;
  $$
  CREATE PROCEDURE countChildrenWithPackage(phUser int)
  begin
    select count(*) as n from 
    (
    select hUser,count(*) from get_support
    where hUser in (select id from users_tree where hParent=phUser)
    group by hUser
    ) r;
  end$$
  DELIMITER;
  `,
  sqlDown: `
  alter table config 
    drop column gainPercentAfter, 
    drop column minDirect10, 
    drop column minDirect20, 
    drop column minDirect30;
      drop procedure countChildrenWithPackage;
  `
});
