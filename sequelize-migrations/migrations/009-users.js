module.exports = require("../doScript.js")({
  sqlUp: `
  ALTER TABLE users
  ADD COLUMN locked TINYINT NULL AFTER googleId;
  `,
  sqlDown: `
  alter table users drop column locked;
  `
});
