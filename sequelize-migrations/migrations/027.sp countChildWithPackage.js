module.exports = require("../doScript.js")({
  sqlUp: `
  drop procedure
  if exists countChildrenWithPackage;
  DELIMITER $$
  $$

  CREATE  PROCEDURE countChildrenWithPackage(phUser int)
  begin
      select count(*) as n from 
      (
      select hUser,count(*) from sell_tickets
      where hUser in (select id from users_tree where hParent=phUser)
      group by hUser
      ) r;
  end
  $$
  `,
  sqlDown: `
  
  `
});
