'use strict';

module.exports = require('../doScript.js')({
  sqlUp: `

drop table if exists users_tree;
drop procedure if exists admin_users_tree;
drop trigger if exists users_insert_tree;
drop trigger if exists users_delete_tree;

CREATE TABLE users_tree (
  id int(11) NOT NULL,
  hParent int(11) DEFAULT NULL,
  hLeft int(11) DEFAULT NULL,
  hRight int(11) DEFAULT NULL,
  primary key(id)
);

DELIMITER $$
$$
drop procedure if exists admin_users_tree;
$$
CREATE PROCEDURE admin_users_tree(
	in action varchar(10),
	in p_id int, 
	in p_hParent int)
BEGIN
	declare m_hr int;
    
	if action='add' then
		begin
			
			select hRight into m_hr FROM users_tree where id=p_hParent;
      if m_hr is null then
				select id, hRight into p_hParent, m_hr from users_tree where hParent is null LIMIT 1;
        if m_hr is null then
					set m_hr=1;
				end if;
      end if;
            
      insert into users_tree (id, hParent, hLeft, hRight ) values (p_id, p_hParent, 0, 0);
		
			update users_tree set hRight = hRight + 2 where hRight >= m_hr;
			update users_tree set hLeft = hLeft + 2 where hLeft > m_hr;
		
			update users_tree set hLeft=m_hr, hRight=m_hr+1 where id=p_id;
			
		end;
	end if;

	if action='delete' then
		delete from users_tree where id=p_id;
    end if;

 END;$$
DELIMITER ;


DELIMITER $$
$$
CREATE TRIGGER users_insert_tree
AFTER INSERT
ON users FOR EACH ROW
BEGIN
	declare hParent int(11);
	select id into hParent from users where userName=NEW.referralUserName;
	call admin_users_tree('add', NEW.id, hParent);
END$$
DELIMITER ;


DELIMITER $$
$$
CREATE TRIGGER users_delete_tree
AFTER DELETE
ON users FOR EACH ROW
BEGIN
	call admin_users_tree('delete', OLD.id, null);
END$$
DELIMITER ;


`,

  sqlDown: `
drop table if exists users_tree;

drop procedure if exists admin_users_tree;
drop trigger if exists users_insert_tree;
drop trigger if exists users_delete_tree;
`
});
