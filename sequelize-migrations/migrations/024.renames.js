module.exports = require("../doScript.js")({
  sqlUp: `
   rename table give_support to buy_tickets;
   rename table get_support to sell_tickets;
   rename table give_get to orders;

   alter table orders change column hGive hBuy int;
   alter table orders change column hGet hSell int;
   alter table orders change column giveRating buyRating smallint;
   alter table orders change column getRating sellRating smallint;
  `,
  sqlDown: `  
   rename table buy_tickets to give_support;
   rename table sell_tickets to get_support;
   rename table orders to give_get;

   alter table give_get change column hBuy hGive int;
   alter table give_get change column hSell hGet int;
   alter table orders change column buyRating giveRating smallint;
   alter table orders change column sellRating getRating smallint;
  `
});
