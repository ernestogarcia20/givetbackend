module.exports = require("../doScript.js")({
  sqlUp: `
  drop table if exists settings;

  create table settings(
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(20),
    type varchar(20),
    value varchar(100),
    description varchar(250),
    createdAt datetime DEFAULT CURRENT_TIMESTAMP,
    updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
  );

  insert into settings (id, name, type, value, description)
  values  (1,'gainPercent','number',50,null),
          (2,'timeToConfirmOrder','number',48,null),
          (3,'expirationDays','number',10,null),
          (4,'payToUnlock','number',0.0096, null),
          (5,'minSell','number',0.0048,null),
          (6,'payToUnlockAddress','string','btcbtcbtcbtc',null),
          (7, 'facebookUrl', 'string', 'facebook.com/givet', 'givets fb Home Page'),
          (8, 'hastags', 'string', '#qwerty', 'network hashtags'),
          (9, 'wHastag', 'string', '#qwerty', 'weekly tag');
          
  `,
  sqlDown: `
  
  `
});
