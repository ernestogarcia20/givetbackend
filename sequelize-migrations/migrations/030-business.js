'use strict';

module.exports = require('../doScript.js')({
  sqlUp: `
  alter table business add column status varchar(20) null default 'Pending';
`,

  sqlDown: `
  alter table business drop column status;
`
});
