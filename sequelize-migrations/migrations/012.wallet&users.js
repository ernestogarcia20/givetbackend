module.exports = require("../doScript.js")({
  sqlUp: `
    
alter table wallet add column status varchar(10);
alter table users_tree add column children int default 0;
  

DROP procedure IF EXISTS admin_users_tree;

DELIMITER $$

CREATE PROCEDURE admin_users_tree(
	in action varchar(10),
	in p_id int, 
	in p_hParent int)
BEGIN
	declare m_hr int;
    
	if action='add' then
		begin
			
			select hRight into m_hr FROM users_tree where id=p_hParent;
			if m_hr is null then
				select id, hRight into p_hParent, m_hr from users_tree where hParent is null LIMIT 1;
				if m_hr is null then
					set m_hr=1;
				end if;
			end if;
				
			insert into users_tree (id, hParent, hLeft, hRight ) values (p_id, p_hParent, 0, 0);
		
			update users_tree set hRight = hRight + 2 where hRight >= m_hr;
			update users_tree set hLeft = hLeft + 2 where hLeft > m_hr;
		
			update users_tree set hLeft=m_hr, hRight=m_hr+1 where id=p_id;
            
      update users_tree set children=children+1 where id=p_hParent;
				
		end;
	end if;

	if action='delete' then
		delete from users_tree where id=p_id;
  end if;

END$$


CREATE OR REPLACE 
VIEW v_users AS
    select 
        t.hParent AS hParent,
        t.hLeft AS hLeft,
        t.hRight AS hRight,
        t.children As children,
        u.id AS id,
        u.email AS email,
        u.userName AS userName,
        u.fullName AS fullName,
        u.hCountry AS hCountry,
        u.phone AS phone,
        u.password AS password,
        u.password2 AS password2,
        u.referralUserName AS referralUserName,
        u.btc AS btc,
        u.phoneOk AS phoneOk,
        u.emailOk AS emailOk,
        u.verifyToken AS verifyToken,
        u.accessPin AS accessPin,
        u.avatar AS avatar,
        u.rating AS rating,
        u.auth0Id AS auth0Id,
        u.googleId AS googleId,
        u.createdAt AS createdAt,
        u.updatedAt AS updatedAt
    from
        (users u
        join users_tree t ON ((t.id = u.id)))
  $$


  `,
  sqlDown: `
  alter table wallet drop column status;
  alter table users_tree drop column children;
  DROP procedure IF EXISTS admin_users_tree;
  `
});
