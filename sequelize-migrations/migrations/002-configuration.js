module.exports = require('../doScript.js')({
  sqlUp: `
  CREATE TABLE gain_level(
    id INT(11) NOT NULL AUTO_INCREMENT,
    level smallint NULL,
    percent smallint NULL,
	directRequired INT NULL,
	createdAt datetime DEFAULT CURRENT_TIMESTAMP,
	updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(id));
	
	CREATE TABLE packs(
	  id INT(11) NOT NULL AUTO_INCREMENT,
	  packName varchar(45) NULL,
	  price float NULL DEFAULT 0,
	  tokens float NULL DEFAULT 0,
	  createdAt datetime DEFAULT CURRENT_TIMESTAMP,
	  updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
	  PRIMARY KEY(id));

  `,
  sqlDown: `
  drop table gain_level;
  drop table packs;
  `
});
