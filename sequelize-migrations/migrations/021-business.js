'use strict';

module.exports = require('../doScript.js')({
  sqlUp: `

CREATE TABLE business_category(
  id int(11) NOT NULL AUTO_INCREMENT,
  categoryName varchar(45) DEFAULT NULL,
  createdAt datetime DEFAULT CURRENT_TIMESTAMP,
  updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id)
) ;


CREATE TABLE business(
  id int(11) NOT NULL AUTO_INCREMENT,
  hCategory int(11) DEFAULT NULL,
  businessName varchar(50),
  hUser int(11) DEFAULT NULL,
  openHour varchar(5),
  closeHour varchar(5),
  gvtToComsumtion decimal(6,2),
  comsumtionToGvt decimal(6,2),
  createdAt datetime DEFAULT CURRENT_TIMESTAMP,
  updatedAt datetime DEFAULT CURRENT_TIMESTAMP,
  address varchar(250),
  latitude varchar(20),
  longitude varchar(20),
  PRIMARY KEY(id)
) ;

`,

  sqlDown: `
drop table business_category;
drop table business;
`
});
