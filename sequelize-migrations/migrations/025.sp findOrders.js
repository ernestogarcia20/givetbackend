module.exports = require("../doScript.js")({
  sqlUp: `
  drop procedure if exists findOrders;
  DELIMITER $$
  $$

  CREATE PROCEDURE findOrders(phUser int)
  BEGIN
  select orders.*,
      buy.hUser as hBuyUser,sell.hUser as hSellUser, 
      giveUser.fullName as buyUserName, getUser.fullName as sellUserName,
      giveUser.rating as buyUserRating, getUser.rating as sellUserRating
  from orders orders
  join buy_tickets buy on buy.id = orders.hBuy
  join sell_tickets sell on sell.id = orders.hSell
  join users giveUser on giveUser.id = buy.hUser
  join users getUser on getUser.id = sell.hUser
  where giveUser.id = phUser or getUser.id = phUser;
  END
  $$
  `,
  sqlDown: `
  
  `
});
