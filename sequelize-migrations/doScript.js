/*
npx sequelize-cli migration:generate --name <unNombre>
npx sequelize-cli db:migrate
npx sequelize-cli db:migrate:undo
npx sequelize-cli db:migrate:undo:all

La migración generada, debe tener la forma:

module.exports = require('../doScript.js')({
      sqlUp: `aquí el script de upgrade`,
      sqlDown:`aquí el script de donwgrade`
});
*/

const delimiter = '$$';

var sqlUp = '';
var sqlDown = '';

const migrator = {
  up: async (queryInterface, Sequelize) => {
    var q = sqlUp.split(delimiter);
    for (var i = 0; i < q.length; i++) {
      var r = q[i].replace(/delimiter/ig, '').trim();
      if (r.length < 3 || r == ';') {
        continue;
      }
      //console.log(r);
      try {
        /*var x = await queryInterface.sequelize.query(r, {
          type: Sequelize.QueryTypes.RAW
        });*/
        var x = await queryInterface.sequelize.query(r);
      } catch (err) {
        console.log('up', err);
        console.log(r);
      }
    };
  },
  down: async (queryInterface, Sequelize) => {
    var q = sqlDown.split(delimiter);
    for (var i = 0; i < q.length; i++) {
      var r = q[i].replace(/delimiter/ig, '').trim();
      if (r.length < 3 || r == ';') {
        continue;
      }
      //console.log(r);
      try {
        /*var x = await queryInterface.sequelize.query(r, {
          type: Sequelize.QueryTypes.RAW
        });*/
        var x = await queryInterface.sequelize.query(r);
      } catch (err) {
        console.log('down', err);
      }
    };
  }
}

module.exports = function (options) {
  sqlUp = options.sqlUp;
  sqlDown = options.sqlDown;
  return migrator;
}
