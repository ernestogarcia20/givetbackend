'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.bulkInsert('gain_level', [{
        level: 1,
        percent: 10,
        directRequired: 1
      }, {
        level: 2,
        percent: 4,
        directRequired: 2
      }, {
        level: 3,
        percent: 4,
        directRequired: 3
      }, {
        level: 4,
        percent: 2,
        directRequired: 4
      }, {
        level: 5,
        percent: 2,
        directRequired: 5
      }, {
        level: 6,
        percent: 2,
        directRequired: 6
      }, {
        level: 7,
        percent: 2,
        directRequired: 7
      }, {
        level: 8,
        percent: 2,
        directRequired: 8
      }, {
        level: 9,
        percent: 2,
        directRequired: 9
      }, {
        level: 10,
        percent: 2,
        directRequired: 10
      }

    ], {});

  },

  down: async (queryInterface, Sequelize) => {

    await queryInterface.bulkDelete('gain_level', {}, {});

  }
};
