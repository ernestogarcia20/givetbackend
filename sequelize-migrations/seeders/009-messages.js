'use strict';

function rest(dt, n) {
  var d = new Date(dt);
  d.setDate(d.getDate() - n);
  return d;
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    var d = new Date();

    await queryInterface.bulkInsert('messages', [{
        hUserTo: 1,
        message: 'test',
        createdAt: d,
        updatedAt: d
      },
      {
        hUserTo: 1,
        message: 'aviso 2',
        createdAt: d,
        updatedAt: d
      }, {
        hUserTo: 1,
        message: 'aviso 3',
        createdAt: d,
        updatedAt: d
      },

    ], {});

  },

  down: async (queryInterface, Sequelize) => {

    await queryInterface.bulkDelete('messages', {}, {});

  }
};
