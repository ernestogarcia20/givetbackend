'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.bulkInsert('users', [{
      id: 1,
      userName: 'admin',
      password: '123',
      password2: '123',
      fullName: 'Administrador',
      email: 'admin@globalpyme.com',
      phone: '123'
    }], {});

    var userCount = 1;

    var users = [];
    for (var i = 0; i < 10; i++) {
      userCount++;
      var userI = {
        id: userCount,
        userName: `user ${i}`,
        password: '123',
        password2: '123',
        fullName: `user ${i}`,
        email: `user ${i}`,
        phone: `123 ${i}`,
        referralUserName: 'admin'
      }
      users.push(userI);
      for (var j = 0; j < 5; j++) {
        userCount++;
        var userJ = {
          id: userCount,
          userName: `user ${i}-${j}`,
          password: '123',
          password2: '123',
          fullName: `user ${i}-${j}`,
          email: `user ${i}-${j}`,
          phone: `123 ${i}-${j}`,
          referralUserName: userI.userName
        }
        users.push(userJ);
        for (var k = 0; k < 5; k++) {
          userCount++;
          var userK = {
            id: userCount,
            userName: `user ${i}-${j}-${k}`,
            password: '123',
            password2: '123',
            fullName: `user ${i}-${j}-${k}`,
            email: `user ${i}-${j}-${k}`,
            phone: `123 ${i}-${j}-${k}`,
            referralUserName: userJ.userName
          }
          users.push(userK);
        }
      }
    }

    await queryInterface.bulkInsert('users', users, {});
  },

  down: async (queryInterface, Sequelize) => {

    await queryInterface.bulkDelete('users', {}, {});

  }
};
