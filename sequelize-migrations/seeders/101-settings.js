'use strict';
const fs = require('fs');
const path = require('path');

const app = require('../../src/app');
const sequelize = app.get('sequelizeClient');
// run setup to initialize relations
app.setup();

var settingsService = app.service('api/settings');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await clear(queryInterface);
    await settingsService.create({
      name: 'w-hastag',
      value: '#abcde',
      type: 'string',
      description: 'weekly tag'
    })


  },

  down: async (queryInterface, Sequelize) => {
    return await clear(queryInterface);
  }
};

async function clear(queryInterface) {
  var rs = await settingsService.find({
    query: {
      name: 'w-hastag'
    }
  });
  if (rs.total > 0) {
    await settingsService.remove(rs.data[0].id);
  }
}
