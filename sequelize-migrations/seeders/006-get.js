'use strict';

function rest(dt, n) {
  var d = new Date(dt);
  d.setDate(d.getDate() - n);
  return d;
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    var d = new Date();

    await queryInterface.bulkInsert('get_support', [{
        hUser: 1,
        amount: 0.01,
        balance: 0.01,
        status: 'Pending',
        createdAt: rest(d, 5),
        updatedAt: rest(d, 5)
      },
      {
        hUser: 1,
        amount: 0.03,
        balance: 0.03,
        status: 'Pending',
        createdAt: rest(d, 1),
        updatedAt: rest(d, 1)
      }, {
        hUser: 1,
        amount: 0.03,
        balance: 0.03,
        status: 'Pending',
        createdAt: d,
        updatedAt: d
      }, {
        hUser: 2,
        amount: 0.003,
        balance: 0.003,
        status: 'Pending',
        createdAt: d,
        updatedAt: d
      }, {
        hUser: 2,
        amount: 0.005,
        balance: 0.005,
        status: 'Pending',
        createdAt: d,
        updatedAt: d
      }, {
        hUser: 3,
        amount: 0.006,
        balance: 0.006,
        status: 'Pending',
        createdAt: d,
        updatedAt: d
      }

    ], {});

  },

  down: async (queryInterface, Sequelize) => {

    await queryInterface.bulkDelete('get_support', {}, {});

  }
};
