'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.bulkInsert('packs', [{
        packName: 'pack 1',
        price: 0.0048,
        tokens: 1
      },
      {
        packName: 'pack 2',
        price: 0.0092,
        tokens: 1
      },
      {
        packName: 'pack 3',
        price: 0.02,
        tokens: 1
      },
      {
        packName: 'pack 4',
        price: 0.03,
        tokens: 1
      },
      {
        packName: 'pack 5',
        price: 0.04,
        tokens: 1
      },
      {
        packName: 'pack 6',
        price: 0.05,
        tokens: 1
      },
      {
        packName: 'pack 7',
        price: 0.072,
        tokens: 1
      },
      {
        packName: 'pack 8',
        price: 0.1,
        tokens: 1
      },
      {
        packName: 'pack 9',
        price: 0.14,
        tokens: 1
      },
      {
        packName: 'pack 10',
        price: 0.24,
        tokens: 1
      },
      {
        packName: 'pack 11',
        price: 0.3,
        tokens: 1
      },
      {
        packName: 'pack 12',
        price: 0.48,
        tokens: 1
      }

    ], {});

  },

  down: async (queryInterface, Sequelize) => {

    await queryInterface.bulkDelete('packs', {}, {});

  }
};
