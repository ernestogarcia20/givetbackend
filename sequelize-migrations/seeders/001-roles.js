'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.bulkInsert('roles', [{
      id: 1,
      roleName: 'Admininstrador',
      permissions: 'All'
    }], {});

  },

  down: async (queryInterface, Sequelize) => {

    await queryInterface.bulkDelete('roles', {
      id: 1
    }, {});

  }
};
