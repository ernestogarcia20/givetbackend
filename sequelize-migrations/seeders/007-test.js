'use strict';

function rest(dt, n) {
  var d = new Date(dt);
  d.setDate(d.getDate() - n);
  return d;
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    var d = new Date();

    //user 0 id=2, user 1 id=33, user 2 id=64

    await queryInterface.bulkInsert('give_support', [{
        hUser: 2,
        amount: 0.25,
        balance: 0.25,
        status: 'Pending',
        createdAt: rest(d, 5),
        updatedAt: rest(d, 5)
      },
      {
        hUser: 33,
        amount: 0.35,
        balance: 0.35,
        status: 'Pending',
        createdAt: rest(d, 5),
        updatedAt: rest(d, 5)
      }

    ], {});

    await queryInterface.bulkInsert('get_support', [{
      hUser: 64,
      amount: 0.5,
      balance: 0.5,
      status: 'Pending',
      createdAt: rest(d, 5),
      updatedAt: rest(d, 5)
    }], {});
  },

  down: async (queryInterface, Sequelize) => {

    await queryInterface.bulkDelete('give_support', {}, {});
    await queryInterface.bulkDelete('get_support', {}, {});
    await queryInterface.bulkDelete('give_get', {}, {});
  }
};
