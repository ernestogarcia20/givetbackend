'use strict';
const fs = require('fs');
const path = require('path');

const app = require('../../src/app');
const sequelize = app.get('sequelizeClient');
// run setup to initialize relations
app.setup();

function rest(dt, n) {
  var d = new Date(dt);
  d.setDate(d.getDate() - n);
  return d;
}
var orderService = app.service('api/orders');
var giveService = app.service('api/give_support');
var getService = app.service('api/get_support');
var walletService = app.service('api/wallet');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await clear(queryInterface);

    var buffer, data;
    buffer = fs.readFileSync(path.join(__dirname, '../data-test/chat.json')).toString();
    data = JSON.parse(buffer.toString())
    await sequelize.queryInterface.bulkInsert('chat', data);

    buffer = fs.readFileSync(path.join(__dirname, '../data-test/get-support.json')).toString();
    data = JSON.parse(buffer.toString())
    data.map(function (r) {
      setNullDate(r, 'finishAt');
    })
    await sequelize.queryInterface.bulkInsert('get_support', data);

    buffer = fs.readFileSync(path.join(__dirname, '../data-test/give-get.json')).toString();
    data = JSON.parse(buffer.toString());
    data.map(function (r) {
      setNullDate(r, 'voucher_receivedAt');
      setNullDate(r, 'voucher_mustCloseAt');
    })
    await sequelize.queryInterface.bulkInsert('give_get', data);

    buffer = fs.readFileSync(path.join(__dirname, '../data-test/give-support.json')).toString();
    data = JSON.parse(buffer.toString())
    await sequelize.queryInterface.bulkInsert('give_support', data);

    buffer = fs.readFileSync(path.join(__dirname, '../data-test/wallet.json')).toString();
    data = JSON.parse(buffer.toString())
    data.map(function (r) {
      setNullDate(r, 'freedAt');
    })
    await sequelize.queryInterface.bulkInsert('wallet', data);

    return;
    var packs = await app.api.service('packs').find({
      query: {
        packName: 'pack 3'
      },
      paginate: false
    });

    //user 0 id=2, user 1 id=33, user 2 id=64, user 0-0-4 id=8
    var give_1 = await orderService.create({
      hUser: 8,
      pack: packs[0].id,
      type: "give"
    });

    var get_1 = await orderService.create({
      amount: 0.05,
      hUser: 33,
      receiveEfective: true,
      receiveToken: true,
      type: "get"
    });

  },

  down: async (queryInterface, Sequelize) => {
    return await clear(queryInterface);
  }
};

async function clear(queryInterface) {
  await queryInterface.bulkDelete('give_get', {}, {});
  await queryInterface.bulkDelete('give_support', {}, {});
  await queryInterface.bulkDelete('get_support', {}, {});
  await queryInterface.bulkDelete('wallet', {}, {});
  await queryInterface.bulkDelete('chat', {}, {});
}

function setNullDate(obj, fieldName) {
  if (obj[fieldName] == "") {
    obj[fieldName] = null;
  }
}
async function confirmarOrden() {
  var giveget = await app.service('api/give_get').find();
  for (var i = 0; i < giveget.length; i++) {
    var o = giveget[i];

    await giveget.patch(o.id, {
      $action: "sendVoucher",
      rating: 5
    });

    return;

    await giveget.patch(o.id, {
      $action: "verifyVoucher",
      rating: 5
    });
  }
}

//module.exports.up(app.get('sequelizeClient').queryInterface);
