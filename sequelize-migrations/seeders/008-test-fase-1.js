'use strict';
const app = require('../../src/app');
// run setup to initialize relations
app.setup();

const sequelize = app.get('sequelizeClient');
const emparejador = app.get('emparejador');


function rest(dt, n) {
  var d = new Date(dt);
  d.setDate(d.getDate() - n);
  return d;
}
var orderService = app.service('api/orders');
var giveService = app.service('api/give_support');
var getService = app.service('api/get_support');
var giveget = app.service('/api/give_get');
var walletService = app.service('api/wallet');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await clear(queryInterface);

    var saldo = {
      op: '+',
      reason: 'INITIAL_BALANCE',
      amount: 0.24,
      localBalance: 0.24,
      ticketAmount: 0.24,
      status: 'Available',
      available: 0.24,
      freedAt: new Date(2019, 10, 1), //ojo es base 0
    }
    var users = await app.service('api/users').find({
      paginate: false
    });
    /* for (var i = 0; i < users.length; i++) {
      console.log('*******', i, users[i].id)
      saldo.hUser = users[i].id;
      await walletService.create(saldo);
    }
*/
    //user 0 id=2, user 1 id=33, user 2 id=64, user 0-0-4 id=8
    await walletService.create(Object.assign(saldo, {
      hUser: 2
    }));
    await walletService.create(Object.assign(saldo, {
      hUser: 33
    }));
    await walletService.create(Object.assign(saldo, {
      hUser: 64
    }));

    var packs = await app.api.service('packs').find({

      paginate: false
    });

    var now = new Date(Date.now() - 60000); //un minuto menos para forzar que entre en el emparejamiento

    console.log(now, new Date());

    //user 0 id=2, user 1 id=33, user 2 id=64, user 0-0-4 id=8
    var give_1 = await orderService.create({
      hUser: 33,
      pack: packs[7].id,
      type: "give",
      createdAt: now
    });

    var get_1 = await orderService.create({
      amount: packs[8].price,
      hUser: 64,
      receiveEfective: true,
      receiveToken: true,
      type: "get",
      createdAt: now
    });

    /*
    var giveget= await giveget.create({
      id:1, 
      hGive:give_1.id,
      hGet:get_1.id,
      amount:get_1.amount,
      status:'Finished',
      voucher_sendAt:new Date(),
      voucher_receivedAt=new Date(),
      mustCloseAt:new Date(),
      hChat:,
      giveRating:5,
      getRating:5

    })
*/
    //
    console.log("Esperando emparejamiento")

    await emparejador.emparejar();

    //await confirmarOrden();

  },

  down: async (queryInterface, Sequelize) => {
    return await clear(queryInterface);
  }
};

async function clear(queryInterface) {
  await queryInterface.bulkDelete('give_get', {}, {});
  await queryInterface.bulkDelete('give_support', {}, {});
  await queryInterface.bulkDelete('get_support', {}, {});
  await queryInterface.bulkDelete('wallet', {}, {});
  await queryInterface.bulkDelete('chat', {}, {});
  await sequelize.query('update users set locked=0');
  return;
}

async function confirmarOrden() {
  var giveget = await app.service('api/give_get').find();
  for (var i = 0; i < giveget.length; i++) {
    var o = giveget[i];

    await giveget.patch(o.id, {
      $action: "sendVoucher",
      rating: 5
    });

    return;

    await giveget.patch(o.id, {
      $action: "verifyVoucher",
      rating: 5
    });
  }
}

//module.exports.up(sequelize.queryInterface, sequelize);
