const assert = require('assert');
const app = require('../../src/app');

describe('\'giveSupport\' service', () => {
  it('registered the service', () => {
    const service = app.service('give_support');

    assert.ok(service, 'Registered the service');
  });
});
