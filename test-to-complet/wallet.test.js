const assert = require('assert');
const app = require('../../src/app');

describe('\'wallet\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/wallet');

    assert.ok(service, 'Registered the service');
  });
});
