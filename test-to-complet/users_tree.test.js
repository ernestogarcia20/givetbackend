const assert = require('assert');
const app = require('../../src/app');

describe('\'users_tree\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/users_tree');

    assert.ok(service, 'Registered the service');
  });
});
