const assert = require('assert');
const app = require('../../src/app');

describe('\'give_get\' service', () => {
  it('registered the service', () => {
    const service = app.service('give-get');

    assert.ok(service, 'Registered the service');
  });
});
