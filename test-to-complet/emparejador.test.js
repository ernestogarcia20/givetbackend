const assert = require('assert');
const app = require('../../src/app');

describe('\'emparejador\' service', () => {
  it('registered the service', () => {
    const service = app.service('emparejador');

    assert.ok(service, 'Registered the service');
  });
});
