const assert = require('assert');
const app = require('../../src/app');

describe('\'profit_level\' service', () => {
  it('registered the service', () => {
    const service = app.service('profit_level');

    assert.ok(service, 'Registered the service');
  });
});
