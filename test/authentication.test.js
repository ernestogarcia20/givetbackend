const assert = require("assert");
const app = require("../src/app");
const addContext = require("mochawesome/addContext");

describe("authentication", function() {
  var userInfo = {
    userName: "someone",
    email: "someone@example.com",
    password: "supersecret",
    phone: "58-123",
    //required
    fullName: "someone",
    password2: "supersecret2"
  };

  before(async () => {
    try {
      await app.service("/api/users").create(userInfo);
    } catch (error) {
      // Do nothing, it just means the user already exists and can be tested
      console.log(error);
    }
  });

  it("registered the authentication service", function() {
    assert.ok(app.service("/api/authentication"));
  });

  describe("local-email strategy", function() {
    it("authenticates user and creates accessToken", async function() {
      userInfo = {
        email: "someone@example.com",
        password: "supersecret"
      };
      const { user, accessToken } = await app
        .service("/api/authentication")
        .create({
          strategy: "local-email",
          ...userInfo
        });

      assert.ok(accessToken, "Created access token for user");
      assert.ok(user, "Includes user in authentication data");

      addContext(this, { title: "accessToken", value: accessToken });
      addContext(this, { title: "user", value: user });
    });
  });

  describe("local-username strategy", () => {
    it("authenticates user and creates accessToken", async () => {
      userInfo = {
        userName: "someone",
        password: "supersecret"
      };
      const { user, accessToken } = await app
        .service("/api/authentication")
        .create({
          strategy: "local-username",
          ...userInfo
        });

      assert.ok(accessToken, "Created access token for user");
      assert.ok(user, "Includes user in authentication data");
    });
  });

  describe("local-phone strategy", () => {
    it("authenticates user and creates accessToken", async () => {
      userInfo = {
        phone: "58-123",
        password: "supersecret"
      };
      const { user, accessToken } = await app
        .service("/api/authentication")
        .create({
          strategy: "local-phone",
          ...userInfo
        });

      assert.ok(accessToken, "Created access token for user");
      assert.ok(user, "Includes user in authentication data");
    });
  });
});
