const assert = require('assert');
const app = require('../../src/app');

describe('\'business_scores\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/business_scores');

    assert.ok(service, 'Registered the service');
  });
});
