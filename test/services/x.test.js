const assert = require('assert');
const app = require('../../src/app');

describe('\'X\' service', () => {
  it('registered the service', () => {
    const service = app.service('x');

    assert.ok(service, 'Registered the service');
  });
});
