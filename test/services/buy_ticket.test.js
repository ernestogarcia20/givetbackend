const assert = require("assert");
const app = require("../../src/app");
const addContext = require("mochawesome/addContext");

const utils = require("../../test-utils");
const client = utils.clients.makeClient({
  transport: "rest"
});

describe("buy_ticket service", () => {
  //autenticate
  before(async () => utils.loginAdmin(client));

  it("registered the service", () => {
    const service = client.service("/api/buy_ticket");

    assert.ok(service, "Registered the service");
  });

  describe("find", function () {
    var sample = {
      id: 0
    };
    before(async () => {
      try {
        sample = (await app.service("/api/buy_ticket").find()).data[0];
      } catch (err) {
        sample = {
          id: 0
        };
      }
    });

    it("get", async function () {
      var ticket;
      try {
        ticket = await client.service("api/buy_ticket").get(sample.id);
      } catch (err) {
        ticket = err;
      }

      addContext(this, {
        title: "ticket",
        value: ticket
      });
    });

    it("find", async function () {
      var rs = await client.service("api/buy_ticket").find({
        query: {
          hUser: 1
        }
      });

      addContext(this, {
        title: "rs",
        value: rs
      });
    });
  });

  describe("create", function () {
    it("created ticket", async function () {
      var ticket = await client.service("api/buy_ticket").create({
        hUser: 1,
        hPack: 1,
        receiveEfective: true,
        receiveToken: true
      });

      addContext(this, {
        title: "buy_ticket",
        value: ticket
      });
    });
  });
});
