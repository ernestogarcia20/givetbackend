const assert = require('assert');
const app = require('../../src/app');

describe('\'system\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/system');

    assert.ok(service, 'Registered the service');
  });
});
