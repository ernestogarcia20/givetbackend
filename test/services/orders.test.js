const assert = require("assert");
const app = require("../../src/app");
const addContext = require("mochawesome/addContext");

const utils = require("../../test-utils");
const client = utils.clients.makeClient({
  transport: "rest"
});

var service = client.service("/api/orders");

describe("orders service", () => {
  //autenticate
  before(async () => utils.loginAdmin(client));

  it("registered the service", () => {
    const service = client.service("/api/orders");

    assert.ok(service, "Registered the service");
  });

  describe("find", function () {
    var sample = {
      id: 0
    };
    before(async () => {
      try {
        sample = (await app.service("/api/orders").find()).data[0];
      } catch (err) {
        console.log('****************', err)
        sample = {
          id: 0
        };
      }
    });

    it("get", async function () {

      var order = await service.get(sample.id);

      addContext(this, {
        title: "order",
        value: order
      });
    });

    it("find", async function () {
      var order;
      order = await service.find();

      addContext(this, {
        title: "order",
        value: order
      });
    });


  });


});
