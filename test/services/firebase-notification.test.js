const assert = require('assert');
const app = require('../../src/app');

describe('\'FirebaseNotification\' service', () => {
  it('registered the service', () => {
    const service = app.service('firebase-notification');

    assert.ok(service, 'Registered the service');
  });
});
