const assert = require("assert");
const app = require("../../src/app");
const addContext = require("mochawesome/addContext");

const utils = require('../../test-utils');
const client = utils.clients.makeClient({
  transport: 'rest'
});


describe("users service", () => {

  var userInfo = {
    userName: "someone",
    email: "someone@example.com",
    password: "supersecret",
    phone: "58-123",
    //required
    fullName: "someone",
    password2: "supersecret2"
  };

  //autenticate
  before(async () => utils.loginAdmin(client));

  //delete if exist to prepare for update and delete
  before(async () => {
    try {
      var rs = await app.service("/api/users").find({
        query: {
          userName: userInfo.userName
        }
      });
      var user = rs.data[0];
      await app.service("/api/users").remove(user.id);
    } catch (error) {
      console.log(error);
    }
  });


  it("registered the service", () => {
    const service = client.service("/api/users");

    assert.ok(service, "Registered the service");
  });

  describe("find users", function () {
    it("get", async function () {
      var userId = 1;
      var user = await client.service('api/users').get(userId)

      addContext(this, {
        title: "user",
        value: user
      });
    })

    it("find", async function () {
      var rs = await client.service('api/users').find({
        query: {
          referralUserName: 'admin'
        }
      })

      addContext(this, {
        title: "rs",
        value: rs
      });
    })

  })

  describe("create user", function () {
    it("created user", async function () {
      var user = await client.service("api/users").create({
        userName: "someone",
        email: "someone@example.com",
        password: "supersecret",
        phone: "58-123",
        //required
        fullName: "someone",
        password2: "supersecret2"
      });

      addContext(this, {
        title: "user",
        value: user
      });
    });
  });

  describe("update user", function () {
    it("updated user", async function () {
      const userId = 1;
      var user = await client.service("api/users").patch(userId, {
        btc: "123456789asdfasfd"
      });

      addContext(this, {
        title: "user",
        value: user
      });
    });
  });

  describe("delete user", function () {

    before(async () => {
      try {
        userInfo = await app.service("/api/users").create(userInfo);
      } catch (error) {
        userInfo = (
          await app.service("/api/users").find({
            query: {
              userName: userInfo.userName
            }
          })
        ).data[0];
      }
    });

    it("deleted user", async function () {

      const userId = userInfo.id;

      var ret = await client.service("api/users").remove(userId);

      addContext(this, {
        title: "ret",
        value: ret
      });
    });
  });



});
