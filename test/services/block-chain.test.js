const assert = require('assert');
const app = require('../../src/app');

describe('\'block-chain\' service', () => {
  it('registered the service', () => {
    const service = app.service('block-chain');

    assert.ok(service, 'Registered the service');
  });
});
