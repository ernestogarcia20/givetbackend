const assert = require("assert");
const app = require("../../src/app");
const addContext = require("mochawesome/addContext");

const utils = require("../../test-utils");
const client = utils.clients.makeClient({
  transport: "rest"
});

describe("sell_ticket service", () => {
  //autenticate
  before(async () => utils.loginAdmin(client));

  it("registered the service", () => {
    const service = client.service("/api/sell_ticket");

    assert.ok(service, "Registered the service");
  });

  describe("find", function () {
    var sample = {
      id: 0
    };
    before(async () => {
      try {
        sample = (await app.service("/api/sell_ticket").find()).data[0];
      } catch (err) {
        sample = {
          id: 0
        };
      }
    });

    it("get", async function () {
      var ticket;
      try {
        ticket = await client.service("api/sell_ticket").get(sample.id);
      } catch (err) {
        ticket = err;
      }

      addContext(this, {
        title: "ticket",
        value: ticket
      });
    });

    it("find", async function () {
      var rs = await client.service("api/sell_ticket").find({
        query: {
          hUser: 1
        }
      });

      addContext(this, {
        title: "rs",
        value: rs
      });
    });
  });

  describe("create", function () {
    var user;
    before(async () => {
      try {
        user = (await app.service("/api/users").find()).data[1];
      } catch (err) {
        user = {
          id: 1
        };
      }
    });

    it("created ticket receiveToken=false", async function () {
      var ticket = await client.service("api/sell_ticket").create({
        hUser: user.id,
        amount: 0.5,
        password2: user.password2,
        receiveEfective: true,
        receiveToken: false
      });

      addContext(this, {
        title: "sell_ticket",
        value: ticket
      });
    });

    it("created ticket receiveToken=true", async function () {
      var ticket = await client.service("api/sell_ticket").create({
        hUser: user.id,
        amount: 0.5,
        password2: user.password2,
        receiveEfective: true,
        receiveToken: true
      });

      addContext(this, {
        title: "sell_ticket",
        value: ticket
      });
    });
  });
});
