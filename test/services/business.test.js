const assert = require("assert");
const app = require("../../src/app");
const addContext = require("mochawesome/addContext");

const utils = require("../../test-utils");
const client = utils.clients.makeClient({
  transport: "rest"
});

var service = client.service("/api/business");

var newItem;

async function createNewItem() {
  try {
    client.service("/api/business_category").create({
      id: 1,
      categoryName: "cat X"
    });
  } catch {}

  var item = {
    hCategory: 1,
    hUser: 1,
    businessName: "someCat"
  };

  var rs = await service.find({
    query: item
  });
  if (rs.total > 0) {
    item = rs.data[0];
  } else {
    item = await service.create(item);
  }
  newItem = item;
  return item;
}

async function removeNewItem() {
  try {
    await service.remove(newItem.id);
  } catch (err) {}
}

describe("business service", () => {
  //autenticate
  before(async () => utils.loginAdmin(client));

  ///

  it("registered the service", () => {
    const srv = client.service("/api/business");

    assert.ok(srv, "Registered the service");
  });

  describe("find", function() {
    //busca o crea un item
    before(async () => {
      await createNewItem();
    });
    //remove si before creó el item
    after(async () => {
      await removeNewItem();
    });

    it("get", async function() {
      var rs = await service.get(newItem.id);

      addContext(this, {
        title: "rs",
        value: rs
      });
    });

    it("find", async function() {
      var rs = await service.find({
        query: {
          categoryName: newItem.categoryName
        }
      });

      addContext(this, {
        title: "rs",
        value: rs
      });
    });
  });

  describe("create", function() {
    var rs;
    after(async () => {
      await service.remove(rs.id);
    });

    it("created", async function() {
      rs = await service.create({
        hCategory: 1,
        hUser: 1,
        businessName: "new business"
      });

      addContext(this, {
        title: "rs",
        value: rs
      });
    });
  });

  describe("update", function() {
    before(async () => {
      await createNewItem();
    });
    after(async () => {
      await removeNewItem();
    });

    it("updated", async function() {
      var rs = await service.patch(newItem.id, {
        businessName: "updated business name"
      });

      addContext(this, {
        title: "rs",
        value: rs
      });
    });
  });

  describe("delete", function() {
    before(async () => {
      await createNewItem();
    });
    after(async () => {
      await removeNewItem();
    });

    it("deleted", async function() {
      var rs = await service.remove(newItem.id);

      addContext(this, {
        title: "rs",
        value: rs
      });
    });
  });
});
