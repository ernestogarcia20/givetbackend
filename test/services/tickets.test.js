const assert = require("assert");
const app = require("../../src/app");
const addContext = require("mochawesome/addContext");

const utils = require("../../test-utils");
const client = utils.clients.makeClient({
  transport: "rest"
});

describe("tickets service", () => {
  //autenticate
  before(async () => utils.loginAdmin(client));

  it("registered the service", () => {
    const service = client.service("/api/tickets");

    assert.ok(service, "Registered the service");
  });

  describe("find", function () {

    it("find", async function () {
      var rs = await client.service("api/tickets").find({
        query: {
          hUser: 1
        }
      });

      addContext(this, {
        title: "rs",
        value: rs
      });
    });
  });


});
