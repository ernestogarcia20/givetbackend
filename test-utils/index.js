require('./setup')
const {
  assert
} = require('feathers-mocha-utils')

module.exports = {
  clients: require('./clients'),
  assert,
  loginAdmin: loginAdmin,
}

async function loginAdmin(client) {
  return await client.authenticate({
    strategy: "local-username",
    userName: 'admin',
    password: '123'
  });
}
