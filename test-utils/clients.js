const feathers = require('@feathersjs/feathers')
const io = require('socket.io-client')
const axios = require('axios')
const rest = require('@feathersjs/rest-client')
const socketio = require('@feathersjs/socketio-client')
const auth = require('@feathersjs/authentication-client')

const host = 'http://localhost:3030'

function makeClient({
  transport = 'socketio'
}) {
  const feathersClient = feathers()

  if (transport === 'socketio') {
    var socket = io(host, {
      transports: ['websocket']
    })
    feathersClient.configure(socketio(socket, {
      timeout: 600000
    }))
  }

  if (transport === 'rest') {
    feathersClient.configure(rest(host).axios(axios))
  }

  feathersClient
    .configure(auth({
      path: "/api/authentication"
    }))

  return feathersClient
}

// Expose makeClient
module.exports.makeClient = makeClient
module.exports.makeClients = function () {
  return [
    makeClient({
      transport: 'socketio'
    }),
    makeClient({
      transport: 'rest'
    })
  ]
}
