// swager doc
const path = require("path");
const express = require("@feathersjs/express");

const sequelizeToJsonSchemas = require("./sequelize-to-json-schemas");
const swagger = require("feathers-swagger");

const version = "1.0.13";
module.exports = function (app) {
  app.configure(sequelizeToJsonSchemas);

  var swConfig = {
    openApiVersion: 3,
    //uiIndex: true,
    uiIndex: path.join(__dirname, "docs.html"),
    docsPath: "/docs",
    prefix: /api\//,
    docsJsonPath: "/docs/schema",
    include: {
      paths: ["users", "buy_ticket", "sell_ticket", "orders", "packs", "files", "business", "business_category", "chat", "settings", "wallet", "messages", "system", "balance", "announcements", "blockchain", "profit_level",
        "mailer", "authManagement", "ethereum"
      ],
      //models: ["users", "buy_ticket"]
    },

    ignore: {
      tags: ["ignore"],
      paths: ["authentication", "last_give", "give_support", "get_support", "give_get", "users_tree"]
    },
    models: [], // evita duplicidad en schemas ??
    specs: {
      info: {
        title: "GIVET API",
        version: version,
        description: `
* [marvel user app](https://marvelapp.com/j11hje4)
* [marvel admin app](https://marvelapp.com/6ffgb51)

* For questions, errors, problems should be made by [https://gitlab.com/Lapinzon/givet.-issue-tracking/issues](https://gitlab.com/Lapinzon/givet.-issue-tracking/issues)
`,
      },
      /*servers: [{
        url: 'http://173.249.40.233:3030/'
      }],*/

      components: {
        securitySchemes: {
          JWT: {
            type: "http",
            scheme: "bearer",
            bearerFormat: "JWT"
          }
        },
        schemas: {
          treeNode: {
            title: 'treeNode',
            type: 'object',
            properties: {
              "id": {
                "type": "integer",
                "format": "int32"
              },
              "hParent": {
                "type": "integer",
                "format": "int32"
              },
              userName: {
                type: 'string'
              },
              "fullName": {
                "type": "string"
              },
              "email": {
                "type": "string",
                "maxLength": 45
              },
              icon: {
                type: 'string'
              },
              label: {
                type: 'string'
              },
              children: {
                type: 'array',
                items: {
                  "$ref": "#/components/schemas/treeNode"
                }
              }
            },
          },
          tree: {
            type: 'array',
            items: {
              "$ref": "#/components/schemas/treeNode"
            }
          }
        }
      },
      paths: {
        "/api/authentication": {
          "post": {
            "parameters": [],
            "responses": {
              "201": {
                "content": {
                  "application/json": {
                    "schema": {
                      "type": "object",
                      "properties": {
                        "accessToken": {
                          "type": "string"
                        },
                        "user": {
                          "type": "object",
                          "$ref": "#/components/schemas/user_response"
                        }
                      }
                    }
                  }
                }
              },
              "401": {
                "description": "not authenticated"
              },
              "500": {
                "description": "general error"
              }
            },
            "description": "",
            "summary": "",
            "tags": [
              "authManagement"
            ],
            "security": [],
            "requestBody": {
              "content": {
                "application/json": {
                  "schema": {
                    "$ref": "#/components/schemas/email"
                  },
                  "examples": {
                    "default": {
                      "value": {
                        "strategy": "local-username",
                        "userName": "admin",
                        "password": "123"
                      }
                    }
                  }
                }
              }
            }
          }
        },
      },
      security: [{
        JWT: []
      }],
      tags: [{
          name: 'blockchain',
          description: 'api interface to blockchain',

        },
        {
          name: '_user:1mini.png',
          methods: ['users/{idUser}/split']
        },
        {
          name: '_user:23a.png',
          methods: [
            'users/{idUser}/messages',
            'users/{idUser}/balance',
            'users/{idUser}/transfer-balance'
          ]
        },
        {
          name: '_user:34.png',
          methods: [
            'users/{idUser}/messages',
            'users/{idUser}/balance',
            'users/{idUser}/orders'
          ]
        },
        {
          name: '_user:35.png',
          methods: [
            'users/{idUser}/messages',
            'messages'
          ]
        },
        {
          name: '_user:anunciar.png',
          methods: [
            'users/{idUser}/messages',
            'settings',
            'anouncements'
          ]
        },
        {
          name: '_user:chat user.png',
          methods: [
            'users/{idUser}/chat/{withUser}',
            'chat',
            'files'
          ]
        },
        {
          name: '_user:15.1.png',
          methods: [
            'users',
            'orders',
            'TODO'
          ]
        },
        {
          name: '_user:15.2.png',
          methods: [
            'users',
            'orders',
            'blockchain/blockexplorer/verify-hash',
            'TODO'
          ]
        },
        {
          name: '_user:15.3.png',
          methods: [
            'users',
            'orders',
            'TODO'
          ]
        },
        {
          name: '_user:15.4.png',
          methods: [
            'users',
            'orders',
            'TODO'
          ]
        },
        {
          name: '_user:15.5.png',
          methods: [
            'users/{id}',
            'orders',
            'TODO'
          ]
        },
        {
          name: '_user:pas2.png',
          methods: [
            'users/{idUser}/resend-password2',
            'TODO'
          ]
        },
        {
          name: '_user:businesscategory.png',
          methods: [
            'business_category'
          ]
        },
        {
          name: '_user:pay balance.png',
          methods: [
            'users/{idUser}/pay-unlock',
          ]
        },
        {
          name: '_user:business1.png',
          methods: [
            'business_category',
            'business',
            'files'
          ]
        },
        {
          name: '_user:11.png',
          methods: [
            'users',
          ]
        },
        {
          name: '_user:mybusiness.png',
          methods: [
            'users/{idUser}/business',
            'files'
          ]
        },
        {
          name: '_user:business2.png',
          methods: [
            'business_category',
            'business'
          ]
        },
        {
          name: '_user:ctoken.png',
          methods: [
            'users/{idUser}/transfer-balance',
          ]
        },
        {
          name: '_user:business description.png',
          methods: [
            'business',
            'files'
          ]
        },
        {
          name: '_user:6.png',
          methods: [
            'users',
            'users/{idUser}/balance',
            'users/{idUser}/orders'
          ]
        },
        {
          name: '_user:27.png',
          description: 'for chat support, chat.isSupport=1',
          methods: [
            'users/{idUser}/chat-support',
            'chat',
            'files'
          ]
        },
        {
          name: '_user:18.png',
          methods: [
            'users/{idUser}/balance',
            'users/{idUser}/messages',
            'packs',
            'buy_tickets',

          ]
        },
        {
          name: '_user:12.png',
          methods: [
            'settings',
          ]
        },
        {
          name: '_user:13.png',
          methods: [
            'users/{idUser}/tree-view',
          ]
        },
        {
          name: '_user:15.png',
          description: 'Verificar',
          methods: [
            'users',
            'orders',
            'blockchain/blockexplorer/verify-hash',
            'TODO'
          ]
        },
        {
          name: '_user:23.png',
          description: 'Verificar',
          methods: [
            'users/{idUser}/messages',
            'users/{idUser}/balance',
            'users/{idUser}/transfer-balance'
          ]
        },
        {
          name: '_user:19.png',
          methods: [
            'users/{idUser}/messages',
            'users/{idUser}/balance',
            'users/{idUser}/withdraw',
            'sell_ticket',

          ]
        },
        {
          name: '_user:20.png',
          methods: [
            'users/{idUser}/verify-password2',
          ]
        },
        {
          name: '_user:21.png',
          methods: [
            'users/{idUser}/tickets',
          ]
        },
        {
          name: '_user:22.png',
          methods: [
            'users/{idUser}/balance',
            'users/{idUser}/wallet',
          ]
        },
        {
          name: '_user:25.png',
          methods: [
            'users/{idUser}/tree-view',
          ]
        },
        {
          name: '_user:26.png',
          methods: [
            'users/{idUser}/tree-view',
          ]
        },
        {
          name: '_admin:1.png',
          description: `settings: buy-active, sell-active `,
          methods: [
            'settings',
            'system/balance'
          ]
        },
        {
          name: '_admin:2.png',
          description: `settings: buy-active, sell-active `,
          methods: [
            'settings',
            'system/balance'
          ]
        },
        {
          name: '_admin:4.png',
          description: `see /users filter sample for query`,
          methods: [
            'users',
          ]
        }, {
          name: '_admin:19.png',
          description: ``,
          methods: [
            'announcements',
          ]
        },
        {
          name: '_admin:19a.png',
          description: `set status to 'Approved' to validate, otherwise set 'Rejected'`,
          methods: [
            'announcements',
          ]
        },

        {
          name: '_admin:5.png',
          description: `see /users filter sample for query`,
          methods: [
            'users',
          ]
        },

        {
          name: '_admin:6.png',
          description: `see /users filter sample for query`,
          methods: [
            'users',
          ]
        },
        {
          name: '_admin:11.png',
          description: `tree.view with idUser=1`,
          methods: [
            'users/{idUser}/tree-view',
          ]
        },
        {
          name: '_admin:12.png',
          methods: [
            'profit_level',
            'settings',
            'packs'
          ]
        },
        {
          name: '_admin:13.png',
          description: ``,
          methods: [
            'chat',
          ]
        },
        {
          name: '_admin:13b.png',
          description: ``,
          methods: [
            'chat',
            'files'
          ]
        },
        {
          name: '_admin:14.png',
          description: ``,
          methods: [
            'wallet',
          ]
        },

        {
          name: '_admin:15.png',
          description: ``,
          methods: [
            'wallet',
          ]
        },
        {
          name: '_admin:7.png',
          description: ``,
          methods: [
            'business',
          ]
        },
        {
          name: '_admin:17.png',
          description: ``,
          methods: [
            'business',
            'files'
          ]
        },
        {
          name: '_admin:16.png',
          description: ``,
          methods: [
            'wallet',
          ]
        },
        {
          name: '_admin:20.png',
          description: `patch status to 'Arrived' or 'No Arrived' `,
          methods: [
            'wallet',
            'messages'
          ]
        },
        {
          name: '_admin:20b.png',
          description: `patch status to 'Arrived' or 'No Arrived' `,
          methods: [
            'wallet',
            'messages'
          ]
        },
        {
          name: '_admin:12a.png',
          description: ``,
          methods: [
            'business_category',
          ]
        },
      ],


    },
    defaults: {
      discoverTags(servicePath) {

        var swaggerPath = servicePath.replace(/\/:([^/]+)/g, (match, parameterName) => {
          return `/{${parameterName}}`;
        });

        if (swaggerPath.indexOf('users') > -1) {
          //console.log(servicePath)
        }

        var tags = [];
        swConfig.specs.tags.forEach(screen => {
          if (screen.methods && screen.methods.indexOf(swaggerPath) > -1) {
            tags.push(screen.name);
          }
        })
        return tags;
      },
      operations: {
        create: {
          description: ''
        },
        find: {
          'parameters[3]': {
            description: 'Query parameters to filter',
            in: 'query',
            name: 'filter',

            style: 'form',
            explode: true,

            schema: {
              type: 'object',
              //$ref: '#/components/schemas/user_input'
            }
          }
        },
        get: {
          description: ''
        },
        update: {
          description: ''
        },
        patch: {
          description: ''
        },
        remove: {
          description: ''
        }
      },
      getOperationArgs({
        service,
        apiPath,
        version
      }) {
        var q = swConfig;
        const group = apiPath.split('/');
        const tag = service.docs.tag ?
          service.docs.tag :
          (apiPath.indexOf('/') > -1 ? group[0] : apiPath) + (version ? ` ${version}` : '');
        var tags = service.docs.tags ? service.docs.tags : [tag];

        tags = tags.concat(this.discoverTags(apiPath));

        const model = service.docs.model ?
          service.docs.model :
          (apiPath.indexOf('/') > -1 ? group[1] : apiPath) + (version ? `_${version}` : '');
        const security = [];


        if (swConfig.specs.security && Array.isArray(swConfig.specs.security)) {
          swConfig.specs.security.forEach(function (schema) {
            security.push(schema);
          });
        }
        const securities = service.docs.securities || [];

        return {
          tag,
          tags,
          model,
          modelName: model,
          security,
          securities
        };
      },
      getOperationsRefs(model, service) {
        if (!service.docs || !service.docs.schemas) {
          return {};
        }
        var refs = {};

        //request
        var name = Object.keys(service.docs.schemas).find(e => {
          return e.indexOf('_create') > -1
        });
        if (name) {
          refs.createRequest = name;
          refs.updateRequest = name;
          refs.patchRequest = name;
        }
        return refs;
      },

      schemasGenerator(service, model, modelName, schemas) {
        if (!service.options || !service.options.Model) {
          return;
        }
        if (service.options.Model.name != model) {
          if (model.indexOf('_response') > 0) {
            return {
              [`${model}_list`]: {
                title: `${model} list`,
                type: "array",
                items: {
                  $ref: `#/components/schemas/${model}`
                }
              }
            };
          }
          //return;
        }

        const modelSchema = app
          .get("jsonSchemaManager")
          .generate(
            service.options.Model,
            app.get("openApi3Strategy"),
            service.options.Model.options.jsonSchema
          );

        return {
          [model]: modelSchema,
          [`${model}_list`]: {
            title: `${modelName} list`,
            type: "array",
            items: {
              $ref: `#/components/schemas/${model}`
            }
          }
        };
      }
    }
  }
  app.configure(
    swagger(swConfig)
  );
  app.use(
    "/docs",
    express.static(process.cwd() + "/node_modules/@mairu/swagger-ui-apikey-auth-form/dist")
  );

}
