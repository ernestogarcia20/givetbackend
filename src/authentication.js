const {
  AuthenticationService,
  JWTStrategy
} = require('@feathersjs/authentication');
const {
  LocalStrategy
} = require('@feathersjs/authentication-local');
const {
  expressOauth
} = require('@feathersjs/authentication-oauth');
const {
  NotAuthenticated
} = require('@feathersjs/errors');

class MyLocalStrategy extends LocalStrategy {
  hashPassword(password) {
    return password;
  }

  comparePassword(entity, password) {
    const {
      entityPasswordField,
      errorMessage
    } = this.configuration;
    if (entity.password == password) {
      return entity;
    } else {
      throw new NotAuthenticated(errorMessage);
    }

  }

  getEntityQuery(query, params) {
    return {
      ...query,
      //active: true,
      $limit: 1
    }
  }

}

module.exports = app => {
  const authentication = new AuthenticationService(app);

  authentication.register('jwt', new JWTStrategy());
  authentication.register('local', new MyLocalStrategy());
  authentication.register('local-email', new MyLocalStrategy());
  authentication.register('local-username', new MyLocalStrategy());
  authentication.register('local-phone', new MyLocalStrategy());

  //app.use('/authentication', authentication);
  app.use('/api/authentication', authentication);

  app.configure(expressOauth());
};
