
var adminFirebase = require("firebase-admin");

var serviceAccount = require("../config/config-firebase.json");

adminFirebase.initializeApp({
    credential: adminFirebase.credential.cert(serviceAccount),
    databaseURL: "https://givetnetwork-ad80c.firebaseio.com"
  });

const firebaseAdmin = adminFirebase;

module.exports = {
    firebaseAdmin
}