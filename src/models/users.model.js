// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

const _base = require('./generated/users')();
_base.options.jsonSchema = {
  title: 'users',
  description: 'the users table',
  exclude: ['avatar'],
}
_base.attributes.fullName.jsonSchema = {
  //description: 'El nombre completo del usuario'
}

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');

  const model = _base.create(sequelizeClient);

  model.hooks = {
    beforeCount(options) {
      options.raw = true;
    }
  }

  // eslint-disable-next-line no-unused-vars
  model.associate = function (models) {
    /*model.hasMany(models.give_support, {
      foreignKey: 'hUser',
      as: 'gives'
    });*/
  };

  model.include_attributes = ['id', 'userName', 'fullName', 'email', 'btc', 'phone', 'fcm_token', 'referralUserName', 'isAdmin'];

  return model;
};
