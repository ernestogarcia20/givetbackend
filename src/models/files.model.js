// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

const _base = require('./generated/uploads')();

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');

  _base.modelName = 'files';
  const model = _base.create(sequelizeClient);

  model.hooks = {
    beforeCount(options) {
      options.raw = true;
    }
  }
  // eslint-disable-next-line no-unused-vars
  model.associate = function (models) {
    //model.belongsTo(models.users);
  };


  return model;
};
