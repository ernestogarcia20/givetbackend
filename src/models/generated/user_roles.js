/* jshint indent: 1 */

var DataTypes = require('sequelize').DataTypes;

module.exports = function() {
	return define('user_roles', {
		'id': {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			primaryKey: true,
			comment: "null",
			autoIncrement: true
		},
		'roleId': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null",
			references: {
				model: 'roles',
				key: 'id'
			}
		},
		'userId': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null",
			references: {
				model: 'users',
				key: 'id'
			}
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'updatedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		}
	}, {
		tableName: 'user_roles'
	});
};

        var _defs;
        function define(modelName, attributes, options) {
          _defs= {
            modelName,
            attributes,
            options,
            create
          }
          return _defs;
        }
        function create(sequelize){
          return sequelize.define(_defs.modelName, _defs.attributes, _defs.options);
        }
        