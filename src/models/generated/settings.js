/* jshint indent: 1 */

var DataTypes = require('sequelize').DataTypes;

module.exports = function() {
	return define('settings', {
		'id': {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			primaryKey: true,
			comment: "null",
			autoIncrement: true
		},
		'name': {
			type: DataTypes.STRING(20),
			allowNull: true,
			comment: "null"
		},
		'type': {
			type: DataTypes.STRING(20),
			allowNull: true,
			comment: "null"
		},
		'value': {
			type: DataTypes.STRING(100),
			allowNull: true,
			comment: "null"
		},
		'description': {
			type: DataTypes.STRING(250),
			allowNull: true,
			comment: "null"
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'updatedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		}
	}, {
		tableName: 'settings'
	});
};

        var _defs;
        function define(modelName, attributes, options) {
          _defs= {
            modelName,
            attributes,
            options,
            create
          }
          return _defs;
        }
        function create(sequelize){
          return sequelize.define(_defs.modelName, _defs.attributes, _defs.options);
        }
        