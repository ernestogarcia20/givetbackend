/* jshint indent: 1 */

var DataTypes = require('sequelize').DataTypes;

module.exports = function() {
	return define('wallet', {
		'id': {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			primaryKey: true,
			comment: "null",
			autoIncrement: true
		},
		'hUser': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'hRef': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'op': {
			type: DataTypes.CHAR(1),
			allowNull: true,
			comment: "null"
		},
		'reason': {
			type: DataTypes.STRING(45),
			allowNull: true,
			comment: "null"
		},
		'description': {
			type: DataTypes.STRING(45),
			allowNull: true,
			comment: "null"
		},
		'amount': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			comment: "null"
		},
		'localBalance': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			comment: "null"
		},
		'balance': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			comment: "null"
		},
		'hParent': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'updatedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'freedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'ticketAmount': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			comment: "null"
		},
		'status': {
			type: DataTypes.STRING(15),
			allowNull: true,
			comment: "null"
		},
		'available': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			defaultValue: '0.00000000',
			comment: "null"
		},
		'hTicket': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		}
	}, {
		tableName: 'wallet'
	});
};

        var _defs;
        function define(modelName, attributes, options) {
          _defs= {
            modelName,
            attributes,
            options,
            create
          }
          return _defs;
        }
        function create(sequelize){
          return sequelize.define(_defs.modelName, _defs.attributes, _defs.options);
        }
        