/* jshint indent: 1 */

var DataTypes = require('sequelize').DataTypes;

module.exports = function() {
	return define('chat', {
		'id': {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			primaryKey: true,
			comment: "null",
			autoIncrement: true
		},
		'fromId': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null",
			references: {
				model: 'users',
				key: 'id'
			}
		},
		'toId': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null",
			references: {
				model: 'users',
				key: 'id'
			}
		},
		'conversationId': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'message': {
			type: DataTypes.STRING(250),
			allowNull: true,
			comment: "null"
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'updatedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'uploadId': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'fileName': {
			type: DataTypes.STRING(100),
			allowNull: true,
			comment: "null"
		},
		'isSupport': {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			comment: "null"
		}
	}, {
		tableName: 'chat'
	});
};

        var _defs;
        function define(modelName, attributes, options) {
          _defs= {
            modelName,
            attributes,
            options,
            create
          }
          return _defs;
        }
        function create(sequelize){
          return sequelize.define(_defs.modelName, _defs.attributes, _defs.options);
        }
        