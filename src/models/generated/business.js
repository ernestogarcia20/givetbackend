/* jshint indent: 1 */

var DataTypes = require('sequelize').DataTypes;

module.exports = function() {
	return define('business', {
		'id': {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			primaryKey: true,
			comment: "null",
			autoIncrement: true
		},
		'hCategory': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null",
			references: {
				model: 'business_category',
				key: 'id'
			}
		},
		'businessName': {
			type: DataTypes.STRING(50),
			allowNull: true,
			comment: "null"
		},
		'hUser': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null",
			references: {
				model: 'users',
				key: 'id'
			}
		},
		'openHour': {
			type: DataTypes.STRING(5),
			allowNull: true,
			comment: "null"
		},
		'closeHour': {
			type: DataTypes.STRING(5),
			allowNull: true,
			comment: "null"
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'updatedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'address': {
			type: DataTypes.STRING(250),
			allowNull: true,
			comment: "null"
		},
		'latitude': {
			type: DataTypes.STRING(20),
			allowNull: true,
			comment: "null"
		},
		'longitude': {
			type: DataTypes.STRING(20),
			allowNull: true,
			comment: "null"
		},
		'status': {
			type: DataTypes.STRING(20),
			allowNull: true,
			defaultValue: 'Pending',
			comment: "null"
		},
		'minConsumtionToReward': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			comment: "null"
		},
		'giftForConsumption': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			comment: "null"
		},
		'gvtToUser': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			comment: "null"
		},
		'gvtToSponsor': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			comment: "null"
		},
		'countryCode': {
			type: DataTypes.STRING(5),
			allowNull: true,
			comment: "null"
		},
		'rating': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			defaultValue: '0.0',
			comment: "null"
		},
		'grade': {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			comment: "null"
		},
		'ratingCount': {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			comment: "null"
		},
		'ratingSum': {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			comment: "null"
		}
	}, {
		tableName: 'business'
	});
};

        var _defs;
        function define(modelName, attributes, options) {
          _defs= {
            modelName,
            attributes,
            options,
            create
          }
          return _defs;
        }
        function create(sequelize){
          return sequelize.define(_defs.modelName, _defs.attributes, _defs.options);
        }
        