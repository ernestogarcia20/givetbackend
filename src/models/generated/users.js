/* jshint indent: 1 */

var DataTypes = require('sequelize').DataTypes;

module.exports = function() {
	return define('users', {
		'id': {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			primaryKey: true,
			comment: "null",
			autoIncrement: true
		},
		'email': {
			type: DataTypes.STRING(45),
			allowNull: false,
			comment: "null",
			unique: true
		},
		'userName': {
			type: DataTypes.STRING(45),
			allowNull: true,
			comment: "null",
			unique: true
		},
		'fullName': {
			type: DataTypes.STRING(45),
			allowNull: false,
			comment: "null"
		},
		'hCountry': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "country id"
		},
		'phone': {
			type: DataTypes.STRING(45),
			allowNull: true,
			comment: "null"
		},
		'password': {
			type: DataTypes.STRING(100),
			allowNull: false,
			comment: "null"
		},
		'password2': {
			type: DataTypes.STRING(100),
			allowNull: false,
			comment: "null"
		},
		'referralUserName': {
			type: DataTypes.STRING(45),
			allowNull: true,
			comment: "null"
		},
		'btc': {
			type: DataTypes.STRING(100),
			allowNull: true,
			comment: "null"
		},
		'phoneOk': {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			comment: "null"
		},
		'emailOk': {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			comment: "null"
		},
		'accessPin': {
			type: DataTypes.STRING(45),
			allowNull: true,
			comment: "null"
		},
		'avatar': {
			type: DataTypes.STRING(45),
			allowNull: true,
			comment: "null"
		},
		'rating': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			comment: "null"
		},
		'auth0Id': {
			type: DataTypes.STRING(45),
			allowNull: true,
			comment: "null"
		},
		'googleId': {
			type: DataTypes.STRING(45),
			allowNull: true,
			comment: "null"
		},
		'locked': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'updatedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'countryCode': {
			type: DataTypes.STRING(5),
			allowNull: true,
			comment: "null"
		},
		'deleted': {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			comment: "null"
		},
		'isAdmin': {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			comment: "null"
		},
		'hasBusiness': {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			comment: "null"
		},
		'verifyToken': {
			type: DataTypes.STRING(45),
			allowNull: true,
			comment: "null"
		},
		'verifyShortToken': {
			type: DataTypes.STRING(10),
			allowNull: true,
			comment: "null"
		},
		'resetShortToken': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'isVerified': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'verifyExpires': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'resetToken': {
			type: DataTypes.STRING(45),
			allowNull: true,
			comment: "null"
		},
		'resetExpires': {
			type: DataTypes.STRING(45),
			allowNull: true,
			comment: "null"
		},
		'fcm_token': {
			type: DataTypes.STRING(255),
			allowNull: true,
			comment: "null"
		}
	}, {
		tableName: 'users'
	});
};

        var _defs;
        function define(modelName, attributes, options) {
          _defs= {
            modelName,
            attributes,
            options,
            create
          }
          return _defs;
        }
        function create(sequelize){
          return sequelize.define(_defs.modelName, _defs.attributes, _defs.options);
        }
        