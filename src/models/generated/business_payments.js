/* jshint indent: 1 */

var DataTypes = require('sequelize').DataTypes;

module.exports = function() {
	return define('business_payments', {
		'id': {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			primaryKey: true,
			comment: "null",
			autoIncrement: true
		},
		'userId': {
			type: DataTypes.INTEGER,
			allowNull: false,
			comment: "null",
			references: {
				model: 'users',
				key: 'id'
			}
		},
		'businessId': {
			type: DataTypes.INTEGER,
			allowNull: false,
			comment: "null",
			references: {
				model: 'business',
				key: 'id'
			}
		},
		'amount': {
			type: DataTypes.DECIMAL,
			allowNull: false,
			comment: "null"
		},
		'giftAmount': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			comment: "null"
		},
		'cryptoAddress': {
			type: DataTypes.STRING(100),
			allowNull: false,
			comment: "null"
		},
		'approved': {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '-1',
			comment: "null"
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'updatedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		}
	}, {
		tableName: 'business_payments'
	});
};

        var _defs;
        function define(modelName, attributes, options) {
          _defs= {
            modelName,
            attributes,
            options,
            create
          }
          return _defs;
        }
        function create(sequelize){
          return sequelize.define(_defs.modelName, _defs.attributes, _defs.options);
        }
        