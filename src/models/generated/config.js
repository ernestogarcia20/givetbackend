/* jshint indent: 1 */

var DataTypes = require('sequelize').DataTypes;

module.exports = function() {
	return define('config', {
		'id': {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			comment: "null"
		},
		'gainPercent': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'timeToConfirmOrder': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'expirationDays': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'paymantToUnlock': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			comment: "null"
		},
		'minGet': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			comment: "null"
		},
		'unlockPaymentAddress': {
			type: DataTypes.STRING(60),
			allowNull: true,
			comment: "null"
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'updatedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'gainPercentAfter': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'minDirect10': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'minDirect20': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'minDirect30': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		}
	}, {
		tableName: 'config'
	});
};

        var _defs;
        function define(modelName, attributes, options) {
          _defs= {
            modelName,
            attributes,
            options,
            create
          }
          return _defs;
        }
        function create(sequelize){
          return sequelize.define(_defs.modelName, _defs.attributes, _defs.options);
        }
        