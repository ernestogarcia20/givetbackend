/* jshint indent: 1 */

var DataTypes = require('sequelize').DataTypes;

module.exports = function() {
	return define('orders', {
		'id': {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			primaryKey: true,
			comment: "null",
			autoIncrement: true
		},
		'hBuy': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null",
			references: {
				model: 'buy_tickets',
				key: 'id'
			}
		},
		'hSell': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null",
			references: {
				model: 'sell_tickets',
				key: 'id'
			}
		},
		'amount': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			comment: "null"
		},
		'status': {
			type: DataTypes.STRING(20),
			allowNull: true,
			comment: "null"
		},
		'hash': {
			type: DataTypes.STRING(100),
			allowNull: true,
			comment: "null"
		},
		'voucher_image': {
			type: DataTypes.STRING(50),
			allowNull: true,
			comment: "null"
		},
		'voucher_sentAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'voucher_receivedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'mustCloseAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'hChat': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'updatedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'buyRating': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'sellRating': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'mustConfirmAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		}
	}, {
		tableName: 'orders'
	});
};

        var _defs;
        function define(modelName, attributes, options) {
          _defs= {
            modelName,
            attributes,
            options,
            create
          }
          return _defs;
        }
        function create(sequelize){
          return sequelize.define(_defs.modelName, _defs.attributes, _defs.options);
        }
        