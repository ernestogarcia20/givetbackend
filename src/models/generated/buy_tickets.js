/* jshint indent: 1 */

var DataTypes = require('sequelize').DataTypes;

module.exports = function() {
	return define('buy_tickets', {
		'id': {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			primaryKey: true,
			comment: "null",
			autoIncrement: true
		},
		'hUser': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null",
			references: {
				model: 'users',
				key: 'id'
			}
		},
		'amount': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			comment: "null"
		},
		'balance': {
			type: DataTypes.DECIMAL,
			allowNull: true,
			comment: "null"
		},
		'status': {
			type: DataTypes.STRING(20),
			allowNull: true,
			comment: "null"
		},
		'revocations': {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			comment: "null"
		},
		'transactions': {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			comment: "null"
		},
		'approvals': {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			comment: "null"
		},
		'gain': {
			type: DataTypes.INTEGER,
			allowNull: true,
			defaultValue: '0',
			comment: "null"
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'updatedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'finishAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'hPack': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null",
			references: {
				model: 'packs',
				key: 'id'
			}
		}
	}, {
		tableName: 'buy_tickets'
	});
};

        var _defs;
        function define(modelName, attributes, options) {
          _defs= {
            modelName,
            attributes,
            options,
            create
          }
          return _defs;
        }
        function create(sequelize){
          return sequelize.define(_defs.modelName, _defs.attributes, _defs.options);
        }
        