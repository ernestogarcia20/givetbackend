/* jshint indent: 1 */

var DataTypes = require('sequelize').DataTypes;

module.exports = function() {
	return define('xxx', {
		'id': {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			primaryKey: true,
			primaryKey: true,
			comment: "null",
			autoIncrement: true
		},
		'text': {
			type: DataTypes.STRING(255),
			allowNull: false,
			comment: "null"
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: false,
			comment: "null"
		},
		'updatedAt': {
			type: DataTypes.DATE,
			allowNull: false,
			comment: "null"
		}
	}, {
		tableName: 'xxx'
	});
};

        var _defs;
        function define(modelName, attributes, options) {
          _defs= {
            modelName,
            attributes,
            options,
            create
          }
          return _defs;
        }
        function create(sequelize){
          return sequelize.define(_defs.modelName, _defs.attributes, _defs.options);
        }
        