/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('v_users', {
		hParent: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		},
		hLeft: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		},
		hRight: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		},
		children: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			defaultValue: '0'
		},
		id: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			defaultValue: '0'
		},
		email: {
			type: DataTypes.STRING(45),
			allowNull: false
		},
		userName: {
			type: DataTypes.STRING(45),
			allowNull: true
		},
		fullName: {
			type: DataTypes.STRING(45),
			allowNull: false
		},
		hCountry: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		},
		phone: {
			type: DataTypes.STRING(45),
			allowNull: true
		},
		password: {
			type: DataTypes.STRING(100),
			allowNull: false
		},
		password2: {
			type: DataTypes.STRING(100),
			allowNull: false
		},
		referralUserName: {
			type: DataTypes.STRING(45),
			allowNull: true
		},
		btc: {
			type: DataTypes.STRING(100),
			allowNull: true
		},
		phoneOk: {
			type: DataTypes.INTEGER(6),
			allowNull: true,
			defaultValue: '0'
		},
		emailOk: {
			type: DataTypes.INTEGER(6),
			allowNull: true,
			defaultValue: '0'
		},
		verifyToken: {
			type: DataTypes.STRING(45),
			allowNull: true
		},
		accessPin: {
			type: DataTypes.STRING(45),
			allowNull: true
		},
		avatar: {
			type: DataTypes.STRING(45),
			allowNull: true
		},
		rating: {
			type: DataTypes.DECIMAL,
			allowNull: true
		},
		auth0Id: {
			type: DataTypes.STRING(45),
			allowNull: true
		},
		googleId: {
			type: DataTypes.STRING(45),
			allowNull: true
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: true
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: true
		}
	}, {
		tableName: 'v_users'
	});
};
