/* jshint indent: 1 */

var DataTypes = require('sequelize').DataTypes;

module.exports = function() {
	return define('uploads', {
		'id': {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			primaryKey: true,
			comment: "null",
			autoIncrement: true
		},
		'hUser': {
			type: DataTypes.INTEGER,
			allowNull: false,
			comment: "null"
		},
		'originalName': {
			type: DataTypes.STRING(100),
			allowNull: true,
			comment: "null"
		},
		'assignedName': {
			type: DataTypes.STRING(100),
			allowNull: true,
			comment: "null"
		},
		'fSize': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'type': {
			type: DataTypes.STRING(20),
			allowNull: true,
			comment: "null"
		},
		'hType': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		},
		'createdAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'updatedAt': {
			type: DataTypes.DATE,
			allowNull: true,
			comment: "null"
		},
		'updatedBy': {
			type: DataTypes.INTEGER,
			allowNull: true,
			comment: "null"
		}
	}, {
		tableName: 'uploads'
	});
};

        var _defs;
        function define(modelName, attributes, options) {
          _defs= {
            modelName,
            attributes,
            options,
            create
          }
          return _defs;
        }
        function create(sequelize){
          return sequelize.define(_defs.modelName, _defs.attributes, _defs.options);
        }
        