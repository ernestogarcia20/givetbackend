// Application hooks that run for every service

module.exports = {
  before: {
    all: [],
    find: [toSequelize],
    get: [toSequelize],
    create: [createdAt],
    update: [updatedAt],
    patch: [updatedAt],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};

function createdAt(context) {
  var dt = new Date();
  context.data.createdAt = dt;
  context.data.updatedAt = dt;
  return context;
}

function updatedAt(context) {
  var dt = new Date();
  context.data.updatedAt = dt;
  return context;
}

async function toSequelize(context) {
  if (context.path == 'api/authentication') {
    return context;
  }

  if (context.method == 'get') {
    //context.params.sequelize = context.params.sequelize || {};
    //OJO: toSequelize está como primer hook y los sucesivos
    // reciben params.sequelize como la authentication
    context.params.sequelize = {};
    context.params.sequelize.nest = true;
    return;
  }

  if (!context.params.query) {
    return context;
  }

  var sequelize = context.params.sequelize || {};
  sequelize.nest = true;

  var bSort = context.params.query.$sort ? true : false;
  if (bSort) {
    var sort = context.params.query.$sort;
    var order = [];
    for (var key in sort) {
      order.push([key, sort[key] == -1 ? 'DESC' : 'ASC']);
    }
    sequelize.order = order;

    //delete context.params.query.$sort;
  }

  var {
    $limit,
    $skip,
    $sort,
    ...where
  } = context.params.query;

  context.params.query = {};
  if ($limit) context.params.query.$limit = $limit;
  if ($skip) context.params.query.$skip = $skip;

  sequelize.where = {
    ...sequelize.where,
    ...where
  };

  context.params.sequelize = sequelize;
  return context;
}

String.prototype.replaceAll = function replaceAll(find, replace) {
  //return this.replace(new RegExp(find, 'g'), replace);
  return this.split(find).join(replace);
}
