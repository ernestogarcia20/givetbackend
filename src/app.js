const path = require("path");
const favicon = require("serve-favicon");
const compress = require("compression");
const helmet = require("helmet");
const cors = require("cors");
const logger = require("./logger");

const feathers = require("@feathersjs/feathers");
const configuration = require("@feathersjs/configuration");
const express = require("@feathersjs/express");
const socketio = require("@feathersjs/socketio");

const middleware = require("./middleware");
const services = require("./services");
const appHooks = require("./app.hooks");
const channels = require("./channels");

const {firebaseAdmin} = require('./firebase');

const authentication = require("./authentication");

const sequelize = require("./sequelize");

const app = express(feathers());

// custom querystring parser 
/*
const qs = require('querystring');
app.set('query parser', str => {
  var q = qs.parse(str);
  return q
});
*/
//

// Load app configuration
app.configure(configuration());

// load swagger docs
const swagger = require("./app.docs.js");
app.configure(swagger);

// Enable security, CORS, compression, favicon and body parsing
app.use(helmet());
app.use(cors());
app.use(compress());
app.use(
  express.json({
    limit: "10mb"
  })
);
app.use(
  express.urlencoded({
    limit: "10mb",
    extended: true
  })
);




//TODO: Revisar app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder
// Host the public folder
app.use('/', express.static(app.get('public')));
app.use('/', express.static(app.get('app')));
app.use("/mocha", express.static(app.get("mocha")));
app.use("/files", express.static(app.get("files")));

// Set up Plugins and providers
app.configure(express.rest());
app.configure(socketio());
app.configure(sequelize);

// versionate
var versionate = require("feathers-versionate");
app.configure(versionate());
app.versionate("api", "/api/");
//

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
app.configure(authentication);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(
  express.errorHandler({
    logger
  })
);

app.set('firebaseAdmin', firebaseAdmin);

app.hooks(appHooks);

/*
app.services.users.find({
  sequelize: {
    raw: false,
    include: ['gives']
  },
}).then(function (r) {
  console.log(r);
}).catch(function (err) {
  console.log(err);
})
*/

const Emparejador = require("./services/emparejador/emparejador.service.js");
var emparejador = Emparejador(app);
app.set("emparejador", emparejador);

/*
//TODO: Enable in runtime
const TIMER = 30000; //30 segundos
var f = (async function f() {
  await emparejador.emparejar();

  setTimeout(f, TIMER);
})();
*/
module.exports = app;

//HTTPS
/*
const fs = require("fs");
const https = require("https");
const server = https
  .createServer({
      key: fs.readFileSync(
        path.resolve(__dirname, "../config/ssl/selfsigned.key")
      ),
      cert: fs.readFileSync(
        path.resolve(__dirname, "../config/ssl/selfsigned.crt")
      ),
      requestCert: false,
      rejectUnauthorized: false
    },
    app
  )
  .listen(3043);
app.setup(server);
*/
//HTTPS\
