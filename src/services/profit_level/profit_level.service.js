// Initializes the `profit_level` service on path `/profit_level`
const {
  GainLevel: Service
} = require('./profit_level.class');
const createModel = require('../../models/profit_level.model');
const hooks = require('./profit_level.hooks');
const docs = {
  operations: {
    find: {
      summary: 'businessPercent es el porcentaje para comisiones por business Payment'
    }
  }
}; // require('./profit_level.docs');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  const servicePath = '/api/profit_level';
  var serviceObject = new Service(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  service.hooks(hooks);
};
