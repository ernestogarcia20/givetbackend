const users = require('./users/users.service.js');
const giveSupport = require('./buy/buy.service.js');

const getSupport = require('./sell/sell.service.js');
const packs = require('./packs/packs.service.js');
const giveGet = require('./orders/orders.service.js');
//const emparejador = require('./emparejador/emparejador.service.js');
const chat = require('./chat/chat.service.js');

const wallet = require('./wallet/wallet.service.js');
const gainLevel = require('./profit_level/profit_level.service.js');
const usersTree = require('./users_tree/users_tree.service.js');

const config = require('./config/config.service.js');

const tickets = require('./tickets/tickets.service.js');
const files = require('./files/files.service.js');

const businessCategory = require('./business-category/business-category.service.js');
const business = require('./business/business.service.js');

const settings = require('./settings/settings.service.js');
const messages = require('./messages/messages.service.js');

const system = require('./system/system.service.js');

const x = require('./x/x.service.js');

const announcements = require('./announcements/announcements.service.js');

const blockChain = require('./blockchain/blockchain.service.js');
const ethereum = require('./ethereum/ethereum.service.js');

const mailer = require('./mailer/mailer.service');
const authmanagement = require('./authmanagement/authmanagement.service');

const businessPayments = require('./business-payments/business-payments.service');

const businessScores = require('./business_scores/business_scores.service.js');

const firebaseNotification = require('./firebase-notification/firebase-notification.service.js');

// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(giveSupport);
  app.configure(getSupport);
  app.configure(packs);
  app.configure(giveGet);
  app.configure(chat);
  app.configure(wallet);
  app.configure(gainLevel);
  app.configure(usersTree);
  app.configure(config);
  app.configure(files);

  //app.configure(emparejador);

  app.configure(tickets);

  app.configure(businessCategory);
  app.configure(business);

  app.configure(settings);
  app.configure(messages);
  app.configure(system);
  app.configure(x);
  app.configure(announcements);
  app.configure(blockChain);
  app.configure(ethereum);

  app.configure(mailer);
  app.configure(authmanagement);

  app.configure(businessPayments);

  app.configure(businessScores);
  app.configure(firebaseNotification);
};
