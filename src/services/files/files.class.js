const {
  Service
} = require('feathers-sequelize');

const BlobService = require('feathers-blob');
const fs = require('fs-blob-store');

const blobStorage = fs('./public/uploads');
let blob = new BlobService({
  Model: blobStorage
});

exports.Uploads = class Uploads extends Service {
  async create(data, params) {

    try {
      let result = await blob.create({
        uri: data.uri
      });
      data.assignedName = result.id;
      data.fSize = result.size;
      var ret = await super.create(data, params);
      return ret;
    } catch (err) {
      return err;
    }

  }

};
