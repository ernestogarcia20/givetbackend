// Initializes the `uploads` service on path `/api/files`
const {
  Uploads: Service
} = require('./files.class');
const createModel = require('../../models/files.model');
const hooks = require('./files.hooks');
const docs = require('./files.service.docs');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  const servicePath = '/api/files';
  var serviceObject = new Service(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  service.hooks(hooks);
};
