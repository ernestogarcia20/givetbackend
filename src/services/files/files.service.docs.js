module.exports = {
  description: 'to file upload',
  schemas: {
    upload_create: {
      "type": "object",
      "properties": {
        "hUser": {
          "type": "integer",
          "format": "int32"
        },
        "originalName": {
          "type": "string",
          "maxLength": 100,
          "nullable": true
        },
        "type": {
          description: 'the class or category of file. May be a tableName or an entityName',
          "type": "string",
          "maxLength": 20,
          "nullable": true
        },
        "hType": {
          description: 'the type id',
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "data": {
          "description": "file content base64",
          "type": "string",
          "format": "base64",
          "nullable": false
        },
      }
    }
  },
  operations: {
    create: {
      description: `
For uploads use uri schema (data:[<media type>][;base64],<data>), see examples. 
For download files, use path host/files/<assignedName>`,
      requestBody: {
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/upload_create'
            },
            examples: {
              'punto.png': {
                value: {
                  hUser: 1,
                  originalName: 'punto.png',
                  type: 'avatar',
                  hType: 1,
                  uri: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=="
                }
              },
              'lentes.jpeg': {
                value: {
                  hUser: 2,
                  originalName: 'lentes.jpeg',
                  type: 'avatar',
                  hType: 2,
                  uri: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDADIiJSwlHzIsKSw4NTI7S31RS0VFS5ltc1p9tZ++u7Kfr6zI4f/zyNT/16yv+v/9////////wfD/////////////2wBDATU4OEtCS5NRUZP/zq/O////////////////////////////////////////////////////////////////////wAARCAAYAEADAREAAhEBAxEB/8QAGQAAAgMBAAAAAAAAAAAAAAAAAQMAAgQF/8QAJRABAAIBBAEEAgMAAAAAAAAAAQIRAAMSITEEEyJBgTORUWFx/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AOgM52xQDrjvAV5Xv0vfKUALlTQfeBm0HThMNHXkL0Lw/swN5qgA8yT4MCS1OEOJV8mBz9Z05yfW8iSx7p4j+jA1aD6Wj7ZMzstsfvAas4UyRHvjrAkC9KhpLMClQntlqFc2X1gUj4viwVObKrddH9YDoHvuujAEuNV+bLwFS8XxdSr+Cq3Vf+4F5RgQl6ZR2p1eAzU/HX80YBYyJLCuexwJCO2O1bwCRidAfWBSctswbI12GAJT3yiwFR7+MBjGK2g/WAJR3FdF84E2rK5VR0YH/9k="
                }
              }
            }
          }
        }
      },

    },
    patch: false,
    update: false

  }
}
