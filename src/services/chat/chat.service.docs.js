module.exports = {
  description: 'isSupport identify chat with some admin',
  schemas: {
    chat_create: {
      "type": "object",
      "properties": {
        "fromId": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "toId": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "message": {
          "type": "string",
          "maxLength": 250,
          "nullable": true
        },
        "file": {
          "description": "file attachemnt content base64",
          "type": "string",
          "format": "base64",
          "nullable": false
        }
      }
    }
  },
  operations: {
    create: {
      requestBody: {
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/chat_create'
            }
          }
        }
      },
    },
    update: false,
    patch: false,
    remove: false
  }
}
