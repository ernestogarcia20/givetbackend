const {
  authenticate
} = require('@feathersjs/authentication').hooks;

const attachFile = require('./attachFile.hook');

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [join],
    get: [join],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [setAvatar],
    get: [setAvatar],
    create: [attachFile],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};


function join(context) {

  const sequelize = context.app.get('sequelizeClient');
  const {
    users
  } = sequelize.models;

  context.params.sequelize.include = [{
    model: users,
    as: 'toUser',
    attributes: users.include_attributes
  }, {
    model: users,
    as: 'fromUser',
    attributes: users.include_attributes
  }]

  return context;
}


async function setAvatar(context) {
  var data = context.result.data || [context.result];
  for (var i = 0; i < data.length; i++) {
    var fromUser = data[i].fromUser;
    var files = await context.app.service('api/files').find({
      query: {
        type: 'avatar',
        hType: fromUser.id,
        $limit: 1
      }
    })
    if (files.total > 0) {
      fromUser.avatar = context.app.get('host') + '/files/' + files.data[0].assignedName
    }

    //
    var toUser = data[i].toUser;
    files = await context.app.service('api/files').find({
      query: {
        type: 'avatar',
        hType: toUser.id,
        $limit: 1
      }
    })
    if (files.total > 0) {
      toUser.avatar = context.app.get('host') + '/files/' + files.data[0].assignedName
    }

  }
}
