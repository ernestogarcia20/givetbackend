// Initializes the `chat` service on path `/chat`
const {
  Chat: Service
} = require('./chat.class');
const createModel = require('../../models/chat.model');
const hooks = require('./chat.hooks');
const docs = require('./chat.service.docs');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  const servicePath = '/api/chat';
  var serviceObject = new Service(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  service.hooks(hooks);

};
