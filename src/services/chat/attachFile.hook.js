const {
  NotAuthenticated,
  NotImplemented
} = require('@feathersjs/errors');

module.exports =
  async function (context) {
    var data = context.data;
    var result = context.result;

    var hFile = null;
    if (data.file) {
      var file = await context.app.service('/api/files').create({
        hUser: data.fromId,
        type: 'CHAT',
        hType: result.id,
        uri: data.file
      });
      context.result = await context.service.patch(result.id, {
        uploadId: file.id,
        fileName: file.assignedName
      });
    }

    return context;

  }
