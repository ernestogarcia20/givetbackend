module.exports = async function (context) {

  if (!context.params.query.$view) {
    return context;
  }

  var sequelize = context.app.get('sequelizeClient');

  delete context.params.query.$view;
  var conversationId = context.params.query.conversationId;

  var data = await sequelize.query(`call findChat(:conversationId);`, {
    replacements: {
      conversationId: conversationId
    }
  });

  var ordered = data.sort(function (a, b) {
    return (a.createdAt - b.createdAt) * 1; //ascending
  });

  context.result = {
    data: ordered
  };

}
