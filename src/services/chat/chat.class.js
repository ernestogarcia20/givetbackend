const {
  Service
} = require('feathers-sequelize');

exports.Chat = class Chat extends Service {
  async create(data) {
    var chat = await super.create(data);
    if (!chat.conversationId) {
      chat = await this.patch(chat.id, {
        conversationId: chat.id
      });
    }
    return chat;
  }

};
