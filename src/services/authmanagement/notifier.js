module.exports = function (app) {
  let config = app.get('notifier');

  function getLink(type, hash) {
    var host = app.get('notifier').host;
    const url = host + '/' + type + '/' + hash
    return url
  }

  function sendEmail(email) {
    return app.service('api/mailer').create(email).then(function (result) {
      console.log('Sent email', result)
    }).catch(err => {
      console.log('Error sending email', err)
    })
  }

  return {
    notifier: function (type, userObject, notifierOptions) {
      let tokenLink
      let email
      let user = {
        ...userObject
      }
      switch (type) {
        case 'resendVerifySignup': //sending the user the verification email

          email = {
            from: config.defaults.from,
            to: user.email,
            template: 'verify',
            user
          }
          return sendEmail(email);

          break;
        case 'resetPwd':
        case 'resendPwd':
          email = {
            from: config.defaults.from,
            to: user.email,
            template: 'forgotPwd',
            user
          }
          return sendEmail(email)
          break

        case 'resendPwd2':
          email = {
            from: config.defaults.from,
            to: user.email,
            template: 'forgotPwd2',
            user
          }
          return sendEmail(email)
          break
          //
        case 'verifySignup': // confirming verification
          //por ahora no se envía mail cuando el usuario realiza la verificación.
          break

        case 'sendResetPwd':
          tokenLink = getLink('changePwd', user.resetToken)
          email = {
            from: config.defaults.from,
            to: user.email,
            subject: config.sendResetPwd.subject,
            html: 'Aquí las instrucciones para reestablecer password ' + tokenLink
          }
          return sendEmail(email)
          break

        case 'resetPwd':


        case 'passwordChange':
          email = {}
          return sendEmail(email)
          break

        case 'identityChange':
          tokenLink = getLink('verifyChanges', user.verifyToken)
          email = {}
          return sendEmail(email)
          break



        default:
          break
      }
    }
  }
}
