module.exports = {
  description: "Authmanagement service. Verify, forgot passwords & resends",
  operations: {
    create: {
      requestBody: {
        required: true,
        content: {
          "application/json": {
            "schema": {
              //$ref: '#/components/schemas/authManagement'
            },

            examples: {
              'CheckUnique': {
                summary: "checkUnique: Verify if an attribute is unique (username, email or phone)",
                value: {
                  action: 'checkUnique',
                  value: {
                    email: 'a@globalpyme.com',
                    username: 'admin',
                    phone: '12345'
                  }, // e.g. {email, username}. Props with null or undefined are ignored.
                  /*ownId: 1, // excludes your current user from the search
                  meta: {
                    noErrMsg: 'no errors'
                  }, // if return an error.message if not unique
                  */
                }
              },
              'resendVerifySignup': {
                summary: "resendVerifySignup: Send verify email with token",
                value: {
                  action: 'resendVerifySignup',
                  value: {
                    email: 'admin@globalpyme.com',
                    //notifierOptions: {}, // options passed to options.notifier, e.g. {preferredComm: 'cellphone'}
                  }
                }
              },
              'verifySignupShort': {
                summary: "verifySignupShort: verify identity with short token",
                value: {
                  action: 'verifySignupShort',
                  value: {
                    user: {
                      email: 'admin@globalpyme.com',
                    },
                    token: '122122'
                  }
                }
              }
            }
          }
        }
      }
    }
  }
};
