// Initializes the `authmanagement` service on path `/authmanagement`
const authManagement = require("feathers-authentication-management");
const hooks = require("./authmanagement.hooks");
const notifier = require("./notifier");

const docs = require("./authmanagement.service.docs");

module.exports = function (app) {
  const servicePath = "/api/authManagement";

  // Initialize our service with any options it requires
  app.configure(
    authManagement({

      service: "/api/users",
      path: 'auth-Management',

      identifyUserProps: ["email"],
      ...notifier(app)
    })
  );

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('auth-Management');

  service.docs = docs;
  app.use(servicePath, service);

  service.hooks(hooks);
};
