// Initializes the `businessCategory` service on path `/settings`
const {
  BusinessCategory: Service
} = require('./settings.class');
const createModel = require('../../models/settings.model');
const hooks = require('./settings.hooks');

const docs = require('./settings.service.docs');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  const servicePath = '/api/settings';
  var serviceObject = new Service(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  service.hooks(hooks);

};
