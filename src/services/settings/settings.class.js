const {
  Service
} = require('feathers-sequelize');

exports.BusinessCategory = class BusinessCategory extends Service {

  async getValue(settingName) {
    var setting = (await this.find({
      query: {
        name: settingName
      }
    })).data[0];
    if (setting.type == 'number') {
      return Number(setting.value);
    }
    return setting.value;
  }
};
