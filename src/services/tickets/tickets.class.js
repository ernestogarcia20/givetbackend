/* eslint-disable no-unused-vars */
exports.Tickets = class Tickets {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
  }

  async find(params) {
    var type = params.query.type;
    delete params.query["type"];
    let gives = {
        total: 0,
        data: []
      },
      gets = {
        total: 0,
        data: []
      };
    if (!type || type == "buy") {
      gives = await this.app.service("/api/buy_ticket").find(params);
    }
    if (!type || type == "sell") {
      gets = await this.app.service("/api/sell_ticket").find(params);
    }
    if (gives.data) {
      gives.data.map(function (r) {
        r.type = "buy";
        return r;
      });
    }
    if (gets.data) {
      gets.data.map(function (r) {
        r.type = "sell";
        return r;
      });
    }
    let pre = gives.data.concat(gets.data);
    let data = pre.sort(function (a, b) {
      return (a.createdAt - b.createdAt) * -1; //descending
    });

    return {
      total: gives.total + gets.total,
      data: data
    };
  }

  async get(id, params) {
    return {
      id,
      text: `A new message with ID: ${id}!`
    };
  }

  async create(data, params) {
    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }

    return data;
  }

  async update(id, data, params) {
    return data;
  }

  async patch(id, data, params) {
    return data;
  }

  async remove(id, params) {
    return {
      id
    };
  }
};
