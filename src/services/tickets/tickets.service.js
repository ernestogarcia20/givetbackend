// Initializes the `tickets` service on path `/api/tickets`
const { Tickets } = require('./tickets.class');
const hooks = require('./tickets.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/api/tickets', new Tickets(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('api/tickets');

  service.hooks(hooks);
};
