// Initializes the `businessCategory` service on path `/business-category`
const {
  BusinessCategory: Service
} = require('./business-category.class');
const createModel = require('../../models/business-category.model');
const hooks = require('./business-category.hooks');

const docs = require('./business-category.service.docs');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  const servicePath = '/api/business_category';
  var serviceObject = new Service(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  service.hooks(hooks);

};
