const {
  NotAcceptable,
  NotImplemented
} = require('@feathersjs/errors');

const docs = require('./usd.service.docs');

module.exports = function (app) {

  const servicePath = 'api/blockchain/exchange/usd';

  const sequelize = app.get('sequelizeClient');
  var service = {
    docs,
    options: {},

    async find(params) {
      var usd = await app.service('api/blockchain').currentUSD();
      return 1 / usd;
    },

  };

  app.use(servicePath, service);

  app.service(servicePath).hooks({});

}
