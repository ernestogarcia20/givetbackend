module.exports = {

  model: 'hash',
  schemas: {
    ok: {}
  },
  operations: {
    create: {
      "requestBody": {
        "required": true,
        "content": {
          "application/json": {
            "schema": {
              type: "object",
              properties: {
                hash: {
                  type: 'string'
                }
              }
            },
            examples: {
              'hash confirmado': {
                value: {
                  hash: "ff370a5f105c9e6f864ef5c46a6dc777c1d284aca7fbef5ceb231e7831edd3c8"
                }
              },
              'hash inválido o no confirmado': {
                value: {
                  hash: "gf370a5f105c9e6f864ef5c46a6dc777c1d284aca7fbef5ceb231e7831edd3c8"
                }
              },
              'otro hash válido': {
                value: {
                  hash: "3f62fd55c89cbab2046f4c66504051e59afcb652ab25a7ac1049a0c557db04b6"
                }
              }
            }
          }
        }
      },
      'responses.200': {
        "content": {
          "application/json": {
            schema: {
              "$ref": "#/components/schemas/ok"
            }
          }
        }
      }
    }
  }
}
