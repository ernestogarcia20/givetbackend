// Initializes the `block-chain` service on path `/block-chain`
const {
  BlockChain: Service
} = require('./blockchain.class');
const hooks = require('./blockchain.hooks');
const docs = require('./blockchain.service.docs');


module.exports = function (app) {
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  const servicePath = '/api/blockchain';
  var serviceObject = new Service(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  service.hooks(hooks);

  // 
  const verifyHash = require('./verify-hash.service');
  app.configure(verifyHash);

  const usd = require('./usd.service');
  app.configure(usd);

};
