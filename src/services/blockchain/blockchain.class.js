/* eslint-disable no-unused-vars */
const {
  NotAcceptable,
  NotImplemented
} = require('@feathersjs/errors');

const blockexplorer = require("blockchain.info/blockexplorer");
var exchange = require('blockchain.info/exchange')

const testHash = "ff370a5f105c9e6f864ef5c46a6dc777c1d284aca7fbef5ceb231e7831edd3c8";

exports.BlockChain = class BlockChain {
  constructor(options) {
    this.options = options || {};
  }

  async find(params) {
    return [];
  }


  async update(id, data, params) {
    return data;
  }

  async patch(id, data, params) {
    return data;
  }

  async remove(id, params) {
    return {
      id
    };
  }


  async verifyHash(hash) {
    var options = {};

    try {
      var blockCount = await blockexplorer.getLatestBlock(options);
      var transaction = await blockexplorer.getTx(hash, options);

      var confirmations = blockCount.height - transaction.block_height + 1
      return {
        confirmations,
        ...transaction
      };
    } catch (err) {
      try {
        var oError = JSON.parse(err);
        throw new NotAcceptable(oError.reason);
      } catch (s) {
        throw new NotAcceptable(err);
      }
    }
  }

  async verifyPayment(hash, buyUser, sellUser, amount) {
    try {
      var transaction = await this.verifyHash(hash);

      if (transaction.confirmations < 3) {
        throw new NotAcceptable("Transaction not confirmed yet")
      }
      // sender
      var input = transaction.inputs.find(el => {
        return el.prev_out.addr == buyUser.btc
      });
      if (!input) {
        throw new NotAcceptable("Transaction sender is not the buyer");
      }
      // receiver
      var output = transaction.out.find(el => {
        return el.addr == sellUser.btc
      });
      if (!output) {
        throw new NotAcceptable("Transaction receiver is not the seller");
      }
      // amount
      if ((input.prev_out.value / 100000000) != amount) {
        throw new NotAcceptable("Transaction amount is not equal to order amount");
      }
    } catch (err) {
      throw err;
    }
  }

  async toGVT(amount) {
    var usd = exchange.toBTC(amount, 'USD');
    return usd;
  }

  async currentUSD() {
    var usd = exchange.toBTC(1, 'USD');
    return usd;
  }
}
