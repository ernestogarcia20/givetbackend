/* eslint-disable no-unused-vars */
const {
  NotAcceptable,
  NotImplemented
} = require('@feathersjs/errors');

const axios = require('axios');

const apiKey = 'Y9FXRIC2GKH7MTKDYN9UUFGZJ7R5NVHF8X';
const testHash = "0xc31c721ef6ef2d1aaaf0218ce3ce7f18789eebd087d6f22299ee9652dfa4ad6c";

exports.Ethereum = class Ethereum {
  constructor(options) {
    this.options = options || {};
  }

  async find(params) {
    return [];
  }


  async update(id, data, params) {
    return data;
  }

  async patch(id, data, params) {
    return data;
  }

  async remove(id, params) {
    return {
      id
    };
  }


  async verifyHash(hash) {
    var options = {};

    try {

      //var rs = await axios.get(`https://api.etherscan.io/api?module=proxy&action=eth_getTransactionByHash&txhash=${hash}&apikey=${apiKey}`);
      var rs = await axios.get(`https://api.etherscan.io/api?module=proxy&action=eth_getTransactionReceipt&txhash=${hash}&apikey=${apiKey}`)
      if (rs.data.error) {
        throw new NotAcceptable(rs.data.error);
        return;
      }
      if (!rs.data.result) {
        throw new NotAcceptable("Hash not valid");
        return;
      }
      var transaction = rs.data.result;

      rs = await axios.get(`https://api.etherscan.io/api?module=proxy&action=eth_blockNumber&apikey=${apiKey}`)
      var blockCount = rs.data.result;

      var confirmations = parseInt(blockCount, 16) - parseInt(transaction.blockNumber, 16);

      return {
        confirmations,
        amount: parseInt(transaction.logs[0].data, 16) / 10e5,
        ...transaction
      };
    } catch (err) {
      try {
        var oError = JSON.parse(err);
        throw new NotAcceptable(oError.reason);
      } catch (s) {
        throw new NotAcceptable(err);
      }
    }
  }

  async verifyPayment(hash, buyUser, sellUser, amount) {
    try {
      var transaction = await this.verifyHash(hash);

      if (transaction.confirmations < 3) {
        throw new NotAcceptable("Transaction not confirmed yet")
      }
      // sender
      if (transaction.from != buyUser.btc) {
        throw new NotAcceptable("Transaction sender is not the buyer");
      }
      // receiver
      if (transaction.to != sellUser.btc) {
        throw new NotAcceptable("Transaction receiver is not the seller");
      }
      // amount
      if ((transaction.amount) != amount) {
        throw new NotAcceptable("Transaction amount is not equal to order amount");
      }
    } catch (err) {
      throw err;
    }
  }

  async toGVT(amount) {
    var usd = exchange.toBTC(amount, 'USD');
    return usd;
  }

  async currentUSD() {
    var rs = await axios(`https://api.etherscan.io/api?module=stats&action=ethprice&apikey=${apiKey}`);

    return rs.data.result.ethusd * 1;
  }
}
