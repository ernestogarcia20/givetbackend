const {
  NotAcceptable,
  NotImplemented
} = require('@feathersjs/errors');

const docs = require('./usd.service.docs');

module.exports = function (app) {

  const servicePath = 'api/ethereum/exchange/usd';

  const sequelize = app.get('sequelizeClient');
  var service = {
    docs,
    options: {},

    async find(params) {
      var usd = await app.service('api/ethereum').currentUSD();
      return usd;
    },

  };

  app.use(servicePath, service);

  app.service(servicePath).hooks({});

}
