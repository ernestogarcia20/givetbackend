const {
  NotAcceptable,
  NotImplemented
} = require('@feathersjs/errors');

const docs = require('./verify-hash.service.docs');

module.exports = function (app) {

  const servicePath = 'api/ethereum/verify-hash';

  const sequelize = app.get('sequelizeClient');
  var service = {
    docs,
    options: {},

    async create(data, params) {
      return app.service('api/ethereum').verifyHash(data.hash);
    },

  };

  app.use(servicePath, service);

  app.service(servicePath).hooks({});

}
