module.exports = {
  description: 'usd value of eth',

  schemas: {
    ok: {}
  },
  operations: {
    find: {
      summary: 'gets the usd value of 1 eth',
      description: '',
      parameters: [],
      'responses.200': {
        "content": {
          "application/json": {
            schema: {
              "$ref": "#/components/schemas/ok"
            }
          }
        }
      }
    }
  }
}
