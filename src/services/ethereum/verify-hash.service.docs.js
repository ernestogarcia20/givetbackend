module.exports = {

  model: 'hash',
  schemas: {
    ok: {}
  },
  operations: {
    create: {
      "requestBody": {
        "required": true,
        "content": {
          "application/json": {
            "schema": {
              type: "object",
              properties: {
                hash: {
                  type: 'string'
                }
              }
            },
            examples: {
              'hash confirmado': {
                value: {
                  hash: "0xf1c834164661af0de8af1d20145ad69501545683aa9a9927f0d6cfc05a65205c"
                }
              },
              'hash inválido o no confirmado': {
                value: {
                  hash: "0xaa1c721ef6ef2d1aaaf0218ce3ce7f18789eebd087d6f22299ee9652dfa4ad6c"
                }
              },
              'otro hash válido': {
                value: {
                  hash: "0xb44a983d972946858eec3b66767843e85f20cc928ef710bc67425a0c79d2034d"
                }
              }
            }
          }
        }
      },
      'responses.200': {
        "content": {
          "application/json": {
            schema: {
              "$ref": "#/components/schemas/ok"
            }
          }
        }
      }
    }
  }
}
