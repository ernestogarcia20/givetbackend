// aliases.js

//TODO: Revisar cómo hacer plugins.
// para camelizar servicios y/o eliminar los '-' de nombres
// el plugin sería para reemplazar app.configure
// este ejemplo sale de https://github.com/feathersjs/feathers/issues/856

module.exports = (aliases = {}) => {
  return app => {
    const oldService = app.api.service;

    Object.assign(app, {
      aliases,
      service(path, ...args) {
        const finalPath = this.aliases[path] ? this.aliases[path] : path;
        return oldService.call(this, finalPath, ...args);
      }
    })
  }
}

const aliases = require('./aliases');

app.configure(aliases({
  // userVerify: 'users/:userId/verify'
}));
