module.exports = {
  model: 'user_response',
  schemas: {
    user_create: {
      "type": "object",
      "properties": {
        "email": {
          "type": "string",
          "maxLength": 45
        },
        "userName": {
          "type": "string",
          "maxLength": 45,
          "nullable": true
        },
        "fullName": {
          "type": "string",
          "maxLength": 45
        },
        "countryCode": {
          "type": "string",
          "maxLength": 5
        },
        "phone": {
          "type": "string",
          "maxLength": 45,
          "nullable": true
        },
        "password": {
          "type": "string",
          "maxLength": 100
        },
        "password2": {
          "type": "string",
          "maxLength": 100
        },
        "referralUserName": {
          "type": "string",
          "maxLength": 45,
          "nullable": true
        },
        "btc": {
          "type": "string",
          "maxLength": 100,
          "nullable": true
        },
        "fcm_token": {
          "type": "string",
          "maxLength": 255,
          "nullable": true
        }
      }
    },
    user_response: {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int32"
        },
        "email": {
          "type": "string",
          "maxLength": 45
        },
        "userName": {
          "type": "string",
          "maxLength": 45,
          "nullable": true
        },
        "fullName": {
          "type": "string",
          "maxLength": 45
        },
        "countryCode": {
          "type": "string",
          "maxLength": 5
        },
        "phone": {
          "type": "string",
          "maxLength": 45,
          "nullable": true
        },
        "referralUserName": {
          "type": "string",
          "maxLength": 45,
          "nullable": true
        },
        "btc": {
          "type": "string",
          "maxLength": 100,
          "nullable": true
        },
        "rating": {
          "type": "number",
          "nullable": true
        },
        "locked": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "deleted": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "isAdmin": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "createdAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        },
        "updatedAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        },
        "fcm_token": {
          "type": "string",
          "maxLength": 255,
          "nullable": true
        }
      },
    }
  },
  operations: {
    find: {
      'parameters[3]': {
        description: 'Query parameters to filter',
        in: 'query',
        name: 'filter',
        style: 'form',
        explode: true,
        schema: {
          type: 'object',
          //$ref: '#/components/schemas/user_input'
        },
        examples: {
          'all users': {
            value: {

            }
          },
          'admin users': {
            value: {
              isAdmin: 1
            }
          },
          'business users': {
            value: {
              hasBusiness: 1
            }
          },
          'roleless users': {
            value: {
              "hasBusiness": 0,
              "isAdmin": 0
            }
          }

        }
      }
    },
    create: {
      requestBody: {
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/user_create'
            },
            examples: {
              'test': {
                value: {
                  "email": "elpinzon@gmail.com",
                  "userName": "elpinzon",
                  "fullName": "el pinzon",
                  "countryCode": "VE",
                  "phone": "12345",
                  "password": "123",
                  "password2": "1234",
                  "referralUserName": "admin",
                  "btc": "xxx-yyy"
                }
              }
            }
          }
        }
      }
    },
    patch: {
      requestBody: {
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/user_create'
            }
          }
        }
      }
    },
    update: false,
    remove: false
  }
}
