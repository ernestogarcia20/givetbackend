module.exports = async function (context) {
  if (!context.params.route || !context.params.route.idUser) {
    return context;
  }
  var sequelize = context.app.get('sequelizeClient');
  var data = await sequelize.query(`call findOrders(:hUser);`, {
    replacements: {
      hUser: context.params.route.idUser
    }
  });
  var usd = await context.app.service('api/ethereum').currentUSD();
  data.forEach(element => {
    element.amountGVT = Number((element.amount / usd).toFixed(8));
  });

  var ordered = data.sort(function (a, b) {
    return (a.createdAt - b.createdAt) * -1; //descending
  });

  context.result = {
    data: ordered
  };

}
