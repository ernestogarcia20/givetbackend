const docs = require('./messages.service.docs');

module.exports = function (app) {

  const servicePath = 'api/users/:idUser/messages';

  const sequelize = app.get('sequelizeClient');

  var service = {
    docs,
    options: {},
    async find(params) {

      const {
        idUser,
        otherUser: withUser
      } = params.route;
      var data = await app.service('api/messages').find({
        query: {
          hUserTo: idUser,
          $sort: {
            id: -1
          },
        }
      });

      return data;
    }
  };

  app.use(servicePath, service);

  app.service(servicePath).hooks({});

}
