const docs = require('./transfer-balance.service.docs');
const {
  authenticate
} = require('@feathersjs/authentication').hooks;

const {
  Forbidden,
  NotAcceptable
} = require('@feathersjs/errors');


module.exports = function (app) {

  const servicePath = '/users/:idUser/transfer-balance';

  const sequelize = app.get('sequelizeClient');
  var service = {
    docs,
    options: {},
    async create(data, params) {

      var balance = await app.service('api/users/:idUser/balance').getBalance(params.route.idUser);

      if (data.amount > balance.available.btc) {
        throw new NotAcceptable("Insufficient balance");
      }

      //substract amount
      var toUser = await app.service('api/users').get(data.toUser);
      data.description = 'Transfer to ' + toUser.userName;
      var debit = await app.service('api/wallet').create({
        hUser: params.route.idUser,
        op: '-',
        reason: 'TRANSFER',
        amount: data.amount,
        hRef: toUser.id,
        description: 'Transfer to ' + toUser.userName
      }, params)

      //receive amount
      var fromUser = await app.service('api/users').get(params.route.idUser);
      var credit = await app.service('api/wallet').create({
        hUser: data.toUser,
        op: '+',
        reason: 'TRANSFER',
        amount: data.amount,
        hRef: fromUser.id,
        description: 'Transfer from ' + fromUser.userName
      }, params)

      return {
        debit,
        credit
      };
    }
  };

  app.api.use(servicePath, service);

  app.api.service(servicePath).hooks({
    before: {
      all: [authenticate('jwt')]
    }
  });

}
