module.exports = {
  description: 'resend wallet password',
  model: '',
  schemas: {
    message: {}
  },
  operations: {
    create: {
      parameters: [{
        in: 'path',
        name: 'idUser',
        style: 'form',
        schema: {
          type: 'string'
        },
        required: true,
        description: 'user id'
      }],

      'responses.201': {
        "content": {
          "application/json": {
            schema: {
              "$ref": "#/components/schemas/message"
            }
          }
        }
      }
    }
  }
}
