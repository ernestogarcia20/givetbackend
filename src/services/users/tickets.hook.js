module.exports = async function (context) {
  if (!context.params.route || !context.params.route.idUser) {
    return context;
  }

  var data = await context.app.service('/api/tickets').find({
    query: {
      hUser: context.params.route.idUser
    }
  });

  context.result = {
    data: data
  };
}
