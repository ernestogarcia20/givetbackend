module.exports = {
  description: 'user tickets',
  operations: {
    find: {
      description: 'get the tickets by user',
      summary: 'get user orders',
      parameters: [{
        in: 'path',
        name: 'idUser',
        schema: {
          type: 'string'
        },
        required: true,
        description: 'user id'
      }],
    },

  }
}
