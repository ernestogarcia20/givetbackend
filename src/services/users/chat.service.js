const docs = require('./chat.service.docs');

module.exports = function (app) {

  const servicePath = '/users/:idUser/chat/:withUser';

  const sequelize = app.get('sequelizeClient');

  var service = {
    docs,
    options: {},
    async find(params) {

      const {
        idUser,
        withUser
      } = params.route;
      var data = await app.service('api/chat').find({
        query: {
          fromId: [idUser, withUser],
          toId: [idUser, withUser],
          $sort: {
            id: -1
          },
        }
      });

      return data;
    }
  };

  app.api.use(servicePath, service);

  app.api.service(servicePath).hooks({});

}
