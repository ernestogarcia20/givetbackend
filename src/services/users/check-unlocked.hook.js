const {
  NotAcceptable,
  NotImplemented
} = require('@feathersjs/errors');

module.exports = async function (context) {

  if (!context.params.user) {
    return context;
  }

  if (!context.params.user.isAdmin && context.params.user.locked == 1) {
    throw new NotAcceptable("User is blocked");
  }

  var user = context.params.user;
  if (!user.email || !user.userName || !user.fullName || !user.countryCode || !user.phone || !user.btc) {
    throw new NotAcceptable("Uncompleted User Profile. Check email,userName,fullName,countryCode,phone,btcAddress");
  }

}
