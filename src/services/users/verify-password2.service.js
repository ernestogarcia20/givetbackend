const docs = require('./verify-password2.service.docs');

const {
  Forbidden,
} = require('@feathersjs/errors');


module.exports = function (app) {

  const servicePath = '/users/:idUser/verify-password2';

  const sequelize = app.get('sequelizeClient');
  var service = {
    docs,
    options: {},
    async create(data, params) {

      var user = await app.service('/api/users').get(params.route.idUser);

      if (user.password2 != data.password2) {
        return new Forbidden('Invalid password');
      }
      return {};
    }
  };

  app.api.use(servicePath, service);

  app.api.service(servicePath).hooks({});

}
