const docs = require('./business.service.docs');

module.exports = function (app) {

  const servicePath = 'api/users/:idUser/business';

  const sequelize = app.get('sequelizeClient');

  var service = {
    docs,
    options: {},
    async find(params) {

      const {
        idUser,
        otherUser: withUser
      } = params.route;
      var data = await app.service('api/business').find({
        query: {
          hUser: idUser
        }
      });

      return data;
    }
  };

  app.use(servicePath, service);

  app.service(servicePath).hooks({});

}
