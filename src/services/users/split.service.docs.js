module.exports = {
  description: '',
  model: 'split_response',
  schemas: {
    split_response: {
      "type": "object",
      "properties": {
        "available": {
          type: "number"
        },
        "user": {
          type: "number"
        },
        "network": {
          type: "number"
        },
      }
    }
  },
  operations: {
    find: {
      description: ' ',
      parameters: [{
        in: 'path',
        name: 'idUser',
        style: 'form',
        schema: {
          type: 'string'
        },
        required: true,
        description: 'user id'
      }],
      'responses.200': {
        "content": {
          "application/json": {
            schema: {
              "$ref": "#/components/schemas/split_response"
            }
          }
        }
      }
    }
  }
}
