module.exports = async function (context) {

  if (!context.params.route || !context.params.route.idUser) {
    return context;
  }

  var sequelize = context.app.get('sequelizeClient');

  var id = context.params.route.idUser;

  var tree = [];

  async function add(root) {
    var children = await sequelize.query(`
      select id,hParent,userName,fullName,email 
      from v_users 
      where hParent=:id
  `, {
      replacements: {
        id: root.id
      },
      raw: true,
      type: sequelize.QueryTypes.SELECT
    });

    root.children = children;
    root.icon = 'person';
    root.label = root.fullName;
    root.email = root.email;

    for (var i = 0; i < children.length; i++) {
      var user = children[i];

      var files = await context.app.service('api/files').find({query:{
        type: 'avatar',
        hType: user.id,
        $limit: 1
      }})
      if (files.total > 0) {
        user.avatar = context.app.get('host') + '/files/' + files.data[0].assignedName
      }

      await add(user);
    }

  }

  var root = await context.app.service('api/users').get(id);

  tree.push(root);
  await add(root);



  context.result = tree;

  return context;
}
