const docs = require('./balance.service.docs');

module.exports = function (app) {

  const servicePath = '/users/:idUser/balance';

  const sequelize = app.get('sequelizeClient');
  var service = {
    docs,
    options: {},
    async find(params) {
      return this.getBalance(params.route.idUser);
    },

    //
    async getBalance(idUser) {
      var data = await sequelize.query(`call userBalance(:hUser);`, {
        replacements: {
          hUser: idUser
        }
      });

      //
      var usd = await app.service('api/ethereum').currentUSD();
      var balance = {
        ...data[0],
        availableGVT: data[0].available / usd
      };
      //

      return {
        ...balance
      };
    }
  };

  app.api.use(servicePath, service);

  app.api.service(servicePath).hooks({});

}
