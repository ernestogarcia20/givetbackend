const docs = require('./resend-password2.service.docs');

const {
  Forbidden,
} = require('@feathersjs/errors');
const accountService = require("../authmanagement/notifier");

module.exports = function (app) {

  const servicePath = '/users/:idUser/resend-password2';

  const sequelize = app.get('sequelizeClient');
  var service = {
    docs,
    options: {},
    async create(data, params) {
      var user = await app.service('api/users').get(params.route.idUser);
      accountService(app).notifier(
        "resendPwd2",
        user
      );
      return {
        message: "An email has been sent"
      };
    }
  };

  app.api.use(servicePath, service);

  app.api.service(servicePath).hooks({});

}
