const docs = require("./split.service.docs");

module.exports = function(app) {
  const servicePath = "/users/:idUser/split";

  const sequelize = app.get("sequelizeClient");
  var service = {
    docs,
    options: {},
    async find(params) {
      var userBalance = await app
        .service("api/users/:idUser/balance")
        .getBalance(params.route.idUser);

      var systemBalance = await app.service("api/system/balance").find();

      var networkSplit =
        systemBalance.available +
        systemBalance.noFreed +
        systemBalance.freed +
        systemBalance.commissionNoAvailable;

      return {
        available: 5250000000 - networkSplit,
        user:
          userBalance.available +
          userBalance.noFreed +
          userBalance.freed +
          userBalance.commissionNoAvailable,
        network: networkSplit
      };
    }
  };

  app.api.use(servicePath, service);

  app.api.service(servicePath).hooks({});
};
