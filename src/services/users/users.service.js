// Initializes the `users` service on path `/users`
const {
  Users
} = require('./users.class');
const createModel = require('../../models/users.model');
const hooks = require('./users.hooks');
const docs = require('./users.service.docs');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate,
    multi: true
  };

  const servicePath = '/api/users';
  var serviceObject = new Users(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  service.hooks(hooks);

  // 
  const lastBuyService = require('./lastBuy.service');
  app.configure(lastBuyService);

  //
  const treeViewService = require('./treeView.service');
  app.configure(treeViewService);

  //
  const ordersService = require('./orders.service');
  app.configure(ordersService);

  //
  const ticketService = require('./tickets.service');
  app.configure(ticketService);

  //
  const walletService = require('./wallet.service');
  app.configure(walletService);

  //
  const balanceService = require('./balance.service');
  app.configure(balanceService);

  //
  const chatService = require('./chat.service');
  app.configure(chatService);

  //
  const chatSupportService = require('./chat-support.service');
  app.configure(chatSupportService);

  //
  const messagesService = require('./messages.service');
  app.configure(messagesService);

  //
  const businessService = require('./business.service');
  app.configure(businessService);

  //
  const splitService = require('./split.service');
  app.configure(splitService);

  //
  const verifyP2Service = require('./verify-password2.service');
  app.configure(verifyP2Service);

  //
  const resendPService = require('./resend-password.service');
  app.configure(resendPService);

  //
  const resendP2Service = require('./resend-password2.service');
  app.configure(resendP2Service);

  //
  const payUnlock = require('./pay-unlock.service');
  app.configure(payUnlock);

  //
  const transferBalance = require('./transfer-balance.service');
  app.configure(transferBalance);

  //
  const withdraw = require('./withdraw.service');
  app.configure(withdraw);

  //
  const lockUser = require('./lock-user');
  app.configure(lockUser);
};
