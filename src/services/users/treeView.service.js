const createModel = require('../../models/users.model');
const hook = require('./treeView.hook');
const docs = require('./treeView.service.docs');

module.exports = function (app) {

  const servicePath = '/users/:idUser/tree-view';

  var service = {
    docs,
    options: {
      Model: createModel(app)
    },
    async find() {
      return resolve('hook');
    }
  };

  app.api.use(servicePath, service);

  app.api.service(servicePath).hooks({
    before: {
      find: [hook]
    }
  });

}
