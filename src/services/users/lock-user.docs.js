module.exports = {
  description: 'block user',
  model: '',
  schemas: {
    message: {}
  },
  operations: {
    find: {
      parameters: [{
        in: 'path',
        name: 'idUser',
        style: 'form',
        schema: {
          type: 'string'
        },
        required: true,
        description: 'user id'
      }],

      'responses.201': {
        "content": {
          "application/json": {
            schema: {
              "$ref": "#/components/schemas/user"
            }
          }
        }
      }
    }
  }
}
