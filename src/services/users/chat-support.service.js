const docs = require('./chat-support.service.docs');

module.exports = function (app) {

  const servicePath = '/users/:idUser/chat-support';

  const sequelize = app.get('sequelizeClient');

  var service = {
    docs,
    options: {},
    async find(params) {

      const {
        idUser,
        withUser
      } = params.route;
      var data = await app.service('api/chat').find({
        query: {
          fromId: idUser,
          isSupport: 1,
          $sort: {
            id: -1
          },
        }
      });

      return data;
    }
  };

  app.api.use(servicePath, service);

  app.api.service(servicePath).hooks({});

}
