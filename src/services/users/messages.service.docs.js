module.exports = {
  //description: 'business service',
  model: 'messages',

  operations: {
    find: {
      //description: '',
      parameters: [{
        in: 'path',
        name: 'idUser',
        style: 'form',
        schema: {
          type: 'string'
        },
        required: true,
        description: 'user id'
      }],

    }
  }
}
