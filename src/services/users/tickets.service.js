//const createModel = require('../../models/give_get.model');
const hook = require('./tickets.hook');
const docs = require('./tickets.service.docs');

module.exports = function (app) {

  const servicePath = '/users/:idUser/tickets';

  var service = {
    docs,
    options: {
      //Model: createModel(app)
    },
    async find() {
      return resolve('hook');
    }
  };

  app.api.use(servicePath, service);

  app.api.service(servicePath).hooks({
    before: {
      find: [hook]
    }
  });

}
