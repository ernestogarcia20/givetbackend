const docs = require('./pay-unlock.service.docs');

const {
  Forbidden,
} = require('@feathersjs/errors');


module.exports = function (app) {

  const servicePath = '/users/:idUser/pay-unlock';

  const sequelize = app.get('sequelizeClient');
  var service = {
    docs,
    options: {},
    async find(params) {

      var rs = app.service('api/wallet').create({
        hUser: params.route.idUser,
        op: "-",
        reason: 'UNLOCK',
        description: 'Pay for Penalty'
      })

      return rs;
    }
  };

  app.api.use(servicePath, service);

  app.api.service(servicePath).hooks({});

}
