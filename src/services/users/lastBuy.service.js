const createModel = require('../../models/buy_tickets.model');
const docs = require('./lastBuy.service.docs');

module.exports = function (app) {

  const servicePath = '/users/:idUser/last-buy';

  var service = {
    docs,
    options: {
      Model: createModel(app)
    },

    async find(params) { //revisar qué debe ser maxToSell

      var idUser = params.route.idUser;


      var rsTickets = await app.api.service('/buy_ticket').find({
        query: {
          hUser: idUser,
          $sort: {
            createdAt: -1
          },
          $limit: 1
        }
      });

      var rs = {};
      if (rsTickets.total > 0) {
        rs = rsTickets.data[0];

        var userBalance = await app.service('api/users/:idUser/balance').getBalance(idUser);

        rs.maxToSell = (rs.amount * (1 + userBalance.currentPercentage / 100)).toFixed(8);

      } else {
        var w = await app.api.service('wallet').find({
          query: {
            hUser: idUser,
            reason: 'INITIAL_BALANCE'
          }
        });
        if (w.data[0]) {
          rs = {
            maxToSell: w.data[0].available
          }
        } else {
          rs = {
            maxToSell: 0,
            amount: 0
          }
        }
      }

      return rs;

    }
  };

  app.api.use(servicePath, service);

  app.api.service(servicePath).hooks({});

}
