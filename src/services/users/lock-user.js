const docs = require('./lock-user.docs');

const {
  Forbidden,
} = require('@feathersjs/errors');


module.exports = function (app) {

  const servicePath = '/users/:idUser/lock-user';

  const sequelize = app.get('sequelizeClient');
  var service = {
    docs,
    options: {},
    async create(data, params) {

      var rs = app.service('api/users').patch(params.route.idUser, {
        locked: 1
      })

      return rs;
    }
  };

  app.api.use(servicePath, service);

  app.api.service(servicePath).hooks({});

}
