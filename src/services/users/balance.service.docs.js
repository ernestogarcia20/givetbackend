module.exports = {
  description: 'system balance',
  model: 'balance_response',
  schemas: {
    balance_response: {
      "type": "object",
      "properties": {
        "available": {
          type: "number"
        },
        "availableGVT": {
          type: "number"
        },
        "noAvailable": {
          type: "number"
        },
        "noFreed": {
          type: "number"
        },
        "freed": {
          type: "number"
        },
        "commissionNoAvailable": {
          type: "number"
        },
        "currentPercentage": {
          type: "number"
        },
        "lastPack": {
          type: "number"
        },
        "lastPackValue": {
          type: "number"
        },
      }
    }
  },
  operations: {
    find: {
      description: 'wallet resume ',
      parameters: [{
        in: 'path',
        name: 'idUser',
        style: 'form',
        schema: {
          type: 'string'
        },
        required: true,
        description: 'user id'
      }],
      'responses.200': {
        "content": {
          "application/json": {
            schema: {
              "$ref": "#/components/schemas/balance_response"
            }
          }
        }
      }
    }
  }
}
