module.exports = {
  description: 'user orders',
  model: 'orders_response',
  operations: {
    find: {
      description: 'get the orders by user',
      summary: 'get user orders',
      parameters: [{
        in: 'path',
        name: 'idUser',
        schema: {
          type: 'string'
        },
        required: true,
        description: 'user id'
      }],
    },

  }
}
