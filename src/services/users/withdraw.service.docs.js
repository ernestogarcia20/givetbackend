module.exports = {
  description: 'withdraw',
  model: 'wallet_response',
  schemas: {
    withdraw_create: {
      type: 'object',
      properties: {
        "amount": {
          "type": "number",
          "nullable": true
        }
      }
    }
  },
  operations: {
    create: {
      //description: '',
      parameters: [{
        in: 'path',
        name: 'idUser',
        style: 'form',
        schema: {
          type: 'string'
        },
        required: true,
        description: 'user id'
      }],
      "requestBody": {
        "required": true,
        "content": {
          "application/json": {
            "schema": {
              "$ref": "#/components/schemas/withdraw_create"
            }
          }
        }
      },

      'responses.200': {
        "content": {
          "application/json": {
            schema: {
              "$ref": "#/components/schemas/wallet_response"
            }
          }
        }
      }
    }
  }
}
