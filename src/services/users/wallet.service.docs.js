module.exports = {
  description: 'wallet',
  model: 'wallet_response',
  operations: {
    find: {
      description: 'get the users wallet',
      summary: '',
      parameters: [{
        in: 'path',
        name: 'idUser',
        schema: {
          type: 'string'
        },
        required: true,
        description: 'user id'
      }],
    },

  }
}
