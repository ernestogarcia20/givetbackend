const createModel = require('../../models/orders.model');
const hook = require('./orders.hook');
const docs = require('./orders.service.docs');

module.exports = function (app) {

  const servicePath = '/users/:idUser/orders';

  var service = {
    docs,
    options: {
      Model: createModel(app)
    },
    async find() {
      return resolve('hook');
    }
  };

  app.api.use(servicePath, service);

  app.api.service(servicePath).hooks({
    before: {
      find: [hook]
    }
  });

}
