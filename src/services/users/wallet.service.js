const docs = require('./wallet.service.docs');

module.exports = function (app) {

  const servicePath = 'api/users/:idUser/wallet';

  var service = {
    docs,
    options: {},
    async find(params) {
      return app.service('api/wallet').find({
        query: {
          hUser: params.route.idUser,
          $sort: {
            createdAt: -1
          }
        }
      })

    }
  };

  app.use(servicePath, service);

  app.service(servicePath).hooks({});

}
