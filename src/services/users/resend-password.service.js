const docs = require('./resend-password.service.docs');

const {
  Forbidden,
} = require('@feathersjs/errors');
const accountService = require("../authmanagement/notifier");

module.exports = function (app) {

  const servicePath = '/users/resend-password';

  const sequelize = app.get('sequelizeClient');
  var service = {
    docs,
    options: {},
    async create(data, params) {
      var users = await app.service('api/users').find({
        query: {
          email: data.email
        }
      });
      if (users.total == 0) {
        return {
          message: "email not found"
        }
      }

      accountService(app).notifier(
        "resendPwd",
        users.data[0]
      );
      return {
        message: "An email has been sent"
      };
    }
  };

  app.api.use(servicePath, service);

  app.api.service(servicePath).hooks({});

}
