module.exports = {
  description: 'resend password',
  model: '',
  schemas: {
    email: {
      type: "object",
      properties: {
        email: {
          type: "string"
        }
      }
    },
    message: {}
  },
  operations: {
    create: {
      parameters: [],
      requestBody: {
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/email"
            },
            examples: {
              default: {
                value: {
                  email: "test@test.com"
                }
              }
            }
          }
        }
      },
      'responses.201': {
        "content": {
          "application/json": {
            schema: {
              "$ref": "#/components/schemas/message"
            }
          }
        }
      }
    }
  }
}
