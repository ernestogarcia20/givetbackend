const common = require("feathers-hooks-common");
const {
  authenticate
} = require("@feathersjs/authentication").hooks;

const {
  hashPassword,
  protect
} = require("@feathersjs/authentication-local").hooks;

const verifyHooks = require("feathers-authentication-management").hooks;
const accountService = require("../authmanagement/notifier");

const viewTree = require("./treeView.hook");
const userName = require("./userName.hook");

module.exports = {
  before: {
    all: [],
    //find: [authenticate('jwt'), lastGive, viewTree],
    find: [authenticate("jwt"), viewTree],
    get: [authenticate("jwt")],
    create: [
      hashPassword("password"),
      hashPassword("password2"),
      userName,
      verifyHooks.addVerification("/api/authManagement")
    ],
    update: [
      hashPassword("password"),
      hashPassword("password2"),
      authenticate("jwt"),
      userName
    ],
    patch: [
      common.iff(
        common.isProvider("external"),
        common.preventChanges(
          "isVerified",
          "verifyToken",
          "verifyShortToken",
          "verifyExpires",
          "verifyChanges",
          "resetToken",
          "resetShortToken",
          "resetExpires"
        )
      ),
      hashPassword("password"),
      hashPassword("password2"),
      authenticate("jwt")
    ],
    remove: [authenticate("jwt")]
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect("password"),
      protect("password2")
    ],
    find: [setAvatar],
    get: [setAvatar],
    create: [
      context => {
        accountService(context.app).notifier(
          "resendVerifySignup",
          context.result
        );
      },
      verifyHooks.removeVerification()
    ],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};


async function setAvatar(context){
  var data=context.result.data || [context.result];
  for(var i=0;i<data.length;i++){
    var user=data[i];
    var files = await context.app.service('api/files').find({query:{
      type: 'avatar',
      hType: user.id,
      $limit: 1
    }})
    if (files.total > 0) {
      user.avatar = context.app.get('host') + '/files/' + files.data[0].assignedName
    }
  }
}
