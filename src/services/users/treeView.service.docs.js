module.exports = {
  description: 'users tree',
  operations: {
    find: {
      description: 'get the tree',
      summary: 'get tree',
      parameters: [{
        in: 'path',
        name: 'idUser',
        schema: {
          type: 'string'
        },
        required: true,
        description: 'user id'
      }],
      responses: {
        '200': {
          content: {
            'application/json': {
              schema: {
                title: 'user',
                type: 'object',
                properties: {
                  "id": {
                    "type": "integer",
                    "format": "int32"
                  },
                  "hParent": {
                    "type": "integer",
                    "format": "int32"
                  },
                  userName: {
                    type: 'string'
                  },
                  "fullName": {
                    "type": "string"
                  },
                  "email": {
                    "type": "string",
                    "maxLength": 45
                  },
                  icon: {
                    type: 'string'
                  },
                  label: {
                    type: 'string'
                  },
                  children: {
                    type: 'array',
                    items: {
                      "$ref": "#/components/schemas/users"
                    }
                  }
                }
              }
            }
          }
        },

      }
    }
  }
}
