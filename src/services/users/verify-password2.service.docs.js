module.exports = {
  description: 'verify wallet password',
  model: '',
  schemas: {
    psw2: {
      type: "object",
      properties: {
        password2: {
          type: "string"
        }
      }
    },
    ok: {}
  },
  operations: {
    create: {

      parameters: [{
        in: 'path',
        name: 'idUser',
        style: 'form',
        schema: {
          type: 'string'
        },
        required: true,
        description: 'user id'
      }],
      requestBody: {
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/psw2"
            },
            examples: {
              default: {
                value: {
                  password2: "123"
                }
              }
            }
          }
        }
      },
      'responses.201': {
        "content": {
          "application/json": {
            schema: {
              "$ref": "#/components/schemas/ok"
            }
          }
        }
      }
    }
  }
}
