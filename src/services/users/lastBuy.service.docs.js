module.exports = {
  description: 'Last users buy',
  model: 'lastbuy_ticket_response',
  schemas: {
    lastbuy_ticket_response: {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int32"
        },
        "hUser": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "amount": {
          "type": "number",
          "nullable": true
        },
        "balance": {
          "type": "number",
          "nullable": true
        },
        "status": {
          "type": "string",
          "maxLength": 20,
          "nullable": true
        },
        "gain": {
          "type": "integer",
          "format": "int32",
          "nullable": true,
          "default": "0"
        },
        "createdAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        },
        "updatedAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        },
        "finishAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        },
        "hPack": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "maxToSell": {
          "type": "number",
        }
      },

    }
  },
  operations: {
    find: {
      description: 'get the last purchase from users',
      summary: 'get last buy from user',
      parameters: [{
        in: 'path',
        name: 'idUser',
        schema: {
          type: 'string'
        },
        required: true,
        description: 'user id'
      }],
    },

  }
}
