module.exports = {
  description: 'transfer balance',
  model: 'wallet_response',
  schemas: {
    transfer_create: {
      type: 'object',
      properties: {
        "toUser": {
          "type": "integer",
          "nullable": true
        },
        "amount": {
          "type": "number",
          "nullable": true
        }
      }
    }
  },
  operations: {
    create: {
      parameters: [{
        in: 'path',
        name: 'idUser',
        style: 'form',
        schema: {
          type: 'string'
        },
        required: true,
        description: 'user id'
      }],
      "requestBody": {
        "required": true,
        "content": {
          "application/json": {
            "schema": {
              "$ref": "#/components/schemas/transfer_create"
            }
          }
        }
      },

      'responses.200': {
        "content": {
          "application/json": {
            schema: {
              "$ref": "#/components/schemas/wallet_response"
            }
          }
        }
      }
    }
  }
}
