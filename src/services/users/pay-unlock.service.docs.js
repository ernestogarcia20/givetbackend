module.exports = {
  description: 'pay penallity to unlock',
  model: '',
  schemas: {
    message: {}
  },
  operations: {
    find: {
      parameters: [{
        in: 'path',
        name: 'idUser',
        style: 'form',
        schema: {
          type: 'string'
        },
        required: true,
        description: 'user id'
      }],

      'responses.200': {
        "content": {
          "application/json": {
            schema: {
              "$ref": "#/components/schemas/wallet_response"
            }
          }
        }
      }
    }
  }
}
