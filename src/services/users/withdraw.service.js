const docs = require('./withdraw.service.docs');
const {
  authenticate
} = require('@feathersjs/authentication').hooks;

const {
  Forbidden,
  NotAcceptable
} = require('@feathersjs/errors');


module.exports = function (app) {

  const servicePath = '/users/:idUser/withdraw';

  const sequelize = app.get('sequelizeClient');
  var service = {
    docs,
    options: {},
    async create(data, params) {

      var balance = await app.service('api/users/:idUser/balance').getBalance(params.route.idUser);

      if (data.amount > balance.available) {
        throw new NotAcceptable("Insufficient balance");
      }

      //substract amount

      var debit = await app.service('api/wallet').create({
        hUser: params.route.idUser,
        op: '-',
        reason: 'WITHDRAW',
        amount: data.amount
      }, params)

      return debit;
    }
  };

  app.api.use(servicePath, service);

  app.api.service(servicePath).hooks({
    before: {
      all: [authenticate('jwt')]
    }
  });

}
