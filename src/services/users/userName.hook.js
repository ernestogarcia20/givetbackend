module.exports = async function (context) {

  if (!context.data.userName) {
    context.data.userName = context.data.email;
  }

  return context;
}
