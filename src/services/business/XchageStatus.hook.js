const {
  BadRequest
} = require('@feathersjs/errors');

module.exports = async function (context) {
  var data = context.data;
  var result = context.result;

  if (!data.status) {
    return;
  }
  if (['Pending', 'Approved', 'Rejected'].indexOf(data.status) == -1) {
    throw new BadRequest("Invalid state value. Must be 'Pending', 'Approved', 'Rejected'");
  }

  var old = await context.service.get(context.id);
  if (data.status == old.status) {
    return;
  }

  if (data.status == 'Approved') {
    await context.app.service('/api/messages').create({
      message: `Your business ${data.businessName} has been approved`
    });
  }

  if (data.status == 'Rejected') {
    await context.app.service('/api/messages').create({
      message: `Your business ${data.businessName} has been rejected`
    });
  }


};
