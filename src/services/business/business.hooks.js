const {
  authenticate
} = require('@feathersjs/authentication').hooks;


async function getData(context) {
  context.old = await context.service.get(context.id);
}

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [join],
    get: [join],
    create: [],
    update: [getData],
    patch: [getData],
    remove: []
  },

  after: {
    all: [],
    find: [agregates],
    get: [agregates],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};

function join(context) {

  const sequelize = context.app.get('sequelizeClient');

  context.params.sequelize.include = include = [{
    model: sequelize.models.business_category,
    attributes: sequelize.models.business_category.include_attributes
  }, {
    model: sequelize.models.users,
    attributes: sequelize.models.users.include_attributes
  }]

  return context;
}

async function agregates(context){
  var data=context.result.data || [context.result];
  for(var i=0;i<data.length;i++){

    var r=data[i];

    r.gradeName = 'Bronce, plata, oro, ruby ,esmeralda, diamante'.split(',')[r.grade]
    //
    var user = r.user;
    var files = await context.app.service('api/files').find({
      type: 'avatar',
      hType: user.id,
      $limit: 1
    })
    if (files.total > 0) {
      user.avatar = context.app.get('host') + '/files/' + files.data[0].assignedName
    }
  };
}