// Initializes the `business` service on path `/business`
const {
  Business: Service
} = require('./business.class');
const createModel = require('../../models/business.model');
const hooks = require('./business.hooks');

const docs = require('./business.service.docs');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  const servicePath = '/api/business';
  var serviceObject = new Service(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  service.hooks(hooks);

  service.on('patched', statusChanged);
  service.on('updated', statusChanged);
  service.on('deleted', setUserRole);
};

async function statusChanged(data, context) {

  if (data.status == context.old.status) {
    return;
  }

  if (data.status == 'Approved') {
    await context.app.service('/api/messages').create({
      hUserTo: data.hUser,
      message: `Your business ${data.businessName} has been approved`
    });
  }

  if (data.status == 'Rejected') {
    await context.app.service('/api/messages').create({
      hUserTo: data.hUser,
      message: `Your business ${data.businessName} has been rejected`
    });
  }
  await setUserRole(data, context);

}

async function setUserRole(data, context) {
  var business = await context.service.find({
    query: {
      hUser: data.hUser,
      status: 'Approved'
    }
  });
  await context.app.service('api/users').patch(data.hUser, {
    hasBusiness: business.total > 0 ? 1 : 0
  });
}
