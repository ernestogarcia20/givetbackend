module.exports = {
  description: 'Changing status, creates a message object',
  schemas: {
    business_create: {
      "type": "object",
      "properties": {
        "hCategory": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "businessName": {
          "type": "string",
          "maxLength": 50,
          "nullable": true
        },
        "hUser": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "openHour": {
          "type": "string",
          "maxLength": 5,
          "nullable": true
        },
        "closeHour": {
          "type": "string",
          "maxLength": 5,
          "nullable": true
        },
        "minConsumtionToReward": {
          "type": "number",
          "nullable": true
        },
        "gvtToUser": {
          "type": "number",
        },
        "gvtToSponsor": {
          "type": "number",
        },
        "address": {
          "type": "string",
          "maxLength": 250,
          "nullable": true
        },
        "latitude": {
          "type": "string",
          "maxLength": 20,
          "nullable": true
        },
        "longitude": {
          "type": "string",
          "maxLength": 20,
          "nullable": true
        },
        "status": {
          "type": "string",
          "maxLength": 20,
          "nullable": true,
          "default": "Pending",
          "enum": ["Pending", "Approved", "Rejected"]
        }
      }
    }
  },
  operations: {

    patch: {
      description: `status 'Pending', 'Approved', 'Rejected'. Default = 'Pending'`
    }
  }
}
