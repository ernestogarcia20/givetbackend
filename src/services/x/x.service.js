// Initializes the `X` service on path `/x`
const { X } = require('./x.class');
const hooks = require('./x.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/x', new X(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('x');

  service.hooks(hooks);
};
