// Initializes the `business_scores` service on path `/business_scores`
const {
  BusinessScores: Service
} = require('./business_scores.class');
const createModel = require('../../models/business_scores.model');
const hooks = require('./business_scores.hooks');

const docs = require('./business_scores.service.docs');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  const servicePath = '/api/business_scores';
  var serviceObject = new Service(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  service.hooks(hooks);

  service.on('created', setScore);


};

async function setScore(data, context) {
  var business=await context.app.service('/api/business').get(data.businessId);
  business.ratingCount++;
  business.ratingSum+=data.rating;
  business.rating=business.ratingSum/business.ratingCount;
  business.range = Math.floor(Math.min(business.ratingCount / 5, 5));

  await context.app.service('api/business').patch(business.id,{
    ratingCount:business.ratingCount,
    ratingSum:business.ratingSum,
    rating:business.rating,
    range:business.range
  });

  await context.app.service('/api/messages').create({
    hUserTo: business.hUser,
    message: `Customer: ${business.user.userName}, just rated you`
  });
  
}
