module.exports = {
  description: "",
  operations: {
    create: {
      requestBody: {
        required: true,
        content: {
          "application/json": {
            "schema": {
              //$ref: '#/components/schemas/authManagement'
            },

            examples: {
              'rating': {
                summary: "add stars",
                value: {
					 "userId": 1,
					 "businessId": 19,
					 "rating": 5
                }
              }
            }
          }
        }
      }
	},
	update:false,
	patch:false
  }
};
