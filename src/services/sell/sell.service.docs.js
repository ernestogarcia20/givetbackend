module.exports = {
  description: 'sell ticket',
  model: 'sell_ticket_response',
  schemas: {
    sell_ticket_create: {
      "type": "object",
      "properties": {
        "hUser": {
          "type": "integer",
          "format": "int32",
          "nullable": false
        },
        "amount": {
          "type": "number",
          "nullable": false
        },
        "password2": {
          "type": "string",
          "maxLength": 100
        },
      }
    },
    sell_ticket_response: {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int32"
        },
        "hUser": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "amount": {
          "type": "number",
          "nullable": true
        },
        "balance": {
          "type": "number",
          "nullable": true
        },
        "status": {
          "type": "string",
          "maxLength": 20,
          "nullable": true
        },
        "createdAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        },
        "updatedAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        },
        "finishAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        }
      },
    }
  },
  operations: {
    update: false,
    patch: false,
  }
}
