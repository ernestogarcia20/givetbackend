// Initializes the `get_support` service on path `/get_support`
const {
  GetSupport: Service
} = require('./get_support.class');
const createModel = require('../../models/sell_tickets.model');
const hooks = require('./sell.hooks');
const docs = require('./sell.service.docs');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  const servicePath = '/api/sell_ticket';
  var serviceObject = new Service(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  service.hooks(hooks);

  //back compat
  app.use('/api/get_support', service);
};
