const {
  BadRequest
} = require('@feathersjs/errors');

module.exports = async function (context) {
  var data = await context.service.get(context.id);
  if (data.status != 'Pending') {
    throw new BadRequest("Can't delete ticket");
  }

  // reversar billetera
  var walletService = context.app.service('api/wallet');
  var wallet = (await walletService.find({
    query: {
      hRef: data.id
    }
  })).data[0];

  await walletService.create({
    ...wallet,
    op: '+',
    reason: 'Ticket Annulled',
    id: null
  });


  return context;
};
