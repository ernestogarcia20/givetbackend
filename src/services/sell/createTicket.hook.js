const {
  NotAuthenticated,
  NotAcceptable
} = require("@feathersjs/errors");

module.exports = {
  async before(context) {
    var data = context.data;

    if (context.params.user.password2 != data.password2) {
      throw new NotAuthenticated("invalid password");
    }

    if (data.amount <= 0) {
      throw new NotAcceptable('Invalid amount')
    }

    data.balance = data.amount;
    data.status = "Pending";

    // must no have unfinalized tickets
    var unfinalizedTickets = await context.service.find({
      query: {
        hUser: context.params.user.id,
        status: {
          $nin: ['Finished', 'Cancelled']
        }
      }
    });
    if (unfinalizedTickets.total > 0) {
      throw new NotAcceptable('You have a pending sale ticket, you can cancel the previous one or wait for that ticket to be finalized')
    }

    // available
    var balance = await context.app.service('api/users/:idUser/balance').getBalance(data.hUser);
    if (balance.available < data.amount) {
      throw new NotAcceptable(
        `Balance not Available ${balance.available}`
      );
    }

    // minSell
    var minSell = await context.app.service("api/settings").getValue("minSell");
    if (data.amount < minSell) {
      throw new NotAcceptable(
        `Amount must be equal or greather than minSell ${minSell}`
      );
    }

    // requiredChildren
    var requiredChildren = await context.app
      .service("api/settings")
      .getValue("requiredChildren");

    var sells = (
      await context.app.service("api/sell_ticket").find({
        query: {
          hUser: data.hUser
        }
      })
    ).total;

    var sequelizeClient = context.app.get("sequelizeClient");
    var rs = await sequelizeClient.query(
      `call countChildrenWithPackage(:hUser);`, {
        replacements: {
          hUser: data.hUser
        }
        //raw: true,
        //type: sequelizeClient.QueryTypes.SELECT
      }
    );
    var nChildren = rs[0].n;
    var ciclos = Math.floor(sells / 10);

    if (nChildren < requiredChildren * ciclos) {
      throw new NotAcceptable(
        `Need more referrals with package to create a new ticket`
      );
    }

    return context;
  },
  async after(context) {
    var data = context.result;
    var wallet = await context.app.api.service("wallet").create({
      hUser: data.hUser,
      hRef: data.id,
      op: "-",
      reason: "SELL",
      description: "SELL",
      ticketAmount: data.amount,
      amount: data.amount,
      hTicket: data.id
    });
    return context;
  }
};
