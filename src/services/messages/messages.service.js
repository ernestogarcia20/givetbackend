// Initializes the `business` service on path `/business`
const {
  Business: Service
} = require('./messages.class');
const createModel = require('../../models/messages.model');
const hooks = require('./messages.hooks');

const docs = require('./messages.service.docs');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  const servicePath = '/api/messages';
  var serviceObject = new Service(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  service.hooks(hooks);

};
