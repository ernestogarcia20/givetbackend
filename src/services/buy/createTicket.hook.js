const {
  NotAuthenticated,
  NotAcceptable
} = require("@feathersjs/errors");


module.exports = async function (context) {
  var data = context.data;
  if (!data.hPack) {
    throw new NotAcceptable('No pack');
  }

  var pack = await context.app.service("api/packs").get(data.hPack);
  var lastBuy = await context.app.service("api/users/:idUser/last-buy").find({
    route: {
      idUser: context.params.user.id
    }
  });
  if (lastBuy.id) {

    // previous unfinished
    if (['Finished', 'Cancelled'].indexOf(lastBuy.status) == -1) {
      throw new NotAcceptable('You have a pending ticket, you can cancel the previous one or wait for that ticket to be finalized')
    }

    // package amount
    if (pack.price < lastBuy.amount) {
      throw new NotAcceptable('Sorry, your package must be equal to or greater than the previous one.')
    }

    // wallet no freed
    var transactions = await context.app.service('api/wallet').find({
      query: {
        hUser: lastBuy.hUser,
        status: 'No Freed'
      }
    });
    if (transactions.total > 0) {
      throw new NotAcceptable('you have an active purchase, please wait for it to last 7 days or refer your friends to streamline the process');
    }

    // requiere anuncio
    var anuncios = await context.app.service('api/announcements').find({
      query: {
        hUser: lastBuy.hUser,
        createdAt: {
          $gte: lastBuy.createdAt
        },
        status: 'Approved'
      }
    });
    if (anuncios.total == 0) {
      throw new NotAcceptable('you need to make an announcement, please do it right now');
    }

  }


  data.status = "Pending";
  data.amount = pack.price;
  data.balance = pack.price;

  return context;
};
