module.exports = {
  description: 'buy ticket',
  model: 'buy_ticket_response',
  schemas: {
    buy_ticket_create: {
      "type": 'object',
      "properties": {
        "hUser": {
          "type": "integer",
          "format": "int32"
        },
        "hPack": {
          "type": "integer",
          "format": "int32"
        },
        "receiveEfective": {
          "type": "boolean",
        },
        "receiveToken": {
          "type": "boolean"
        }

      }
    },
    buy_ticket_response: {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int32"
        },
        "hUser": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "amount": {
          "type": "number",
          "nullable": true
        },
        "balance": {
          "type": "number",
          "nullable": true
        },
        "status": {
          "type": "string",
          "maxLength": 20,
          "nullable": true
        },
        "gain": {
          "type": "integer",
          "format": "int32",
          "nullable": true,
          "default": "0"
        },
        "createdAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        },
        "updatedAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        },
        "finishAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        },
        "hPack": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        }
      },
    }
  },
  operations: {

    update: false,
    patch: false,
  }
}
