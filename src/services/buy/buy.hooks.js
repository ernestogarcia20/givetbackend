const {
  authenticate
} = require("@feathersjs/authentication").hooks;

const {
  disallow
} = require("feathers-hooks-common");

const createTicket = require("./createTicket.hook");
const deleteTicket = require('./deleteTicket.hook');
const checkUnlockUser = require('../users/check-unlocked.hook');

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [checkUnlockUser, createTicket],
    update: [disallow("external")],
    patch: [],
    remove: [deleteTicket]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [
      function (context) {
        console.log("after", context);
        return context;
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
