const {
  GiveSupport: Service
} = require('./give_support.class');
const createModel = require('../../models/buy_tickets.model');
const hooks = require('./buy.hooks');

const docs = require('./buy.service.docs');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  const servicePath = '/api/buy_ticket';
  var serviceObject = new Service(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  //back compatible
  app.use('api/give_support', service);

  service.hooks(hooks);
};
