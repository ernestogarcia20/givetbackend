const {
  BadRequest
} = require('@feathersjs/errors');

module.exports = async function (context) {
  var data = await context.service.get(context.id);
  if (data.status != 'Pending') {
    throw new BadRequest("Can't delete ticket");
  }
/*
  context.service.patch(context.id, {
    status: 'Annulled'
  });
*/
  return context;
};
