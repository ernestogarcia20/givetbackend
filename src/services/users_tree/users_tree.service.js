// Initializes the `users_tree` service on path `/api/users_tree`
const {
  UsersTree
} = require('./users_tree.class');
const createModel = require('../../models/users_tree.model');
const hooks = require('./users_tree.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.api.use('/users_tree', new UsersTree(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.api.service('users_tree');

  service.hooks(hooks);
};
