module.exports = async function (context) {
  var action = context.data.$action;
  if (action != 'sendMessage') {
    return context;
  }
  delete context.data.$action;

  var sequelize = context.app.get('sequelizeClient');

  var giveget = await context.service.get(context.id);
  var give = await context.app.api.service('give_support').get(giveget.hGive);
  var get = await context.app.api.service('get_support').get(giveget.hGet);

  var chat = await context.app.api.service('chat').create({
    fromId: context.params.user.id,
    toId: context.params.user.id == give.hUser ? give.hUser : get.hUser,
    conversationId: giveget.hChat,
    message: context.data.message,
  });

}
