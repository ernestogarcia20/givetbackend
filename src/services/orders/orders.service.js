// Initializes the `give_get` service on path `/give-get`
const {
  GiveGet: Service
} = require('./give_get.class');

const createModel = require('../../models/orders.model');
const hooks = require('./give_get.hooks');


const docs = require('./orders.service.docs');
const servicePath = '/api/orders';

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  var serviceObject = new Service(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  service.hooks(hooks);

  //
  const sendPayment = require('./send-payment');
  app.configure(sendPayment);
  //
  const confirmPayment = require('./confirm-payment');
  app.configure(confirmPayment);
};
