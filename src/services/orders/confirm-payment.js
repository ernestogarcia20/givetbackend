const {
  NotAcceptable,
  NotImplemented
} = require("@feathersjs/errors");

const docs = require("./confirm-payment.docs");

module.exports = function (app) {
  const servicePath = "api/orders/:orderId/confirm-payment";

  const sequelize = app.get("sequelizeClient");
  var service = {
    docs,
    options: {},

    async create(data, params) {
      var order = await app.service("api/orders").get(params.route.orderId);
      if (order.status == "Confirmed") {
        return order;
      }

      var buy = await app.service("api/buy_ticket").get(order.hBuy);
      var sell = await app.service("api/sell_ticket").get(order.hSell);

      //se actualiza estatus de tickets
      buy.approvals++;
      if (buy.status == "Completed" || buy.status == "PreFinished") {
        if (buy.transactions == buy.approvals + buy.revocations) {
          buy.status = "Finished";

          buy.finishAt = new Date();

          await addToWallet(buy);
          await addCommissions(buy);
        }
      }
      await app.service("api/buy_ticket").update(buy.id, buy);

      sell.approvals++;
      if (sell.status == "Completed" || sell.status == "PreFinished") {
        if (sell.transactions == sell.approvals + sell.revocations) {
          sell.status = "Finished";

          sell.finishAt = new Date();
        }
      }
      await app.service("api/sell_ticket").update(sell.id, sell);

      //se actualiza la orden
      order.status = "Confirmed";
      order.voucher_sentAt = new Date();
      order.hash = data.hash;
      //
      if (!order.mustConfirmAt) {
        var confirmationTime = await app
          .service("/api/settings")
          .getValue("confirmationTime");
        var TIME_TO_CLOSE = confirmationTime * 60 * 60 * 1000;
        order.mustConfirmAt = new Date(Date.now() + TIME_TO_CLOSE);
      }

      await confirmSELL(sell.id, order.amount);

      return app.service("api/orders").update(order.id, order);
    }
  };

  app.use(servicePath, service);

  app.service(servicePath).hooks({});

  //
  async function addToWallet(buy) {
    var wallet = app.service("api/wallet");

    var madurationTime = await app
      .service("/api/settings")
      .getValue("madurationTime");

    var userBalance = await app.service('api/users/:idUser/balance').getBalance(buy.hUser);

    //se agrega a la billetera
    return await wallet.create({
      hUser: buy.hUser,
      hRef: buy.id,
      op: "+",
      reason: "BUY",
      description: "BUY",
      ticketAmount: buy.amount,
      amount: buy.amount * (1 + userBalance.currentPercentage / 100),

      freedAt: new Date(Date.now() + madurationTime * 24 * 60 * 60 * 1000),
      status: "No Freed"
    });
  }

  async function addCommissions(give) {
    var gainLevels = await app.service("api/profit_level").find({
      query: {
        $sort: {
          level: 1
        }
      },
      paginate: false
    });

    var walletService = app.service("api/wallet");
    var userTreeService = app.service("api/users_tree");


    var giveUser = await app.api.service("users").get(give.hUser);
    var hUser = give.hUser;
    for (var i = 0; i < gainLevels.length; i++) {
      var user = await userTreeService.get(hUser);
      if (!user.hParent) {
        return;
      }
      var parent = await userTreeService.get(user.hParent);
      if (parent.children < gainLevels[i].directRequired) {
        return;
      }

      hUser = parent.id;

      var commission = (give.amount * (gainLevels[i].percent / 100)).toFixed(8);
      try {
        var wallet = await walletService.create({
          hUser: parent.id,
          op: "+",
          reason: "Commission",
          amount: commission,
          ticketAmount: commission,
          freedAt: new Date(),
          status: "No Available",
          description: giveUser.userName
        });
      } catch (err) {
        console.log(err);
      }
      console.log(wallet);
    }
    return;
  }

  async function confirmSELL(hSell, amount) {
    var walletService = app.service('api/wallet');
    var rs = await walletService.find({
      query: {
        hTicket: hSell
      },
      paginate: false
    });
    var wallet = rs[0];
    await walletService.patch(wallet.id, {
      balance: wallet.balance - amount
    });
  }
};
