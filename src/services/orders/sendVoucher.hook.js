module.exports = async function (context) {

  var action = context.data.$action;
  if (action != 'sendVoucher') {
    return context;
  }
  delete context.data.$action;
  var timeExtension = context.data.extension;

  var sequelize = context.app.get('sequelizeClient');

  var giveget = await context.service.get(context.id);
  var give = await context.app.api.service('give_support').get(giveget.hGive);
  var get = await context.app.api.service('get_support').get(giveget.hGet);


  if (giveget.status == "Confirmed") {
    return;
  }

  var chat = {
    fromId: give.hUser,
    toId: get.hUser,
    conversationId: giveget.hChat,
  };

  if (timeExtension) {
    chat.message = 'Time Request ' + timeExtension + 'h';
    chat = await context.app.api.service('chat').create(chat);

    var dt = giveget.mustCloseAt.getTime();
    var extension = context.data.extension * 60 * 60 * 1000;
    giveget.mustCloseAt = new Date(dt + extension);

  } else {
    chat.message = 'Voucher enviado';
    chat = await context.app.api.service('chat').create(chat);

    if (give.status == "Completed") {
      await context.app.api.service('give_support').patch(giveget.hGive, {
        status: 'PreFinished'
      });
    }

    if (get.status == "Completed") {
      await context.app.api.service('get_support').patch(giveget.hGet, {
        status: 'PreFinished'
      });
    }

    //se actualiza la orden
    context.data.status = 'Sending';
    context.data.voucher_sentAt = chat.createdAt;
    //
    var current = await context.service.get(context.id);
    if (!current.mustConfirmAt) {
      var config = await context.app.api.service('config').get(1);
      var TIME_TO_CLOSE = config.timeToConfirmOrder * 60 * 60 * 1000;
      context.data.mustConfirmAt = new Date(Date.now() + TIME_TO_CLOSE);
    }

  }
  context.data.hChat = giveget.hChat || chat.id;
  if (context.data.rating) {
    context.data.getRating = context.data.rating;
  }
}
