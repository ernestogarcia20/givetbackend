const {
  authenticate
} = require('@feathersjs/authentication').hooks;

const givegetView = require('./view.hook');
const sendVoucher = require('./sendVoucher.hook');
const verifyVoucher = require('./verifyVoucher.hook');
const sendMessage = require('./sendMessage.hook');

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [givegetView],
    get: [],
    create: [],
    update: [],
    patch: [sendVoucher, verifyVoucher, sendMessage],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
