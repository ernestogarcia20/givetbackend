module.exports = {
  description: 'confirm payment',
  model: '',
  operations: {
    create: {
      parameters: [{
        in: 'path',
        name: 'orderId',
        style: 'form',
        schema: {
          type: 'number'
        },
        required: true,
        description: 'order id'
      }],
      "requestBody": {
        "required": true,
        "content": {
          "application/json": {
            "schema": {
              type: "object",
              properties: {
                hash: {
                  type: 'string'
                }
              }
            },
            examples: {
              'hash confirmado': {
                value: {
                  hash: "ff370a5f105c9e6f864ef5c46a6dc777c1d284aca7fbef5ceb231e7831edd3c8"
                }
              },
              'hash inválido o no confirmado': {
                value: {
                  hash: "gf370a5f105c9e6f864ef5c46a6dc777c1d284aca7fbef5ceb231e7831edd3c8"
                }
              }
            }
          }
        }
      },
      'responses.201': {
        "content": {
          "application/json": {
            schema: {
              "$ref": "#/components/schemas/orders_response"
            }
          }
        }
      }
    }
  }
}
