module.exports = async function (context) {
  if (!context.params.query) {
    return;
  }
  var sequelize = context.app.get('sequelizeClient');
  var $view = context.params.query.$view;
  if (!$view) {
    return;
  }
  delete context.params.query.$view;

  var data = await sequelize.query(`call findOrders(:hUser);`, {
    replacements: {
      hUser: $view.hUser
    }
  });

  var ordered = data.sort(function (a, b) {
    return (a.createdAt - b.createdAt) * -1; //descending
  });

  context.result = {
    data: ordered
  };

}
