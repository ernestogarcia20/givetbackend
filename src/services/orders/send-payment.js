const {
  NotAcceptable,
  NotImplemented
} = require('@feathersjs/errors');

const docs = require('./send-payment.docs');

module.exports = function (app) {

  const servicePath = 'api/orders/:orderId/send-payment';

  const sequelize = app.get('sequelizeClient');
  var service = {
    docs,
    options: {},

    async create(data, params) {
      var order = await app.service('api/orders').get(params.route.orderId);
      if (order.status == "Confirmed") {
        return order;
      }

      var buy = await app.service('api/buy_ticket').get(order.hBuy);
      var sell = await app.service('api/sell_ticket').get(order.hSell);

      var buyUser = await app.service('api/users').get(buy.hUser);
      var sellUser = await app.service('api/users').get(sell.hUser);

      if (data.hash) {
        await app.service('api/ethereum').verifyPayment(data.hash, buyUser, sellUser, order.amount);
      }

      if (buy.status == "Completed") {
        await app.service('api/buy_ticket').patch(order.hBuy, {
          status: 'PreFinished'
        });
      }

      if (sell.status == "Completed") {
        await app.service('api/sell_ticket').patch(order.hSell, {
          status: 'PreFinished'
        });
      }

      //se actualiza la orden
      order.status = 'Sending';
      order.voucher_sentAt = new Date();
      order.hash = data.hash;
      //
      if (!order.mustConfirmAt) {
        var confirmationTime = await app.service('/api/settings').getValue('confirmationTime');
        var TIME_TO_CLOSE = confirmationTime * 60 * 60 * 1000;
        order.mustConfirmAt = new Date(Date.now() + TIME_TO_CLOSE);
      }

      return app.service('api/orders').update(order.id, order);
    },

  };

  app.use(servicePath, service);

  app.service(servicePath).hooks({});

}
