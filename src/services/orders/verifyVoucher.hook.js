let context;
module.exports = async function (_context) {
  context = _context;
  var action = context.data.$action;
  if (action != 'verifyVoucher') {
    return context;
  }
  delete context.data.$action;

  var sequelize = context.app.get('sequelizeClient');

  var giveget = await context.service.get(context.id);
  var give = await context.app.api.service('give_support').get(giveget.hGive);
  var get = await context.app.api.service('get_support').get(giveget.hGet);


  if (giveget.status == "Confirmed") {
    return;
  }

  var chat = await context.app.api.service('chat').create({
    fromId: get.hUser,
    toId: give.hUser,
    conversationId: giveget.hChat,
    message: 'Voucher recibido',
  });

  give.approvals++;
  //se actualiza el status de los tickets
  if (give.status == "Completed" || give.status == "PreFinished") {
    if (give.transactions == (give.approvals + give.revocations)) {
      give.status = "Finished";

      give.finishAt = new Date();

      await addToGiveBalance(give);
      await addCommissions(give);
    }
  }
  await context.app.api.service('give_support').update(giveget.hGive, give);

  get.approvals++;
  if (get.status == "Completed" || get.status == "PreFinished") {
    if (get.transactions == (get.approvals + get.revocations)) {
      get.status = "Finished";

      get.finishAt = new Date();
    }
  }
  await context.app.api.service('get_support').update(giveget.hGet, get);

  //se actualiza la orden
  context.data.hChat = giveget.hChat || chat.id;
  context.data.status = 'Confirmed';
  context.data.voucher_sentAt = chat.createdAt;

  if (context.data.rating) {
    context.data.giveRating = context.data.rating;
  }



  await confirmSELL(get.id, giveget.amount);

}

async function confirmSELL(hGet, amount) {
  var walletService = context.app.api.service('wallet');
  var rs = await walletService.find({
    query: {
      hTicket: hGet
    },
    paginate: false
  });
  var wallet = rs[0];
  await walletService.patch(wallet.id, {
    available: wallet.available - amount
  });
}

async function addToGiveBalance(give) {
  var wallet = context.app.api.service('wallet');

  var config = await context.app.api.service('config').get(1);

  var retiros = await wallet.find({
    query: {
      hUser: give.id,
      op: '-'
    },
    paginate: false
  }).length;
  var percent = config.gainPercent;
  if (retiros > 6) {
    percent = config.gainPercentAfter;
  }

  //se agrega a la billetera
  return await wallet.create({
    hUser: give.hUser,
    hRef: give.id,
    op: '+',
    reason: 'BUY',
    description: 'BUY',
    ticketAmount: give.amount,
    amount: give.amount * (1 + percent / 100),

    freedAt: new Date(Date.now() + (config.expirationDays * 24 * 60 * 60 * 1000)),
    status: 'No Freed'
  });
}


async function addCommissions(give) {

  var gainLevels = await context.app.api.service('profit_level').find({
    query: {
      $sort: {
        level: 1
      },
    },
    paginate: false
  });

  var walletService = context.app.api.service('wallet');
  var userTreeService = context.app.api.service('users_tree');

  var hUser = give.hUser;
  var giveUser = await context.app.api.service('users').get(give.hUser);
  for (var i = 0; i < gainLevels.length; i++) {
    var user = await userTreeService.get(hUser);
    if (!user.hParent) {
      return;
    }
    var parent = await userTreeService.get(user.hParent);
    if (parent.children < gainLevels[i].directRequired) {
      return;
    }

    hUser = parent.id;

    var commission = give.amount * (gainLevels[i].percent / 100);
    try {
      var wallet = await walletService.create({
        hUser: parent.id,
        op: '+',
        reason: 'Commission',
        amount: commission,
        ticketAmount: commission,
        freedAt: new Date(),
        status: 'No Available',
        description: giveUser.userName
      });
    } catch (err) {
      console.log(err);
    }
    console.log(wallet);
  }
  return;
}
