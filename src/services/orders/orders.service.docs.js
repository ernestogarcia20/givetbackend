module.exports = {
  description: 'orders',
  model: 'orders_response',
  schemas: {
    orders_response: {
      type: 'object',
      properties: {
        "id": {
          "type": "integer",
          "format": "int32"
        },
        "hBuy": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "hSell": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "amount": {
          "type": "number",
          "nullable": true
        },
        "status": {
          "type": "string",
          "maxLength": 20,
          "nullable": true
        },
        "hash": {
          "type": "string",
          "maxLength": 100,
          "nullable": true
        },
        "voucher_image": {
          "type": "string",
          "maxLength": 50,
          "nullable": true
        },
        "voucher_sentAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        },
        "voucher_receivedAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        },
        "mustCloseAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        },
        "createdAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        },
        "updatedAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        },
        "buyRating": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "sellRating": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "mustConfirmAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        }
      }

    }
  },
  operations: {
    create: false,
    update: false,
    patch: false,
    remove: false
  }
}
