// Initializes the `emparejador` service on path `/emparejador`
const MILLIS_TO_ANULL = 0 // 30 * 1000; // 30 segundos
const TIMER = 30 * 1000; // 1/2 minuto

var running = false;

module.exports = function (app) {
  var emparejador = new Emparejador(app);

  return emparejador;
}

function Emparejador(app) {
  this.emparejar = emparejar;

  const log = console.log.bind(console);

  const userService = app.service("api/users");
  const buyService = app.service("api/buy_ticket");
  const sellService = app.service("api/sell_ticket");
  const orderService = app.service("api/orders");
  const chatService = app.service("api/chat");
  const walletService = app.service("api/wallet");
  const settingsService = app.service('api/settings');


  return {
    emparejar: this.emparejar
  }


  async function emparejar() {
    console.log("emparejando", new Date().toString());
    running = true;

    await cancelOrdersAndLockUsers();

    var orders = await generateOrders();

    await freedBalances();

    //await doAvailableBalances();

    running = false;
    return {
      orders
    };
    //
  }

  //tickets para completar
  async function sellbuys() {
    return await buyService.find({
      paginate: false,
      query: {
        balance: {
          $gt: 0
        },
        //procesa los tickets después de SECONDS_TO_ANULL puestos
        createdAt: {
          $lt: new Date(Date.now() - MILLIS_TO_ANULL)
        },
        status: {
          $nin: ["Annulled", "Cancelled"]
        }
      }
    });
  }

  // ordenes de los demás usuarios para ser completadas
  async function getSellTicketsFromOthers(hUser) {
    return sellService.find({
      paginate: false,
      query: {
        balance: {
          $gt: 0
        },
        //procesa las ordenes después de SECONDS_TO_ANULL puestas
        createdAt: {
          $lt: new Date(Date.now() - MILLIS_TO_ANULL)
        },
        //que no sean del mismo usuario del buy
        hUser: {
          $ne: hUser
        },
        status: {
          $nin: ["Annulled", "Cancelled"]
        }
      }
    });
  }

  //genera las órdenes y crea root en chat
  //Para las tickets buy busca tickets sell y crea orden
  async function generateOrders() {
    var orders = [];

    var confirmationTime = await settingsService.getValue('confirmationTime');

    var TIME_TO_CLOSE = confirmationTime * 60 * 60 * 1000;

    var buys = await sellbuys();
    for (var i = 0; i < buys.length; i++) {
      var buy = buys[i];
      var sells = await getSellTicketsFromOthers(buy.hUser);

      for (var j = 0; j < sells.length; j++) {
        var sell = sells[j];

        var parAmount = 0;
        if (buy.balance > sell.balance) {
          parAmount = sell.balance;

          buy.balance = buy.balance - parAmount;
          buy.status = "PreCompleted";

          sell.balance = 0;
          sell.status = "Completed";
        } else if (buy.balance < sell.balance) {
          parAmount = buy.balance;

          buy.balance = 0;
          buy.status = "Completed";

          sell.balance = sell.balance - parAmount;
          sell.status = "PreCompleted";
        } else {
          parAmount = sell.balance;

          buy.balance = 0;
          buy.status = "Completed";

          sell.balance = 0;
          sell.status = "Completed";
        }

        // Actualiza estados y nro de transacciones
        buy.transactions++;
        sell.transactions++;

        buy = await buyService.update(buy.id, buy);
        sell = await sellService.update(sell.id, sell);

        // Crea una entrada en el chat como root de la conversación compra-venta
        /*
        var chat = await chatService.create({
          fromId: buy.hUser,
          toId: sell.hUser
        });
*/
        // Crea la compra-venta
        var par = await orderService.create({
          hBuy: buy.id,
          hSell: sell.id,
          // hChat: chat.id,
          status: "Pending",
          amount: parAmount,
          mustCloseAt: new Date(Date.now() + TIME_TO_CLOSE)
        });

        /*
        await chatService.patch(par.hChat, {
          message: "Conversation for " + par.id
        });
        */

        orders.push(par);

      }
    };

    return orders;
  }


  async function cancelOrdersAndLockUsers() {
    //
    // actualización de estado de los pares a "cancelled" si vence el tiempo
    // 
    var paresPendientes = await orderService.find({
      paginate: false,
      query: {
        status: ["Pending", "Sending"]
      }
    });
    var now = new Date();
    for (var i = 0; i < paresPendientes.length; i++) {
      var item = paresPendientes[i];
      if (item.status == "Pending") {
        var diff = new Date(item.mustCloseAt) - now;
        if (diff < 0) {
          await cancelBuyTicket(item.hBuy);
        }
      } else {
        if (item.status == "Sending") {
          var diff = new Date(item.mustConfirmAt) - now;
          if (diff < 0) {
            var sellTicket = await sellService.get(item.hSell);
            await lockUser(sellTicket.hUser);
          }
        }
      }
    }
  }

  async function cancelBuyTicket(hBuy) {
    var buy = await buyService.get(hBuy);
    if (buy.status == "Cancelled") {
      return;
    }

    await lockUser(buy.hUser);

    // todos las ordenes de esta compra
    var orders = await orderService.find({
      paginate: false,
      query: {
        hBuy: hBuy,
        status: ["Pending"]
      }
    });

    for (var i = 0; i < orders.length; i++) {
      var order = orders[i];

      orderService.patch(order.id, {
        status: 'Cancelled'
      });

      //reversa status en ticket de venta
      var sell = await sellService.get(order.hSell);
      sell.revocations++;
      sell.balance += order.amount;
      if (sell.balance == sell.amount) {
        sell.status = "Pending";
      } else {
        sell.status = "PreCompleted"
      }
      await sellService.update(sell.id, sell);
      //

    }

    // cancela ticket de compra
    buy = await buyService.patch(buy.id, {
      revocations: buy.revocations + orders.length,
      status: 'Cancelled'
    });
  }

  async function lockUser(hUser) {
    await app.service('api/users/:idUser/lock-user').create({}, {
      route: {
        idUser: hUser
      }
    });
    // anula (elimina tickets de venta Pendientes)
    var sellTickets = await sellService.find({
      query: {
        hUser: hUser,
        status: 'Pending'
      }
    });
    for (var i = 0; i < sellTickets.total; i++) {
      var ticket = sellTickets.data[i];
      await sellService.remove(ticket.id);
    }
  }

  // convierte en freed los no-freed pasados los 10 días
  async function freedBalances() {
    try {
      var rsWallet = await walletService.find({
        query: {
          freedAt: {
            $lt: new Date()
          },
          status: 'No Freed'
        },
        paginate: false
      });
    } catch (err) {
      console.log(err);
    }
    for (var i = 0; i < rsWallet.length; i++) {
      var wallet = rsWallet[i];
      await walletService.patch(wallet.id, {
        status: 'Freed'
      });
    }
  }
  /*
  //return await emparejar();
  (async function f() {
    const TIMER = 30000; //30 segundos
    console.log("emparejando", new Date().toString());

    await emparejar();

    //console.log("OJO: Habilitar emparejador");
    setTimeout(f, TIMER);
  })();
  */
}
