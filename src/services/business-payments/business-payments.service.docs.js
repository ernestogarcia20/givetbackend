module.exports = {
  description: 'create payment',
  operations: {
    create: {
      "requestBody": {
        "required": true,
        "content": {
          "application/json": {
            "schema": {},
            examples: {
              'create': {
                value: {
                  "userId": 2,
                  "businessId": 53,
                  "amount": 5,
                  "cryptoAddress": "xxx"
                }
              }
            }
          }
        }
      },
    }
  }
}
