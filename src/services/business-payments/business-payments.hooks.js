const {
  authenticate
} = require('@feathersjs/authentication').hooks;


module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [join],
    get: [join],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};

function join(context) {

  const sequelize = context.app.get('sequelizeClient');

  context.params.sequelize.include = [{
    model: sequelize.models.business,
    attributes: sequelize.models.business.include_attributes
  }, {
    model: sequelize.models.users,
    attributes: sequelize.models.users.include_attributes
  }]

  return context;
}
