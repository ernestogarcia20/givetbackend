module.exports = {
  description: 'confirm payment',
  model: '',
  operations: {
    create: {
      summary: 'Confirm or Reject a payment',
      parameters: [{
        in: 'path',
        name: 'id',
        style: 'form',
        schema: {
          type: 'number'
        },
        required: true,
        description: 'business payment id'
      }],
      "requestBody": {
        "required": true,
        "content": {
          "application/json": {
            "schema": {
              type: "object",
              properties: {
                action: {
                  type: 'string'
                }
              }
            },
            examples: {
              'confirm': {
                value: {
                  action: 'Confirm'
                }
              },
              'reject': {
                value: {
                  action: "Reject"
                }
              }
            }
          }
        }
      },
    }
  }
}
