const {
  NotAcceptable,
  NotImplemented
} = require("@feathersjs/errors");

const docs = require("./confirm-payment.docs");

module.exports = function (app) {
  const servicePath = "api/business_payments/:id/confirm-payment";

  const sequelize = app.get("sequelizeClient");
  var service = {
    docs,
    options: {},

    async create(data, params) {
      var bpService = app.service("api/business_payments");

      var bp = await bpService.get(params.route.id);

      if (bp.approved == 1) {
        return bp;
      }
      if (data.action == "Reject") {
        bp = await bpService.patch(bp.id, {
          approved: 0
        });
        return bp;
      }

      if (data.action == "Confirm") {
        bp = await bpService.patch(bp.id, {
          approved: 1
        });

        var giftToCommisions = await addToWallet(bp);
        await addCommissions(giftToCommisions);
      }

      return bp;
    }
  };

  app.use(servicePath, service);

  app.service(servicePath).hooks({});

  ////////////
  async function addToWallet(bp) {
    var wallet = app.service("api/wallet");

    var business = await app.service('api/business').get(bp.businessId);
    if (bp.giftAmount > 0) {

      // se resta el gift
      await wallet.create({
        hUser: business.hUser,
        hRef: bp.id,
        op: "-",
        reason: "GIFT",
        description: "Gift from " + business.businessName,
        amount: bp.giftAmount
      });
      // agrega la mitad al usuario
      return await wallet.create({
        hUser: bp.userId,
        hRef: bp.id,
        op: "+",
        reason: "GIFT Commission",
        description: "Gift from " + business.businessName,
        amount: bp.giftAmount * 0.5
      });

    } else {
      return null;
    }

  }

  async function addCommissions(gift) {
    var gainLevels = await app.service("api/profit_level").find({
      query: {
        $sort: {
          level: 1
        }
      },
      paginate: false
    });

    var giftUser = await app.service('api/users').get(gift.hUser);
    var walletService = app.service("api/wallet");
    var userTreeService = app.service("api/users_tree");

    var hUser = gift.hUser;
    for (var i = 0; i < gainLevels.length; i++) {
      var user = await userTreeService.get(hUser);
      if (!user.hParent) {
        return;
      }
      var parent = await userTreeService.get(user.hParent);
      if (parent.children < gainLevels[i].directRequired) {
        return;
      }

      hUser = parent.id;
      if (gainLevels[i].businessPercent > 0) {
        var commission = (gift.amount * (gainLevels[i].businessPercent / 100)).toFixed(8);
        try {
          var wallet = await walletService.create({
            hUser: parent.id,
            hRef: gift.hRef,
            op: "+",
            reason: "GIFT Commission",
            description: gift.description + ': ' + giftUser.userName,
            amount: commission
          });
        } catch (err) {
          console.log(err);
        }
      }
      console.log(wallet);
    }
    return;
  }

};
