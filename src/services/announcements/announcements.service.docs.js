module.exports = {
  description: 'Changing status, creates a message object',
  model: 'announces',
  schemas: {},
  operations: {
    patch: {
      description: `status 'Pending', 'Approved', 'Rejected'. Default = 'Pending'`
    }
  }
}
