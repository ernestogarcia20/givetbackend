const {
  authenticate
} = require('@feathersjs/authentication').hooks;

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [join],
    get: [join],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};

function join(context) {
  const sequelize = context.app.get('sequelizeClient');
  const {
    users
  } = sequelize.models;
  context.params.sequelize.include = [{
    model: users,
    as: 'user',
    attributes: users.include_attributes
  }]

  return context;
}
