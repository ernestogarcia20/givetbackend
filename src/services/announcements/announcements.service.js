// Initializes the `announcements` service on path `/announcements`
const {
  Announcements: Service
} = require('./announcements.class');
const createModel = require('../../models/announcements.model');
const hooks = require('./announcements.hooks');

const docs = require('./announcements.service.docs');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  const servicePath = '/api/announcements';
  var serviceObject = new Service(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  service.hooks(hooks);

  service.on('patched', statusChanged);
  service.on('updated', statusChanged);
};

async function statusChanged(data, context) {

  if (data.status == context.old.status) {
    return;
  }

  if (data.status == 'Approved') {
    await context.app.service('/api/messages').create({
      hUserTo: data.hUser,
      message: `Your Announce ${data.announcementsName} has been approved`
    });
  }

  if (data.status == 'Rejected') {
    await context.app.service('/api/messages').create({
      hUserTo: data.hUser,
      message: `Your Announce ${data.announcementsName} has been rejected`
    });
  }
}
