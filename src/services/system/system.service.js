// Initializes the `system` service on path `/api/system`
const {
  System: Service
} = require('./system.class');
const hooks = require('./system.hooks');
const docs = require('./system.service.docs');

module.exports = function (app) {

  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  const servicePath = '/api/system';
  var serviceObject = new Service(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  //app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  //const service = app.service(servicePath);

  //service.hooks(hooks);

  //
  const balanceService = require('./balance.service');
  app.configure(balanceService);

  //
  const matchmaker = require('./matchmaker.service');
  app.configure(matchmaker);

  //
  const prepareUsers = require('./prepare-users');
  app.configure(prepareUsers);

  //
  const gen10FinishedSell = require('./gen10FinishedSell');
  app.configure(gen10FinishedSell);
};
