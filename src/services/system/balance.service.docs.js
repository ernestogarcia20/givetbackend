module.exports = {
  description: 'system balance',
  model: 'systembalance_response',
  schemas: {
    systembalance_response: {
      "type": "object",
      "properties": {
        "totalBuy": {
          type: "number"
        },
        "totalSell": {
          type: "number"
        },
        "countBuy": {
          type: "number"
        },
        "countSell": {
          type: "number"
        },
        "countDeletedUsers": {
          type: "number"
        },
        "countUsers": {
          type: "number"
        },
        "countLockedUsers": {
          type: "number"
        },
        "countPoorSells": {
          type: "number"
        },
      }
    }
  },
  operations: {
    find: {
      description: 'wallet resume',
      parameters: [],
      'responses.200': {
        "content": {
          "application/json": {
            schema: {
              "$ref": "#/components/schemas/systembalance_response"
            }
          }
        }
      },
    }
  }
}
