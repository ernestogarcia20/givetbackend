const {
  GeneralError
} = require('@feathersjs/errors');

const docs = {
  description: 'Business Logic for buy-sell process',
  operations: {
    create: false,
    update: false,
    delete: false,
    patch: false,
    find: {
      parameters: []
    }
  }
};

module.exports = function (app) {

  const servicePath = '/api/system/matchmaker';

  const sequelize = app.get('sequelizeClient');
  var service = {
    docs,
    options: {},
    async find(params) {
      var emparejador = app.get('emparejador');
      try {
        var result = await emparejador.emparejar();
        return result;
      } catch (err) {
        throw new GeneralError(err, err);
      }

    },

  };

  app.use(servicePath, service);

  app.service(servicePath).hooks({});

}
