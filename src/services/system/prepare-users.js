const {
  GeneralError
} = require('@feathersjs/errors');

const docs = {
  description: 'Clear and prepare an user list with root=test',
  operations: {
    create: false,
    update: false,
    delete: false,
    patch: false,
    find: {
      parameters: []
    }
  }
};

let emparejador, userService;
module.exports = function (app) {

  const servicePath = '/api/system/prepare-users';

  const sequelize = app.get('sequelizeClient');

  emparejador = app.get('emparejador');
  userService = app.service('api/users');

  var service = {
    docs,
    options: {},
    async find(params) {

      try {

        var r = await userService.remove(null, {
          query: {
            emailOk: 1
          }
        });

        await sequelize.query('delete from users where id>1');
        await sequelize.query('ALTER TABLE users AUTO_INCREMENT = 2');

        await sequelize.query('delete from buy_tickets');
        await sequelize.query('delete from sell_tickets');
        await sequelize.query('delete from messages');
        await sequelize.query('delete from wallet');

        await genU('a', null);
        await genU('b', null);
        await genU('c', null);
        await genU('d', null);
        await genU('e', null);

        await genChild('a', 5);
        await genChild('a1', 5);
        await genChild('a11', 5);
        await genChild('a111', 5);
        await genChild('a1111', 5);
        await genChild('a2', 5);

        await app.service('/api/wallet').create({
          "hUser": 2,
          "op": "+",
          "reason": "INITIAL_BALANCE",
          "description": "Initial Balance",
          "amount": 5
        });
        //
        await userService.patch(1, {
          btc: 'aaa'
        });
        await userService.patch(2, {
          btc: '19n2cGWSVK8zco42fxh7iymFUUikXTik2N'
        });
        await userService.patch(3, {
          btc: '1NRBeHuSKdyMFub4RjJAmaHjcLafvuyceg'
        });
        await userService.patch(7, {
          btc: 'xxx'
        });
        await userService.patch(12, {
          btc: 'yyy'
        });
        try {
          await app.service('api/packs').remove(13);
        } catch (err) {
          //
        } finally {
          await app.service('api/packs').create({
            id: 13,
            packName: 'test',
            price: 0.00635355,
            tokens: 1
          })
        }

        var announcement = await app.service('api/announcements').create({
          "hUser": 3,
          "url": "http://test",
          "status": "Approved",
          "hash": "#abc"
        });
        await app.service('api/announcements').patch(announcement.id, {
          createdAt: new Date(2021, 1, 1)
        });
        announcement = await app.service('api/announcements').create({
          "hUser": 7,
          "url": "http://test",
          "status": "Approved",
          "hash": "#abc"
        });
        await app.service('api/announcements').patch(announcement.id, {
          createdAt: new Date(2021, 1, 1)
        });

        announcement = await app.service('api/announcements').create({
          "hUser": 12,
          "url": "http://test",
          "status": "Approved",
          "hash": "#abc"
        });
        await app.service('api/announcements').patch(announcement.id, {
          createdAt: new Date(2021, 1, 1)
        });

        return await userService.find({
          query: {
            emailOk: 1
          }
        });

      } catch (err) {
        throw new GeneralError(err, err);
      }

    },

  };

  app.use(servicePath, service);

  app.service(servicePath).hooks({});

}

async function genChild(parent, n) {
  if (n && n > 0) {
    for (var i = 1; i <= n; i++) {
      await genU(parent + i, parent);
    }
  }
}

async function genU(name, parent) {
  return await userService.create({
    referralUserName: parent || null,
    email: name + '@globalpyme.com',
    userName: name,
    fullName: name,
    countryCode: 'PE',
    phone: name + '-4123',
    password: '123',
    password2: '123',
    emailOk: 1,
  });
}
