const {
  GeneralError
} = require('@feathersjs/errors');

const docs = {
  description: 'gen 9 finished sell',
  operations: {
    create: false,
    update: false,
    delete: false,
    patch: false,
    find: {
      parameters: [{
        in: 'query',
        name: 'idUser',
        style: 'form',
        schema: {
          type: 'string'
        },
        required: true,
        description: 'user id'
      }]
    }
  }
};

let emparejador, userService;
module.exports = function (app) {

  const servicePath = '/api/system/gen10FinishedSell';

  const sequelize = app.get('sequelizeClient');

  var service = {
    docs,
    options: {},
    async find(params) {
      var sellService = app.service('api/sell_ticket');
      var user = await app.service('api/users').get(params.query.idUser);
      try {
        for (var i = 0; i < 10; i++) {
          var ticket = await sellService.create({
            hUser: user.id,
            amount: 0.1,
            reason: 'SELL',
            password2: '123'
          }, {
            user
          });
          await sellService.patch(ticket.id, {
            status: 'Finished'
          });
        }

        return sellService.find({
          query: {
            huser: user.id
          }
        });

      } catch (err) {
        throw new GeneralError(err, err);
      }

    },

  };

  app.use(servicePath, service);

  app.service(servicePath).hooks({});

}

async function genChild(parent, n) {
  if (n && n > 0) {
    for (var i = 1; i <= n; i++) {
      await genU(parent + i, parent);
    }
  }
}

async function genU(name, parent) {
  return await userService.create({
    referralUserName: parent || null,
    email: name + '@globalpyme.com',
    userName: name,
    fullName: name,
    countryCode: 'PE',
    phone: name + '-4123',
    password: '123',
    password2: '123',
    emailOk: 1,
  });
}
