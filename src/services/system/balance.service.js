const docs = require('./balance.service.docs');

module.exports = function (app) {

  const servicePath = '/api/system/balance';

  const sequelize = app.get('sequelizeClient');
  var service = {
    docs,
    options: {},
    async find(params) {
      var data = await sequelize.query(`call systemBalance();`, {
        replacements: {}
      });
      var data = data[0];
      return {
        ...data
      };
    },

  };

  app.use(servicePath, service);

  app.service(servicePath).hooks({});

}
