module.exports = {
  description: '',
  model: 'wallet_response',
  schemas: {
    wallet_create: {
      "type": "object",
      "properties": {
        "hUser": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "op": {
          "type": "string",
          "nullable": true,
          description: 'operation type "+" or "-"'
        },
        "reason": {
          "type": "string",
          "maxLength": 45,
          "nullable": true
        },
        "description": {
          "type": "string",
          "maxLength": 45,
          "nullable": true
        },
        "amount": {
          "type": "number",
          "nullable": true
        }
      }
    },
    wallet_response: {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int32"
        },
        "hUser": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "op": {
          "type": "string",
          "nullable": true
        },
        "reason": {
          "type": "string",
          "maxLength": 45,
          "nullable": true
        },
        "description": {
          "type": "string",
          "maxLength": 45,
          "nullable": true
        },
        "amount": {
          "type": "number",
          "nullable": true
        },
        "balance": {
          "type": "number",
          "nullable": true
        },
        "status": {
          "type": "string",
          "maxLength": 15,
          "nullable": true
        },
        "available": {
          "type": "number",
          "nullable": true,
          "default": "0.00000000"
        },
        "hTicket": {
          "type": "integer",
          "format": "int32",
          "nullable": true
        },
        "createdAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        },
        "updatedAt": {
          "type": "string",
          "format": "date-time",
          "nullable": true
        }
      }
    }
  },
  operations: {
    create: {
      requestBody: {
        required: true,
        content: {
          "application/json": {
            "schema": {
              $ref: '#/components/schemas/wallet_create'
            },
            examples: {
              'BUY test': {
                value: {
                  "hUser": 3,
                  "op": "+",
                  "reason": "BUY",
                  "description": "test",
                  "ticketAmount": 0.1,
                  "amount": 0.125,
                  "freedAt": new Date(Date.now() + 72 * 24 * 60 * 60 * 1000),
                  "status": "No Freed"
                }
              }
            }
          }
        }
      }
    },
    update: false,
    patch: false,
    remove: false
  }
}
