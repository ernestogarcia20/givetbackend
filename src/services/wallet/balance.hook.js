let context;
module.exports = async function (_context) {
  context = _context;
  var action = context.params.query.$view;
  if (action != 'balance') {
    return context;
  }
  delete context.params.query.$view;

  var sequelizeClient = context.app.get('sequelizeClient');

  var rs = await sequelizeClient.query(`
    select
      sum(if(status='Available',available,0)) as available,
      sum(if(status='No Available',balance,0)) as noAvailable,
      sum(if(status='No Freed',balance,0)) as noFreed,
      sum(if(status='Freed',available,0)) as freed,
      sum(if(status='Extracted',amount,0)) as extracted 
    from wallet
    where hUser=:hUser and op='+';
    `, {
    replacements: {
      hUser: context.params.query.hUser
    },
    raw: true,
    type: sequelizeClient.QueryTypes.SELECT
  });

  var balance = rs[0];
  balance.available = balance.available || 0;
  context.result = balance;
  return context;
}
