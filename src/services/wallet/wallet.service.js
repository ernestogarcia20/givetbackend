// Initializes the `wallet` service on path `/api/wallet`
const {
  Wallet: Service
} = require('./wallet.class');
const createModel = require('../../models/wallet.model');
const hooks = require('./wallet.hooks');
const docs = require('./wallet.service.docs');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  const servicePath = '/api/wallet';
  var serviceObject = new Service(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  service.hooks(hooks);

};
