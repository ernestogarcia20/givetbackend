const {
  authenticate
} = require('@feathersjs/authentication').hooks;

const balance = require('./balance.hook');
const create = require('./create.hook');
const patch = require('./patch.hook');

const checkUnlockUser = require('../users/check-unlocked.hook');


module.exports = {
  before: {
    all: [
      //authenticate('jwt')
    ],
    find: [join, balance],
    get: [join],
    create: [checkUnlockUser, create],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};

function join(context) {
  const sequelize = context.app.get('sequelizeClient');
  const {
    users
  } = sequelize.models;

  context.params.sequelize.include = [{
    model: users,
    as: 'user',
    attributes: users.include_attributes
  }]

  return context;
}
