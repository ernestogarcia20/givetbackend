const {
  NotAcceptable,
  NotImplemented
} = require("@feathersjs/errors");

let context;
module.exports = async function (_context) {
  context = _context;
  var data = context.data;

  if (context.params.provider == "rest" && ["BUY", "SELL", "Commission", "GIFT", "GIFTCommission"].indexOf(data.reason) > -1) {
    throw new NotAcceptable(`Invalid reason. ${data.reason} is reserved for system`);
  }
  if (data.amount <= 0) {
    throw new NotAcceptable("Invalid amount");
  }
  if (!data.reason) {
    throw new NotAcceptable("Needs a reason to add or reduce balance");
  }

  if (data.reason == "GIFT") {

    //admite saldo insuficiente

    var balance = await context.app
      .service("api/users/:idUser/balance")
      .getBalance(data.hUser);

    data.balance = 0;
    data.available = 0;

    var resto = await substract(context);
    if (resto > 0) {
      data.status = null;
      data.balance = resto * -1;
      data.available = resto * -1;
    } else {
      data.status = null;
      data.balance = 0;
      data.available = 0;
    }

    return;
  }

  if (data.op == "-") {
    var balance = await context.app
      .service("api/users/:idUser/balance")
      .getBalance(data.hUser);
    if (balance.available < data.amount) {
      throw new NotAcceptable("Insufficient balance");
    }
    data.balance = 0;
    data.available = 0;

    //
    if (data.reason == "SELL") {
      await substract(context);
      return;
    }

    if (data.reason == "WITHDRAW") {
      await substract(context);
      data.status = "No Arrived";
      data.balance = 0;
      data.available = 0;
      return;
    }

    if (data.reason == "UNLOCK") {
      data.amount = await context.app
        .service("api/settings")
        .getValue("paymentForUnlock");

      await substract(context);
      await context.app.service("api/users").patch(data.hUser, {
        locked: false
      });

      data.status = null;
      data.balance = 0;
      data.available = 0;
      return;
    }

    //otro
    await substract(context);
    data.status = null;
    data.balance = 0;
    data.available = 0;
    return;
  } else if (data.op == "+") {
    if (data.reason == "Commission") {
      await addCommission(context);
      return;
    }

    if (data.reason == "BUY") {
      await addBuy(context);
      return;
    }

    //otro
    data.status = "Available";
    data.available = data.amount;
    data.balance = data.amount;
    return;
  } else {
    throw new NotAcceptable('Valid operators are "+" and "-"');
  }
};

// verificada! Resta de availables
async function substract(context) {
  var walletService = context.service;

  var sell = {
    ...context.data
  };

  toSubstract = sell.amount;
  /*
  busca entradas con available>0
  */
  var rs = await walletService.find({
    query: {
      hUser: sell.hUser,
      op: "+",
      //status: ['Available'],
      available: {
        $gt: 0
      },
      $sort: {
        id: 1
      }
    },
    paginate: false
  });
  //para cada fila con disponible se va restando hasta agotar saldo
  for (var i = 0; i < rs.length; i++) {
    var w = rs[i];
    if (w.available <= toSubstract) {
      toSubstract = toSubstract - w.available;
      w.available = 0;
    } else {
      w.available = w.available - toSubstract;
      toSubstract = 0;
    }

    await walletService.patch(w.id, {
      status: w.status,
      balance: w.balance,
      available: w.available
    });

    //se restó todo el monto
    if (toSubstract == 0) {
      break;
    }
  }
  return toSubstract;
}

async function addBuy(context) {
  var buy = context.data;
  buy.status = "No Freed";
  buy.balance = buy.amount;
  buy.available = 0;

  var rs = await context.service.find({
    query: {
      hUser: buy.hUser,
      status: ["No Available", "No Freed", "Freed"],
      balance: {
        $gt: 0
      },
      $sort: {
        id: 1
      }
    },
    paginate: false
  });
  for (var i = 0; i < rs.length; i++) {
    var w = rs[i];
    if (w.reason == "BUY") {
      w.available = w.available + w.balance;
      w.balance = 0;
      w.status = "Available";
    } else {
      if (w.balance <= buy.balance) {
        buy.balance = buy.balance - w.balance;
        w.available = w.available + w.balance;
        w.balance = 0;
        w.status = "Available";
      } else {
        w.balance = w.balance - buy.balance;
        w.available = w.available + buy.balance;
        buy.balance = 0;
        buy.status = "Extracted";
      }
    }

    await context.service.patch(w.id, {
      status: w.status,
      balance: w.balance,
      available: w.available
    });
  }
}

async function addCommission(context) {
  var commission = context.data;
  commission.status = "No Available";
  commission.balance = commission.amount;
  commission.available = 0;
  /*
  busca si hay alguna compra previa con available > 0 para 
  establecer su available
  */
  var rs = await context.service.find({
    query: {
      hUser: commission.hUser,
      reason: "BUY",
      balance: {
        $gt: 0
      },
      $sort: {
        id: 1
      }
    },
    paginate: false
  });
  for (var i = 0; i < rs.length; i++) {
    var buy = rs[i];

    if (commission.balance >= buy.balance) {
      commission.balance = commission.balance - buy.balance;
      buy.balance = 0;
      buy.status = "Extracted";
    } else {
      buy.balance = buy.balance - commission.balance;
      commission.available = commission.available + buy.balance;
      commission.balance = 0;
      commission.status = "Available";
    }

    commission.available = commission.amount - commission.balance;

    await context.service.patch(buy.id, {
      status: buy.status,
      balance: buy.balance
    });
  }
}
