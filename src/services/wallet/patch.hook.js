let context;
module.exports = async function (context) {
  var old = await context.service.get(context.data.id);
  if (old.status == 'No Freed' && context.data.status == 'Freed') {
    context.data.balance = context.data.amount;
  }
  return context;
}
