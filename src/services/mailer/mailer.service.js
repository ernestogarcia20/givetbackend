// Initializes the `mailer` service on path `/mailer`
const hooks = require("./mailer.hooks");
const Mailer = require("feathers-mailer");
const smtpTransport = require("nodemailer-smtp-transport");

const path = require('path');
const nodemailer = require("nodemailer");
const Email = require("email-templates");

const docs = require('./mailer.service.docs');

module.exports = function (app) {
  /*
    app.use('/api/mailer', Mailer(smtpTransport({
      host: 'grupospyme.com',
      secure: true,
      auth: {
        user: '',
        pass: ''
      }
    })));*/
  const transporter = nodemailer.createTransport(
    smtpTransport(app.get("notifier").smtpTransport),
    app.get("notifier").defaults);


  const servicePath = "/api/mailer";
  var serviceObject = {
    docs,
    options: {},
    async create(data, params) {
      if (data.template) {
        try {

          const email = new Email({
            transport: transporter,
            send: true,
            preview: {
              open: {
                app: 'google-chrome',
                wait: false
              }
            },
            views: {
              root: path.resolve('public/emails'),
              options: {
                extension: "ejs"
              }
            },
            juice: true,
            juiceResources: {
              preserveImportant: true,
              webResources: {
                //relativeTo: path.resolve('public/emails/', data.template),
                //relativeTo: path.resolve('public/emails/common/'),
                //image: false
              }
            }
          });

          var ret = await email.send({
            template: path.resolve('public/emails/', data.template),
            message: {
              from: data.from,
              to: data.to
            },
            locals: data
          });

          return ret;
        } catch (err) {
          var x = err;
          return x;
        }
      } else {
        return transporter.sendMail(data);
      }
    }
  };
  /* 
  // con Mailer
    var serviceObject = Mailer(smtpTransport(app.get('notifier').smtpTransport));
    serviceObject.docs = docs;
  */
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  service.hooks(hooks);
};
