module.exports = {
  description: "send email from system. Only for use from server",
  schemas: {
    mail_create: {
      type: "object",
      properties: {
        from: {
          type: "string"
        },
        to: {
          type: "string"
        },
        subject: {
          type: "string"
        },
        html: {
          type: "string"
        },
        text: {
          type: "string"
        }
      }
    },
    mail_response: {}
  },
  operations: {
    create: {
      description: `api interno para envío de mail. Solo para uso desde el server`,
      requestBody: {
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/mail_create"
            },
            examples: {
              basic: {
                value: {
                  from: "support@winsshar.com",
                  to: "test@gmail.com",
                  subject: "test emailer service",
                  html: "html message",
                  text: "text message"
                }
              },
              'forgot Password': {
                value: {
                  from: "support@winsshar.com",
                  to: "test@gmail.com",
                  template: "forgotPwd",
                  user: {
                    password: '*** supassword ***'
                  }

                }
              },
              verify: {
                value: {
                  from: "support@winsshar.com",
                  to: "test@gmail.com",
                  template: "verify"
                }
              },
            }
          }
        }
      },
      "responses.201": {
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/mail_response"
            }
          }
        }
      }
    }
  }
};
