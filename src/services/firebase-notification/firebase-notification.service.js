// Initializes the `FirebaseNotification` service on path `/firebase-notification`
const { FirebaseNotification } = require('./firebase-notification.class');
const hooks = require('./firebase-notification.hooks');

module.exports = function (app) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/api/firebase-notification', new FirebaseNotification(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('api/firebase-notification');

  service.hooks(hooks);
};
