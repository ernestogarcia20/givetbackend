/* eslint-disable no-unused-vars */
/**
 * "payload": {
      "notification": {
        "title": "Title max 100 character",
        "body": "Body max 100 character"
      },
      "data": {
        "title": "Title Complete",
        "body": "Body complete",
        "action": "Action for the notification",
        "params": "Params for the action" 
      }
    }
 */
const {
  Forbidden
} = require("@feathersjs/errors");
exports.FirebaseNotification = class FirebaseNotification {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
  }

  async find (params) {
    return [];
  }

  async get (id, params) {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  async create (data, params) {
    const firebaseAdmin = this.app.get('firebaseAdmin');
    const messaging = firebaseAdmin.messaging();
    const {fcm_token, payload, topic, idUser = false} = data;
    const options = {
      priority: "high",
      timeToLive: 60 * 60 * 24
    };
    if(!payload){
      throw new Forbidden('Payload Empty')
    }
    if(topic){
      await messaging.sendToTopic(topic, payload, options);
    }else {
      if(fcm_token){
        await messaging.sendToDevice(fcm_token, payload, options);
      }
    }
    if(idUser){
     return await this.app.service('/api/messages').create({
        hUserTo: idUser,
        message: payload.data.body,
      });
    }
    return {result: true};
  }

  async update (id, data, params) {
    return data;
  }

  async patch (id, data, params) {
    return data;
  }

  async remove (id, params) {
    return { id };
  }
};
