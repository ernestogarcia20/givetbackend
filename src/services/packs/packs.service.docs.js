module.exports = {
  description: '',
  schemas: {
    pack_create: {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int32"
        },
        "packName": {
          "type": "string",
          "maxLength": 45,
          "nullable": true
        },
        "price": {
          "type": "number",
          "format": "float",
          "nullable": true,
          "default": "0"
        },
        "tokens": {
          "type": "number",
          "format": "float",
          "nullable": true,
          "default": "0"
        }
      }
    }
  },
  operations: {
    create: {
      requestBody: {
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/pack_create'
            }
          }
        }
      },
    },
    update: false,
    patch: {
      requestBody: {
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/pack_create'
            }
          }
        }
      },
    },
  }
}
