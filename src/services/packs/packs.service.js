// Initializes the `packs` service on path `/packs`
const {
  Packs: Service
} = require('./packs.class');
const createModel = require('../../models/packs.model');
const hooks = require('./packs.hooks');
const docs = require('./packs.service.docs');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  const servicePath = '/api/packs';
  var serviceObject = new Service(options, app);
  serviceObject.docs = docs;

  // Initialize our service with any options it requires
  app.use(servicePath, serviceObject);

  // Get our initialized service so that we can register hooks
  const service = app.service(servicePath);

  service.hooks(hooks);
};;
